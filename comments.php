<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains comments and the comment form.
 *
 */
 
/*
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
if ( post_password_required() )
    return;
?>

<?php

$args = array(
    'fields' => apply_filters(
        'comment_form_default_fields', array(
            'author' =>'<div class="comment-form-author">' . '<input id="author" placeholder="Escribe tu nombre" name="author" type="text" value="' .
                esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' />'.
                '</div>'
                ,
            'email'  => '<div class="comment-form-email">' . '<input id="email" placeholder="Escribe tu email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
                '" size="30"' . $aria_req . ' />' 
                 .
                '</div>'
        )
    ),
    'comment_field' => '<div class="comment-form-comment">' .
        '<textarea id="comment" name="comment" placeholder="Escribe un comentario" cols="45" rows="1" aria-required="true"></textarea>' .
        '</div>',
    'url'    => '',
    'title_reply' => 'Deja un comentario',
    'submit_button' => '<button type="submit" class="btn is-verde is-rounded size-s %3$s" id="%2$s">%4$s</button>',
    'submit_field' => '<div class="form-submit">%1$s %2$s</div>'
);

?>

<div id="comments" class="comments-area">
    <?php comment_form($args, $post_id); ?>

    <?php if ( have_comments() ) : ?>

    <ol class="comment-list">
        <?php
                wp_list_comments('type=comment&callback=ccu_type_comment');
            ?>
    </ol><!-- .comment-list -->

    <?php if ( ! comments_open() && get_comments_number() ) : ?>
    <p class="no-comments"><?php _e( 'Comments are closed.' , 'twentythirteen' ); ?></p>
    <?php endif; ?>

    <?php endif; // have_comments() ?>



</div><!-- #comments -->