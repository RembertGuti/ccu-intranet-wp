$(document).ready(function() {
	$('.slider-4').slick({
		slidesToShow: 1,
	  slidesToScroll: 1,
	  fade: false,
		speed: 700,
		arrows: true,
		dots: true,
		asNavFor: '.slider-nav-4',
		responsive: [
		{
		  breakpoint: 851,
		  settings: {
				arrows: false,
		  }
		}
		]
	});

	$('.slider-nav-4').slick({
		slidesToShow: 3,
	  slidesToScroll: 1,
	  asNavFor: '.slider-4',
	  dots: false,
	  centerMode: true,
		arrows: false,
	  focusOnSelect: true,
		responsive: [
		{
		  breakpoint: 851,
		  settings: {
				arrows: false,
				slidesToShow: 2,
			  slidesToScroll: 1,
		  }
		}
		]
	});

	equalOuterHeight($(".slider-4 .slide .content"));
	equalOuterHeight($(".slider-nav-4 li"));
})
