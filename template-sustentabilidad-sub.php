<?php
/*
* Template Name: Subpágina Sustentabilidad
*/
get_header();
?>
<?php $color_base = get_field( 'color_base' ); ?>
<section class="section shortcuts-area">
    <div class="wrap-xl">
        <div class="shortcuts-boxes">
            <?php $boton_eje_personas = get_field( 'boton_eje_personas', 14 ); ?>
            <?php if ( $boton_eje_personas ) { ?>
            <div class="shortcut-box">
                <a href="<?php echo $boton_eje_personas['url']; ?>"
                    target="<?php echo $boton_eje_personas['target']; ?>"
                    class="btn size-s is-rounded is-burdeo <?php echo ($color_base == 'burdeo' ? '' : 'is-bordered'); ?>"><?php echo $boton_eje_personas['title']; ?></a>
            </div>
            <?php } ?>
            <?php $boton_eje_marcas = get_field( 'boton_eje_marcas', 14 ); ?>
            <?php if ( $boton_eje_marcas ) { ?>
            <div class="shortcut-box">
                <a href="<?php echo $boton_eje_marcas['url']; ?>" target="<?php echo $boton_eje_marcas['target']; ?>"
                    class="btn size-s is-rounded is-celeste <?php echo ($color_base == 'celeste' ? '' : 'is-bordered'); ?>"><?php echo $boton_eje_marcas['title']; ?></a>
            </div>
            <?php } ?>
            <?php $boton_eje_planeta = get_field( 'boton_eje_planeta', 14 ); ?>
            <?php if ( $boton_eje_planeta ) { ?>
            <div class="shortcut-box">
                <a href="<?php echo $boton_eje_planeta['url']; ?>" target="<?php echo $boton_eje_planeta['target']; ?>"
                    class="btn size-s is-rounded is-amarillo <?php echo ($color_base == 'amarillo' ? '' : 'is-bordered'); ?>"><?php echo $boton_eje_planeta['title']; ?></a>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
<section class="section page-content-area">
    <div class="wrap-xl">
        <div class="page-content">
            <div class="wysiwyg force-<?php the_field( 'color_base' ); ?>">
                <h1 class="page-title"><?php the_title(); ?></h1>
                <?php the_field( 'contenido' ); ?>
            </div>
            <div class="test-multisite">
                <?php
                // in this variable you can pass all the blog IDs you would like to display posts from
                $blog_ids = array( 1, 2 );

                foreach( $blog_ids as $id ) {
                    switch_to_blog( $id );

                    $args = array(
                        'post_type' => 'post',
                        'posts_per_page' => -1,
                    ); // any WP_Query args should be here
                    $query = new WP_Query( $args );

                    if( $query->have_posts() ) : 
                    while( $query->have_posts() ) : $query->the_post(); ?>

                <ul>
                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                </ul>

                <?php
                    endwhile;
                    endif;
                    wp_reset_postdata();

                    restore_current_blog();
                }
                ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>