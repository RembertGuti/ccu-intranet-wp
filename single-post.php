<?php get_header() ?>
<?php while(have_posts()) : the_post(); ?>
<section class="section">
    <div class="wrap-xl">
        <div class="page-heading single-post-heading">
            <?php
        $pageThumbImg = get_the_post_thumbnail_url();
        $pageThumbnailID = get_post_thumbnail_ID();
        $alt = get_post_meta ( $pageThumbnailID, '_wp_attachment_image_alt', true );
        $thumbPos = get_field( 'bg_posicion' );
        ?>
            <div class="bg-image cover"
                style="background-image: url(<?php echo $pageThumbImg; ?>); background-position: <?php echo $thumbPos; ?>;"
                title="<?php echo $alt; ?>">
                <div class="veil"></div>
            </div>
            <div class="content">
                <div class="post-cat-area">
                    <?php $category_detail = get_the_category($post->ID);
                    foreach($category_detail as $cd){
                    echo '<span>#'.$cd->cat_name.'</span> ';
                    } ?>
                </div>
                <span>Publicado el <?php the_date(); ?></span>
                <h1><?php the_title(); ?></h1>
                <div class="intro-page">
                    <?php the_excerpt(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php if ( have_rows( 'contenido' ) ): ?>
<section class="section single-post-content-area">
    <div class="wrap-xl">
        <div class="content">
            <?php while ( have_rows( 'contenido' ) ) : the_row(); ?>
            <?php if ( get_row_layout() == 'texto_basico' ) : ?>
            <div class="block-content wysiwyg">
                <?php the_sub_field( 'texto_content' ); ?>
            </div>
            <?php elseif ( get_row_layout() == 'galeria_imagenes' ) : ?>
            <div class="slider-area">
                <?php if ( have_rows( 'slider_gal' ) ) : ?>
                <a href="#" id="gal-arrow-prev" class="arrow prev"><i class="icon-chevron-left"></i></a>
                <a href="#" id="gal-arrow-next" class="arrow next"><i class="icon-chevron-right"></i></a>
                <div class="slider-galeria">
                    <?php while ( have_rows( 'slider_gal' ) ) : the_row(); ?>
                    <?php $tipo_de_imagen_gal = get_sub_field( 'tipo_de_imagen_gal' ); ?>

                    <div class="slide">
                        <?php if ($tipo_de_imagen_gal == 'horizontal') { ?>
                        <?php $imagen_gal = get_sub_field( 'imagen_gal' ); ?>
                        <div class="imagen-gal cover" style="background-image: url(<?php echo $imagen_gal['url']; ?>);"
                            title="<?php echo $imagen_gal['alt']; ?>"></div>
                        <?php }else{ ?>
                        <?php $imagen_gal = get_sub_field( 'imagen_gal' ); ?>
                        <div class="imagen-gal cover img-vertical"
                            style="background-image: url(<?php echo $imagen_gal['url']; ?>);"
                            title="<?php echo $imagen_gal['alt']; ?>"></div>
                        <?php } ?>
                    </div>

                    <?php endwhile; ?>
                </div>
                <?php endif; ?>
                <?php if ( have_rows( 'slider_gal' ) ) : ?>
                <div class="slider-nav-galeria">
                    <?php while ( have_rows( 'slider_gal' ) ) : the_row(); ?>
                    <div class="slide">
                        <?php $imagen_gal = get_sub_field( 'imagen_gal' ); ?>
                        <div class="imagen-gal cover" style="background-image: url(<?php echo $imagen_gal['url']; ?>);"
                            title="<?php echo $imagen_gal['alt']; ?>"></div>
                    </div>
                    <?php endwhile; ?>
                </div>
                <?php endif; ?>
            </div>
            <?php endif; ?>
            <?php endwhile; ?>
        </div>
    </div>
</section>
<?php endif; ?>
<?php $postTags = get_the_tags();
    $tagsComma      = ' ';
    $outputTags     = '';

?>
<section class="section single-post-extra-meta-area">
    <div class="wrap-xl">
        <div class="content">
            <?php if($postTags) { ?>
            <div class="post-tags-area">
                <div class="post-tags-box">
                    <h5 class="tags-title">Etiquetado en: </h5>
                    <div class="post-tags">
                        <?php foreach( $postTags as $tag ) {
                        $outputTags .= '<span class="tag-name">#' . $tag->name . '</span>' . $tagsComma;
                        }
                        echo trim( $outputTags, $tagsComma );
                        ?>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
<?php endwhile; ?>
<script>
$(document).ready(function() {
    $('.slider-galeria').slick({
        arrows: false,
        dots: false,
        speed: 750,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.slider-nav-galeria'
    });
    $('.slider-nav-galeria').slick({
        arrows: false,
        dots: false,
        speed: 750,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-galeria',
        focusOnSelect: true
    });
    $('#gal-arrow-prev').click(function(e) {
        e.preventDefault();
        $('.slider-galeria').slick('slickPrev');
    });
    $('#gal-arrow-next').click(function(e) {
        e.preventDefault();
        $('.slider-galeria').slick('slickNext');
    });
});
</script>
<?php get_footer() ?>