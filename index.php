<?php get_header() ?>
	<section>
		<p class="primary-title">Para empezar</p>
		<p style="margin-bottom: 20px;">Merlín es una aproximación al diseño atómico construído día a día por la experiencia de permanentes desarrollos, <strong>buscando generar un lenguaje común y versátil entre diseñadores y desarrolladores front y back end</strong>, para así agilizar procesos y optimizar tiempos.</p>
		
		<div class="grid-column-2 gap-s">
			<div id="atomos" class="block">
				<p class="bold font-24">Átomos</p>
				<p>Son <strong>unidades discretas</strong>. Juntando dos o más de ellas se pueden construir <strong>moléculas</strong> y cuando se juntan moléculas, <strong>organismos</strong>.</p>
				<br>
				<a href="<?php echo get_permalink(2);?>">Revisa los átomos</a>
			</div>

			<div id="organismos" class="block">
				<p class="bold font-24">Organismos</p>
				<p><strong>Módulos funcionales, tanto en HTML como en PHP (Wordpress)</strong>. Todos tienen su propio SCSS y algunos de ellos tienen condicionantes, como clases o archivos JS asociados.</p>
				<br>
				<a href="<?php echo get_permalink(11);?>">Revisa los organismos</a>
			</div>
		</div>
	</section>
	
	<section>
		<p class="primary-title">Para los diseñadores</p>
		<div class="message-block text-center grid-column-full">
			<p>No queremos bajo ninguna circunstancia, eliminar tu libertad creativa. De hecho, te alentamos a que, en caso de que estimes conveniente y Merlín no solucione tus necesidades, diseñes nuevos módulos y así le enseñas un nuevo truco de magia.</p>	
		</div>
		<div class="grid-column-2 gap-s">
			<div id="wireframes" class="block">
				<p class="bold font-24">Wireframes/Diseño</p>
				<ul>
					<li>
						Para empezar tu nuevo proyecto, puedes descargar:
						<ul>
							<!--
							<li><a href="<?php echo site_url(); ?>/diseno/v.1.5/merlin/merlin_v.1.5" target="_blank">La versión actual (1.5) de Merlín:</a> Esta incluye todo lo que ha sido desarrollado por nuestros programadores.</li>
							-->
							<li><a href="<?php echo site_url(); ?>/diseno/latest/merlin/merlin.xd" target="_blank">La versión completa de Merlín:</a> Esta incluye todos los módulos diseñados hasta el momento, pero que no necesariamente han sido desarrollado por nuestros programadores.</li>
						</ul>
					</li>
				</ul>
			</div>

			<div id="iconografia" class="block">
				<p class="bold font-24">Iconografía</p>
				<ul>
					<li><a href="<?php echo site_url(); ?>/diseno/v.1.5/icons/SVG_v.1.5.zip" target="_blank">Descarga acá la versión actual (1.5)</a> de la fuente iconográfica de Merlín para comenzar a diseñar tu nuevo proyecto.</li>
					<li><a href="<?php echo site_url(); ?>/diseno/latest/icons/SVG.zip" target="_blank">Descarga acá la versión más actualizada</a> de íconos de Merlín para comenzar a diseñar tu nuevo proyecto.</li>
					<li>Si no se adecúa, puedes comenzar a hacer una nueva familia. Recuerda subirla a <a href="https://icomoon.io/" target="_blank">Icomoon</a> para que el desarrollador pueda programar sin problemas.</li>
				</ul>
			</div>
		</div>
	</section>

	<section>
		<p class="primary-title">Para los desarrolladores</p>

		<div class="grid-column-full message-block">
			<p>Merlín es un proyecto separado por organismos, eso quiere decir que cada uno de ellos tiene su propio .scss y js. Es <strong>sumamente importante que "limpies" el proyecto de aquellos módulos que no serán utilizados</strong>.</p>
			<p>Si estás en un Wordpress, debes ir al functions, buscar "merlin_styles_and_scripts()" y eliminar los .js y .css que no serán utilizados, además de eliminar en el archivo style.scss la importación de distintos .scss, de modo tal que no generes estilos que no necesites.</p>
		</div>
		
		<div id="estructura" class="grid-column-2 gap-s">
			<div class="block">
				<p class="bold font-24">Estructura del proyecto:</p>
				<p>Merlín presenta archivos ordenados por carpetas y subcarpetas con el objetivo de cuidar el orden:</p>
				<ul>
					<li>404.php</li>
					<li class="message-block">atomos.php <strong>(este archivo debe eliminarse en tu proyecto)</strong></li>					
					<li>
						<strong>components/</strong>
						<ul style="margin-bottom:0;">
							<li>
								<strong>custom/</strong>
								<ul style="margin-bottom: 0;">
									<li>
									<strong>js/</strong>
										<ul style="margin-bottom: 0;">
											<li class="message-block">
												<strong>highlight/ (esta carpeta debe eliminarse en tu proyecto)</strong>
												<ul style="margin-bottom:0;">
													<li>androidstudio.css</li>
													<li>default.css</li>
													<li>highlight.pack.js</li>
												</ul>
											</li>
										</ul>
									</li>
								</ul>
								<ul style="margin-bottom: 0;">
									<li>
									<strong>scss/</strong>
										<ul style="margin-bottom: 0;">
											<li>_buttons.scss</li>
											<li>_colors.scss</li>
											<li>_fonts.scss</li>
											<li>_header.scss</li>
											<li>_layout.scss</li>
											<li>_modal.scss</li>
											<li>_responsive_desktop.scss</li>
											<li>_responsive_phone.scss</li>
											<li>_responsive_tablet.scss</li>
											<li>_slick.scss</li>
											<li>_wysiwyg.scss</li>
										</ul>
									</li>
								</ul>
							<li>
								<strong>merlin/</strong>
								<ul style="margin-bottom: 0;">
									<li>
									<strong>icons/</strong>
										<ul style="margin-bottom: 0;">
											<li>icomoon.eot</li>
											<li>icomoon.svg</li>
											<li>icomoon.ttf</li>
											<li>icomoon.woff</li>
											<li>selection.json</li>
											<li>style.css</li>
											<li class="message-block">SVG.ZIP <strong>(este archivo debe eliminarse en tu proyecto)</strong></li>
										</ul>
									</li>
									<li>
									<strong>js/</strong>
										<ul style="margin-bottom: 0;">
											<li>animacion-anclas.js</li>
											<li>
												<strong>aos/</strong>
												<ul style="margin-bottom: 0;">
													<li>aos.css</li>
													<li>aos.js</li>
												</ul>
											</li>
											<li>header.js</li>
											<li>
												<strong>jquery/</strong>
												<ul style="margin-bottom: 0;">
													<li>jquery-ui.js</li>
													<li>jquery.js</li>
												</ul>
											</li>
											<li>
												<strong>lightgallery/</strong>
												<ul style="margin-bottom: 0;">
													<li>
														fonts/
														<ul style="margin-bottom: 0;">
															<li>lg.eot</li>
															<li>lg.svg</li>
															<li>lg.ttf</li>
															<li>lg.woff</li>
														</ul>
													</li>
													<li>lg-thumbnail.js</li>
													<li>lg-thumbnail.min.js</li>
													<li>lg-video.js</li>
													<li>lg-video.min.js</li>
													<li>lightgallery.css</li>
													<li>lightgallery.js</li>
												</ul>
											</li>
											<li>modal.js</li>
											<li>scripts.js</li>
											<li>simpleParallax.min.js</li>
											<li>
												<strong>slick/</strong>
												<ul style="margin-bottom: 0;">
													<li>slick.css</li>
													<li>slick.min.js</li>
												</ul>
											</li>
											<li>toggle.js</li>
										</ul>
									</li>
									<li>
									<strong>scss/</strong>
										<ul style="margin-bottom: 0;">
											<li>_animations.scss</li>
											<li>_flex.scss</li>
											<li>_fonts.scss</li>
											<li>_general.scss</li>
											<li>_icons.scss</li>
											<li>_mixins.scss</li>
											<li>_reset.scss</li>
											<li>_responsive_desktop.scss</li>
											<li>_responsive_phone.scss</li>
											<li>_responsive_tablet.scss</li>
										</ul>
									</li>
								</ul>
							</li>	
						</ul>
					</li>				
					<li>footer.php</li>					
					<li>functions.php</li>					
					<li>header.php</li>					
					<li>index.php <strong>(este archivo debe modificarse en tu proyecto)</strong></li>					
					<li class="message-block">moleculas.php <strong>(este archivo debe eliminarse en tu proyecto)</strong></li>					
					<li class="message-block">organismos.php <strong>(este archivo debe eliminarse en tu proyecto)</strong></li>					
					<li>
						<strong>organisms/</strong>
						<ul style="margin-bottom:0;">
							<li>
								<strong>blocks/</strong>
								<ul style="margin-bottom: 0;">
									<li>
									<strong><a href="<?php echo get_permalink(11);?>#block-1" target="_blank">01/ <i class="icon-external-link"></i></a></strong>
										<ul style="margin-bottom: 0;">
											<li>
												<strong>scss/</strong>
												<ul style="margin-bottom:0;">
													<li>_block-1.scss</li>
												</ul>
											</li>
										</ul>
									</li>
									<li>
									<strong><a href="<?php echo get_permalink(11);?>#block-2" target="_blank">02/ <i class="icon-external-link"></i></a></strong>
										<ul style="margin-bottom: 0;">
											<li>
												<strong>scss/</strong>
												<ul style="margin-bottom:0;">
													<li>_block-2.scss</li>
												</ul>
											</li>
										</ul>
									</li>
									<li>
									<strong><a href="<?php echo get_permalink(11);?>#block-3" target="_blank">03/ <i class="icon-external-link"></i></a></strong>
										<ul style="margin-bottom: 0;">
											<li>
												<strong>js/</strong>
												<ul style="margin-bottom:0;">
													<li>block-3.js</li>
													<li>count-up.js</li>
													<li>in-view.js</li>
												</ul>
											</li>

											<li>
												<strong>scss/</strong>
												<ul style="margin-bottom:0;">
													<li>_block-3.scss</li>
												</ul>
											</li>
										</ul>
									</li>
									<li>
									<strong><a href="<?php echo get_permalink(11);?>#block-4" target="_blank">04/ <i class="icon-external-link"></i></a></strong>
										<ul style="margin-bottom: 0;">
											<li>
												<strong>scss/</strong>
												<ul style="margin-bottom:0;">
													<li>_block-4.scss</li>
												</ul>
											</li>
										</ul>
									</li>
									<li>
									<strong><a href="<?php echo get_permalink(11);?>#block-5" target="_blank">05/ <i class="icon-external-link"></i></a></strong>
										<ul style="margin-bottom: 0;">
											<li>
												<strong>js/</strong>
												<ul style="margin-bottom:0;">
													<li>block-5.js</li>
												</ul>
											</li>
											<li>
												<strong>scss/</strong>
												<ul style="margin-bottom:0;">
													<li>_block-5.scss</li>
												</ul>
											</li>
										</ul>
									</li>
									<li>
									<strong><a href="<?php echo get_permalink(11);?>#block-6" target="_blank">06/ <i class="icon-external-link"></i></a></strong>
										<ul style="margin-bottom: 0;">
											<li>
												<strong>scss/</strong>
												<ul style="margin-bottom:0;">
													<li>_block-6.scss</li>
												</ul>
											</li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<strong>sliders/</strong>
								<ul style="margin-bottom: 0;">
									<li>
									<strong><a href="<?php echo get_permalink(11);?>#slider-1" target="_blank">01/ <i class="icon-external-link"></i></a></strong>
										<ul style="margin-bottom: 0;">
											<li>
												<strong>js/</strong>
												<ul style="margin-bottom:0;">
													<li>slider-1.js</li>
												</ul>
											</li>
											<li>
												<strong>scss/</strong>
												<ul style="margin-bottom:0;">
													<li>_slider-1.scss</li>
												</ul>
											</li>
										</ul>
									</li>
									<li>
									<strong><a href="<?php echo get_permalink(11);?>#slider-2" target="_blank">02/ <i class="icon-external-link"></i></a></strong>
										<ul style="margin-bottom: 0;">
											<li>
												<strong>js/</strong>
												<ul style="margin-bottom:0;">
													<li>slider-2.js</li>
												</ul>
											</li>
											<li>
												<strong>scss/</strong>
												<ul style="margin-bottom:0;">
													<li>_slider-2.scss</li>
												</ul>
											</li>
										</ul>
									</li>
									<li>
									<strong><a href="<?php echo get_permalink(11);?>#slider-3" target="_blank">03/ <i class="icon-external-link"></i></a></strong>
										<ul style="margin-bottom: 0;">
											<li>
												<strong>js/</strong>
												<ul style="margin-bottom:0;">
													<li>slider-3.js</li>
												</ul>
											</li>
											<li>
												<strong>scss/</strong>
												<ul style="margin-bottom:0;">
													<li>_slider-3.scss</li>
												</ul>
											</li>
										</ul>
									</li>
									<li>
									<strong><a href="<?php echo get_permalink(11);?>#slider-4" target="_blank">04/ <i class="icon-external-link"></i></a></strong>
										<ul style="margin-bottom: 0;">
											<li>
												<strong>js/</strong>
												<ul style="margin-bottom:0;">
													<li>slider-4.js</li>
												</ul>
											</li>
											<li>
												<strong>scss/</strong>
												<ul style="margin-bottom:0;">
													<li>_slider-4.scss</li>
												</ul>
											</li>
										</ul>
									</li>
									<li>
									<strong><a href="<?php echo get_permalink(11);?>#slider-5" target="_blank">05/ <i class="icon-external-link"></i></a></strong>
										<ul style="margin-bottom: 0;">
											<li>
												<strong>js/</strong>
												<ul style="margin-bottom:0;">
													<li>slider-5.js</li>
												</ul>
											</li>
											<li>
												<strong>scss/</strong>
												<ul style="margin-bottom:0;">
													<li>_slider-5.scss</li>
												</ul>
											</li>
										</ul>
									</li>
									<li>
									<strong><a href="<?php echo get_permalink(11);?>#slider-6" target="_blank">06/ <i class="icon-external-link"></i></a></strong>
										<ul style="margin-bottom: 0;">
											<li>
												<strong>js/</strong>
												<ul style="margin-bottom:0;">
													<li>slider-6.js</li>
												</ul>
											</li>
											<li>
												<strong>scss/</strong>
												<ul style="margin-bottom:0;">
													<li>_slider-6.scss</li>
												</ul>
											</li>
										</ul>
									</li>	
									<li>
									<strong><a href="<?php echo get_permalink(11);?>#slider-7" target="_blank">07/ <i class="icon-external-link"></i></a></strong>
										<ul style="margin-bottom: 0;">
											<li>
												<strong>js/</strong>
												<ul style="margin-bottom:0;">
													<li>slider-7.js</li>
												</ul>
											</li>
											<li>
												<strong>scss/</strong>
												<ul style="margin-bottom:0;">
													<li>_slider-7.scss</li>
												</ul>
											</li>
										</ul>
									</li>
								</ul>
							</li>
						</ul>
					</li>					
					<li>page.php</li>					
					<li>screenshot.png</li>					
					<li>search.php</li>					
					<li>searchform.php</li>					
					<li>single.php</li>					
					<li>style.css</li>					
					<li>style.scss</li>					
				</ul>
			</div>
	
			<div class="block">
				<p class="bold font-24">JS:</p>
				<p>Merlín trae unos archivos básicos que pueden aplicar para gran cantidad de proyectos:</p>
				<ul>
					<li>
						<strong>components/</strong>
						<ul style="margin-bottom:0;">
							<li>
								<strong>merlin/</strong>
								<ul style="margin-bottom: 0;">
									<li>
									<strong>js/</strong>
										<ul style="margin-bottom: 0;">
											<li><strong>animacion-anclas.js:</strong> Al elemento <strong>href</strong> que tenga como destino una id dentro de la misma página, debe añadírsele la clase <strong>.scroll-move</strong> y se animará el movimiento del scroll con una transición fluida.</li>
											<li><strong>aos/:</strong> Es una libería para generar animaciones mientras el usuario hace scroll. <a href="https://michalsnik.github.io/aos/" target="_blank">La documentación completa la puedes encontrar haciendo click acá <i class="icon-external-link"></i></a>.</li>
											<li><strong>jquery/:</strong> Se encuentran los archivos jquery.js (v3.4.1) y jquery-ui.js(v1.12.1)</li>
											<li><strong>jquery/:</strong> Librería para generar galerías de videos/fotos. <a href="https://sachinchoolur.github.io/lightGallery/" target="_blank">La documentación completa la puedes encontrar haciendo click acá <i class="icon-external-link"></i></a>.</li>
											<li><strong>modal.js:</strong> Todo aquello necesario para hacer funcionar los distintos modals en Merlín.</strong></li>
											<li><strong>scripts.js:</strong> Scripts generales que podrían aplicar a muchos proyectos. Entre ellos: Igualar elementos en altura, la llamada a tabs ó los triggers para el menú en dispositivos móviles</strong></li>
											<li><strong>simpleParallax.min.js:</strong> Parallax en aquellos elementos que sea necesario aplicarlo (documentado en Átomos). <a href="https://simpleparallax.com/" target="_blank">La documentación completa de la librería la puedes encontrar haciendo click acá <i class="icon-external-link"></i></a>.</li>
											<li><strong>slick/:</strong> Este script (y su .css básico) son aplicados en todos los sliders de Merlín. Debe eliminarse sólo en el caso de que no exista ningún carrusel en el proyecto. <a href="https://kenwheeler.github.io/slick/" target="_blank">La documentación completa la puedes encontrar haciendo click acá <i class="icon-external-link"></i></a>.</li>
											<li><strong>toggle.js:</strong> Para hacer toggle en el elemento que incorpore las clases correctas.</li>
										</ul>
									</li>
								</ul>
							</li>
						</ul>
					</li>
			</div>
			
			<div id="sass-merlin" class="block">
				<p class="bold font-24">SCSS de Merlín:</p>
				<p>Estos estilos <strong>no son editables</strong>, ya que manejan estructuras bases para que tu construyas el repertorio de magia de Merlín. Estos archivos se encuentran en la carpeta <strong>components/merlin/scss/</strong>:</p>
				<ol>
					<li><strong>_reset.scss:</strong> Quita atributos que vienen por default en navegadores (como márgenes, paddings), etc.</li>
					<li><strong>_mixins.scss:</strong> Este es un archivo sumamente importante, porque en ocasiones puede ahorrarte tiempo en soluciones que ya están desarrolladas. <a href="<?php echo get_permalink(2);?>#mixins">Pinchando acá puedes saber más de los @mixins de Merlín</a>.</li>
					<li><strong>_icons.scss:</strong> Llama a la familia tipográfica de íconos de merlín. También crea clases por cada ícono.</li>
					<li><strong>_fonts.scss:</strong> Este archivo es distinto al que se encuentra en components/customs/scss/, ya que este maneja atributos generales, como el peso, la alineación/justificado y tamaños.</li>
					<li><strong>_general.scss:</strong> Se declaran los contenedores, elementos gráficos, los breakpoints para el responsive, columnas y grillas, entre otras cosas.</li>
					<li><strong>_flex.scss:</strong> Distintos aspectos de un elemento con display: flex.</li>
					<li><strong>_animations.scss:</strong> Efectos de animación (zoom, zoom-out) y transiciones para hacer mas "suave" transiciones entre :hover o lo que estimes conveniente.</li>
					<li><strong>_responsive_desktop.scss:</strong> Estilos para alguna resolución de escritorio más pequeña.</li>
					<li><strong>_responsive_tablet.scss:</strong> Estilos para tablets.</li>
					<li><strong>_responsive_phone.scss:</strong> Estilos para smartphones.</li>
				</ol>
			</div>
	

			<div id="sass-customizable" class="block">
				<p class="bold font-24">SCSS Customizable:</p>
				<p>Puedes comenzar a editar todos aquellos .scss que se encuentran en la carpeta <strong>components/custom/scss/</strong> para darle vida a tu nuevo proyecto:</p>
				<ol style="margin-bottom: 0;">
					<li><strong>_colors.scss:</strong> Generar variables para los distintos colores.</li>
					<li><strong>_fonts.scss:</strong> Definir la(s) familia(s) tipográfica(s).</li>
					<li>
						<strong>_header.scss:</strong> Vienen ya, estilos pre-definidos para la cabecera, tales cómo:
						<ul style="margin-bottom: 0;">
							<li>ul.main-menu: Listado del menu principal</li>
							<li>ul.sub-menu: Estilos para un sub-menú</li>
							<li>.nav-down, .nav-up, .nav-top: Estilos para que la cabecera aparezca/desaparezca cuando el usuario hace scroll. Esta es dependiente del script <a href="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/js/header.js" target="_blank">header.js</a> ubicado en /components/melin/js/header.js.</li>
							<li>.menu-responsive: Menú que rezca/desaparezca cuando el usuario hace scroll. Esta es dependiente del script <a href="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/js/header.js" target="_blank">header.js</a> ubicado en /components/melin/js/header.js.</li>
							<li class="message-block"><strong>.aside:</strong> Eliminar estilo. Este no te servirá en tu proyecto.</li>
						</ul>
					</li>
					<li><strong>_modal.scss:</strong> Estilos para el modal.</li>
					<li><strong>_slick.scss:</strong> SCSS generales para la librería <a href="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/js/slick/slick.min.js" target="_blank">slick.js</a>, utilizada para los distintos sliders de Merlín.</li>
					<li><strong>_wysiwyg.scss:</strong> Muchos aspectos del editor WYSIWYG (What You See Is What You Get) ya se encuentran parametrizados en Merlín. Este archivo ya tiene desarrollado estilos básicos para párrafos, citas, galerías, tablas, etc.</li>
					<li><strong>_buttons.scss:</strong> Estilos para los botones.</li>
					<li><strong>_responsive_desktop.scss:</strong> Estilos para alguna resolución de escritorio más pequeña.</li>
					<li><strong>_responsive_tablet.scss:</strong> Estilos para tablets.</li>
					<li><strong>_responsive_phone.scss:</strong> Estilos para smartphones.</li>
					<li class="message-block">
						<strong>_layout.scss: </strong>Este archivo debes eliminarlo ó modificarlo. Acá hay estilos que estas visualizando ahora, pero que no te servirán en nada para tu proyecto. Sigue la siguiente lógica y así, todos los desarrolladores podrán encontrar tu código mucho más rápido si es que necesitan echarte una mano en un futuro:
						<ul>
							<li>Si tu proyecto, por ejemplo, tiene cómo páginas <strong>"Inicio", "Quiénes somos" y "Contacto"</strong>, <strong>anda construyendo un archivo .scss por cada una de ellas</strong>, de manera tal que exista una consistencia. En este hipotético caso, los archivos que darían estilos a cada una de estas páginas serían components/custom/scss/<strong>_inicio.scss</strong>, components/custom/scss/<strong>_quienes-somos.scss</strong> y components/custom/scss/<strong>_contacto.scss</strong>, los cuales debes llamar desde el archivo <strong>style.scss</strong>.</li>
						</ul>
					</li>
				</ol>
			</div>
		</div>
	</section>
	
	<section>
		<p class="primary-title">Merlín para Wordpress</p>
		<div class="grid-column-2 gap-s">
			<div id="instalacion" class="block grid-column-full">
				<p class="bold font-24">Instalación de un nuevo proyecto:</p>
				<ol>
					<li>Comienza por lo básico: Descarga la <a href="https://es.wordpress.org/latest-es_ES.zip" target="_blank">última versión de Wordpress en Español</a> <strong>(es_ES, no es_CL)</strong>.
					<li>Crea una nueva carpeta dentro de <strong>want.cl/dev/</strong> con el nombre de tu proyecto y añade los archivos del zip ahí.</li>
					<li>Mientras se suben los archivos al servidor, pídele a tu jefe de área que cree una nueva bbdd.</li>
					<li>Duplica el archivo <strong>wp-config-sample.php</strong> que se encontrará en la raíz de tu nuevo proyecto y ponle el nombre <strong>wp-config.php</strong>.</li>
					<li>Asígnale la bbdd, el user y pass que existe para desarrollo. Si no sabes cual es, pregúntale.</li>
				</ol>
			</div>
			
			<div id="theme" class="block">
				<p class="bold font-24">El tema:</p>
				<ol>
					<li>Descarga el tema <strong>merlin-want</strong>.</li>
					<li><strong>Cambia el nombre de la carpeta del tema por el de tu proyecto. </strong>Jamás debe dejarse el nombre <strong>merlin-want</strong>.</li>
					<li>Sube la carpeta a <strong>want.cl/dev/tu-proyecto/wp-content/themes/</strong>.</li>
					<li>
						Debes <strong>Modificar ciertos atributos </strong>en el archivo <strong>style.scss</strong>:
						<ul>
							<li><strong>Theme Name:</strong> Por el nombre del proyecto</li>
							<li><strong>Theme URI:</strong> Por la URL del proyecto</li>
							<li><strong>Version:</strong> Generalmente es 1.0, pero dependiendo del cliente, esta puede cambiar (por ejemplo, si es una postventa).</li>
							<li><strong>Description:</strong> Lo más probable es que este campo pueda ser eliminado, sin embargo, si deseas, puedes rellenarlo para hacer más precisa la documentación del proyecto.</li>
						</ul>
					</li>
					<li><strong>Reemplazar el archivo screenshot.png</strong>. Puedes pedirle al diseñador a cargo del proyecto que te genere un archivo .PNG de 1200x900 del home.</li>
					<li class="message-block"><strong>Elimina</strong> los archivos <strong>atomos.php</strong>, <strong>moleculas.php</strong> y <strong>organismos.php</strong>.</li>
					<li>También debes modificar el archivo <strong>index.php</strong> para comenzar en tu nuevo home.</li>
					<li><strong>Activa</strong> el tema desde el panel de Wordpress.</li>
					<li>Ya puedes comenzar a crear nuevas plantillas, así como también modificar y añadir nuevos archivos en <strong>components/custom</strong>.</li>
				</ol>
			</div>

			<div id="acf" class="block">
				<p class="bold font-24">ACF Pro:</p>
				<ol>
					<li>Ve al Wordpress de tu proyecto e instala ACF Pro (si no lo tienes, consúltale a algún colega por la versión de pago que tiene Want y su Key).</li>
					<li>Antes de empezar a generar nuevos ACF para tu proyecto, te recomendamos que veas la página de <a href="<?php echo get_permalink(11);?>"><?php echo get_the_title(11);?></a>. Ahí encontrarás documentación completa sobre qué módulos (y cómo se llaman) existen ya en Merlín.</li>
					<li>Entendemos que no todos los sitios/softwares web son iguales, sin embargo, existen patrones que se repiten. <strong>Levanta la información de cuáles en Merlín se parecen a los de tu proyecto y anótalos.</strong></li>
					<li>Dirígete a la pestaña <strong>Campos Personalizados -> Herramientas</strong>.</li>
					<li>Selecciona aquellos módulos que anotaste y <strong>expórtalos</strong>.</li>
					<li>Una vez instalado, dirígete a la pestaña <strong>Campos Personalizados -> Herramientas</strong>.</li>
					<li>En el recuadro Importar, selecciona el archivo importado desde Merlín e <strong>impórtalo</strong> en tu nuevo proyecto</li>
					<li class="message-block"><strong>Importante:</strong> Debes modificar el nombre de los ACF para hacerlos más accesibles al cliente. <strong>Por ejemplo Slider 1 puede pasar a llamarse Slider Principal o el Slider 2 puede llamarse ahora Carrusel de logotipos</strong>. Recuerda que hay una persona que administrará este sitio, y nuestro deber es hacer que sea fácil de utilizar.</li>
				</ol>
			</div>

			<div id="functions" class="block grid-column-full">
				<p class="bold font-24">Functions:</p>
				<p>Ya vienen algunas funciones básicas en Merlín. Acá las revisamos:</p>
				<pre>
					<code class="php">
&lt;?php
/*-------------------------------------------------------------*/
/*--- No mostrar la version de Wordpress dentro del &lt;head&gt; ----*/
/*-------------------------------------------------------------*/
function eliminar_version_wordpress() {
return '';
}
add_filter('the_generator', 'eliminar_version_wordpress');


/*-------------------------------------------------------------*/
/*-------------- Eliminar barra de administración -------------*/
/*-------------------------------------------------------------*/
function quitar_barra_administracion()  {
  return false;
}
add_filter( 'show_admin_bar' , 'quitar_barra_administracion');

/*-------------------------------------------------------------*/
/*---------------------- Eliminar Tags ------------------------*/
/*-------------------------------------------------------------*/
add_action('init', 'remove_tags');
function remove_tags(){
    register_taxonomy('post_tag', array());
}

/*-------------------------------------------------------------*/
/*--------- Permito imagen destacada en los Posts -------------*/
/*-------------------------------------------------------------*/
if ( function_exists( 'add_theme_support' ) )
add_theme_support( 'post-thumbnails' );

/*-------------------------------------------------------------*/
/*------------------ Menús personalizados ---------------------*/
/*-------------------------------------------------------------*/
register_nav_menus( array(
  'Menu principal' =&gt; 'Menu principal',
  'Menu footer' =&gt; 'Menu footer',
));

/*-------------------------------------------------------------*/
/*--- Le añado la clase "active" al elemento actual del menu ---*/
/*-------------------------------------------------------------*/
add_filter('nav_menu_css_class', function ($classes, $item, $args, $depth) {
    //Todas las diferentes clases "active" añadidas por WordPress
    $active = [
        'current-menu-item',
        'current-menu-parent',
        'current-menu-ancestor',
        'current_page_item'
    ];
    //Si coincide, añade la clase "active"
    if ( array_intersect( $active, $classes ) ) {
        $classes[] = 'active';
    }
    return $classes;
}, 10, 4);

/*----------------------------------------------------------------------------*/
/*---------------------------- Eliminar Gutenberg ----------------------------*/
/*----------------------------------------------------------------------------*/
add_filter('use_block_editor_for_post', '__return_false', 10);
add_filter('use_block_editor_for_post_type', '__return_false', 10);

/*-------------------------------------------------------------*/
/*--------- Compatibilidad del tema con Woocommerce -----------*/
/*-------------------------------------------------------------*/
function my_theme_setup() {
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'my_theme_setup' );


/*-------------------------------------------------------------*/
/*------------- Añadimos los CSS y JS del Theme ---------------*/
/*-------------------------------------------------------------*/
function merlin_styles_and_scripts() {
  wp_deregister_script( 'jquery' );

  //jQuery
  wp_enqueue_script( 'jquery', get_template_directory_uri() . '/components/merlin/js/jquery/jquery.js');

  //jQuery UI
  wp_enqueue_script( 'jquery-ui', get_template_directory_uri() . '/components/merlin/js/jquery/jquery-ui.js');

  //Slick slider
  wp_enqueue_script( 'slick', get_template_directory_uri() . '/components/merlin/js/slick/slick.min.js');
  wp_enqueue_style( 'slick-style', get_template_directory_uri() . '/components/merlin/js/slick/slick.css');

  //Lightgallery
  wp_enqueue_script( 'lightgallery', get_template_directory_uri() . '/components/merlin/js/lightgallery/lightgallery.js');
  wp_enqueue_style( 'lightgallery-style', get_template_directory_uri() . '/components/merlin/js/lightgallery/lightgallery.css');

  //AOS
  wp_enqueue_script( 'aos', get_template_directory_uri() . '/components/merlin/js/aos/aos.js');
  wp_enqueue_style( 'aos-style', get_template_directory_uri() . '/components/merlin/js/aos/aos.css');

  //Scripts de Merlín
  wp_enqueue_script( 'scripts', get_template_directory_uri() . '/components/merlin/js/scripts.js');
  wp_enqueue_script( 'simple-parallax', get_template_directory_uri() . '/components/merlin/js/simpleParallax.min.js');
  wp_enqueue_script( 'modal', get_template_directory_uri() . '/components/merlin/js/modal.js');
  wp_enqueue_script( 'toggle', get_template_directory_uri() . '/components/merlin/js/toggle.js');
  wp_enqueue_script( 'header', get_template_directory_uri() . '/components/merlin/js/header.js');
  wp_enqueue_script( 'animacion-anclas', get_template_directory_uri() . '/components/merlin/js/animacion-anclas.js');

  //Scripts de Organismos: Sliders
  wp_enqueue_script( 'slider-1', get_template_directory_uri() . '/organisms/sliders/01/js/slider-1.js');
  wp_enqueue_script( 'slider-2', get_template_directory_uri() . '/organisms/sliders/02/js/slider-2.js');
  wp_enqueue_script( 'slider-3', get_template_directory_uri() . '/organisms/sliders/03/js/slider-3.js');
  wp_enqueue_script( 'slider-4', get_template_directory_uri() . '/organisms/sliders/04/js/slider-4.js');
  wp_enqueue_script( 'slider-5', get_template_directory_uri() . '/organisms/sliders/05/js/slider-5.js');
  wp_enqueue_script( 'slider-6', get_template_directory_uri() . '/organisms/sliders/06/js/slider-6.js');
  wp_enqueue_script( 'slider-7', get_template_directory_uri() . '/organisms/sliders/07/js/slider-7.js');

  //Scripts de Organismos: Bloques
    //Bloque #3
    wp_enqueue_script( 'count-up', get_template_directory_uri() . '/organisms/blocks/03/js/count-up.js');
    wp_enqueue_script( 'in-view', get_template_directory_uri() . '/organisms/blocks/03/js/in-view.js');
    wp_enqueue_script( 'block-3', get_template_directory_uri() . '/organisms/blocks/03/js/block-3.js');
    //Bloque #5
    wp_enqueue_script( 'block-5', get_template_directory_uri() . '/organisms/blocks/05/js/block-5.js');
}

add_action( 'wp_enqueue_scripts', 'merlin_styles_and_scripts' );
?&gt;
					</code>
				</pre>
			</div>
		</div>
	</section>
	
	

<?php get_footer() ?>