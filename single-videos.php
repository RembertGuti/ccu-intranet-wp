<?php get_header() ?>
<?php
while ( have_posts() ) : the_post(); ?>
<section class="section">
    <div class="wrap-xl">
        <div class="page-heading single-video-heading">
            <?php
            $pageThumbImg = get_the_post_thumbnail_url();
            $pageThumbnailID = get_post_thumbnail_ID();
            $alt = get_post_meta ( $pageThumbnailID, '_wp_attachment_image_alt', true );
            $thumbPos = get_field( 'bg_posicion' );
            $thisYTvideo = get_field('id_video_youtube');

            if(empty($pageThumbImg)) {
                $videoBG = 'https://img.youtube.com/vi/'.$thisYTvideo.'/maxresdefault.jpg';
            } else {
                $videoBG = $pageThumbImg;
            }

            if(empty($thumbPos)) {
                $bgPos = 'center';
            } else {
                $bgPos = $thumbPos;
            }
            ?>
            <div class="bg-image"
                style="background-image: url(<?php echo $videoBG; ?>); background-position: <?php echo $bgPos; ?>; background-size:cover;"
                title="<?php echo $alt; ?>">
                <div class="veil"></div>
            </div>
            <div class="content-area">
                <div class="play-icono">
                    <a href="#" class="modal-trigger" data-id="modal-numero-dos"
                        data-video-url="<?php the_field( 'id_video_youtube' ); ?>"><img
                            src="<?php echo get_template_directory_uri(); ?>/img/play.svg" alt="">
                        <span class="tooltip-video">Ver video</span>
                    </a>
                </div>
                <div class="video-intro width-content">
                    <div class="post-cat-area">
                        <?php $category_detail = get_the_category($post->ID);
                        foreach($category_detail as $cd){
                        echo '<span>#'.$cd->cat_name.'</span> ';
                        } ?>
                    </div>
                    <span class="post-date">Publicado el <?php the_date(); ?></span>
                    <h1><?php the_title(); ?></h1>
                    <div class="bajada">
                        <p><?php the_field( 'introduccion' ); ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="single-video-content">
            <div class="width-content wysiwyg">
                <?php the_field( 'texto_content' ); ?>
            </div>
        </div>
    </div>
</section>
<div data-id="modal-numero-dos" class="modal">
    <i class="close icon-equis"></i>
    <div class="content-modal contenido wp-content">
        <div class="iframeVideo relative">
            <div id="player"></div>
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<?php endwhile; ?>
<script>
$(document).ready(function() {
    $('#videos-slider').slick({
        arrows: false,
        dots: false,
        speed: 750
    });
    $('#video-arrows .arrow').each(function(index, element) {
        if ($(this).hasClass('prev')) {
            $(this).click(function(e) {
                e.preventDefault();
                $('#videos-slider').slick('slickPrev');
            });

        } else if ($(this).hasClass('next')) {
            $(this).click(function(e) {
                e.preventDefault();
                $('#videos-slider').slick('slickNext');
            });
        }
    });
    $('.producto-formato-slider').each(function(index, element) {
        $(element).slick({
            arrows: false,
            dots: true,
            speed: 750
        });
    });
});
</script>
<?php get_footer() ?>