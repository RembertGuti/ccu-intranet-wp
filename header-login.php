<!doctype html>
<html lang="es" <?php language_attributes() ?>>

<head profile="http://gmpg.org/xfn/11">
    <title><?php echo esc_html( get_bloginfo('name'), 1 ); wp_title( '|', true, 'left' ); ?></title>
    <meta http-equiv="content-type" content="<?php bloginfo('html_type') ?>; charset=<?php bloginfo('charset') ?>" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url') ?>" />
    <?php if ( get_field( 'favicon', 'option' ) ) { ?>
    <link rel="shortcut icon" href="<?php the_field( 'favicon', 'option' ); ?>" type="image/x-icon">
    <?php } else { ?>
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">
    <?php } ?>
    <?php wp_head() ?>

    <script src="https://apis.google.com/js/api:client.js"></script>

    <!-- API YouTube -->
    <script src="https://www.youtube.com/iframe_api"></script>

    <!-- Scripts utilizados en el tema -->
    <link rel="stylesheet" type="text/css"
        href="<?php echo get_stylesheet_directory_uri(); ?>/components/custom/js/highlight/androidstudio.css" />
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/components/custom/js/highlight/highlight.pack.js"
        type="text/javascript"></script>
    <script>
    hljs.initHighlightingOnLoad();
    </script>

    <meta name="viewport"
        content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />

    <?php the_field('ga_code', 'option'); ?>
</head>

<body class="login-page">
    <?php
    $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
    if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0; rv:11.0') !== false)) { ?>
    <div class="not-compatible">
        <div class="box-info">
            <p class="disclaimer">Lo sentimos, tu navegador <b>no es compatible con este sitio.</b><br>Por favor
                descarga uno
                de los siguientes navegadores para visualizar el sitio:</p>
            <div class="browsers-area">
                <div class="browser-box">
                    <a href="https://www.google.com/chrome/" target="_blank" class="browser-link">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/chrome-logo.png" alt=""
                            class="logo-browser">
                    </a>
                </div>
                <div class="browser-box">
                    <a href="https://www.mozilla.org/es-CL/firefox/new/" target="_blank" class="browser-link">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/firefox-logo.png" alt=""
                            class="logo-browser">
                    </a>
                </div>
                <div class="browser-box">
                    <a href="https://www.microsoft.com/es-es/edge" target="_blank" class="browser-link">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/edge-logo.png" alt=""
                            class="logo-browser">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <header id="login-header">
        <div class="brand-area">
            <a href="<?php echo site_url('/'); ?>" target="_blank">
                <img src="<?php echo get_template_directory_uri(); ?>/img/logo-ccu-c.svg" alt="" class="logo-login">
            </a>
        </div>
    </header>

    <main>