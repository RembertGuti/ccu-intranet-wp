$(document).ready(function(){
	//Smooth scroll para el div que quiero
	$(".scroll-move").on('click', function(e) {
		e.preventDefault();
			var target = $(this).attr('href');
		$('html, body').animate({
			scrollTop: ($(target).offset().top)
		}, 1000);
	});
});
