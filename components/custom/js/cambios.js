(function($){

    $(document).on('change','#post-date-selector',function(e){
        e.preventDefault();
         
        var id = this.value;
        
        console.log(id);

        $.ajax({
            url: front_info.site_url+'/wp-json/ccu/cambios_enviar_contenido',
            type: 'post',
            accepts: {
                mycustomtype: 'application/json'
            },
            converters: {
                'text mycustomtype': function(result) {
                    return result;
                }
            },
            dataType: 'mycustomtype',
            data: {
                id_post: id
            },
            beforeSend: function(){
                $('#post-cambios').html('Cargando ...');
            },
            success: function(resultado){
                 $('#post-cambios').html(resultado);        
            }

        });

    });

})(jQuery);