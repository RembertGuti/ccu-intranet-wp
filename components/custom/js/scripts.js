function timeDisplay(zone, divSet) {
    var currentTime = new Date();
    currentTime.setHours(currentTime.getHours() + zone);
    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();
    var seconds = currentTime.getSeconds();
    var meridiem = " ";

    if (hours >= 12) {
        hours = hours - 12;
        meridiem = "pm";
    } else {
        meridiem = "am";
    }
    if (hours === 0) {
        hours = 12;
    }
    if (hours < 10) {
        hours = "0" + hours;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }

    var clockDiv = document.getElementById(divSet);
    clockDiv.innerText = hours + ":" + minutes + ":" + seconds + " " + meridiem;
}

function dateDisplay(divSet) {
    var currentDate = new Date();
    var meses = [
        "enero",
        "febrero",
        "marzo",
        "abril",
        "mayo",
        "junio",
        "julio",
        "agosto",
        "septiembre",
        "octubre",
        "noviembre",
        "diciembre",
    ];
    var diasemana = [
        "Domingo",
        "Lunes",
        "Martes",
        "Miércoles",
        "Jueves",
        "Viernes",
        "Sábado",
    ];
    var weekday = diasemana[currentDate.getDay()];
    var day = currentDate.getDate();
    var month = meses[currentDate.getMonth()];
    var year = currentDate.getFullYear();
    var dateDiv = document.getElementById(divSet);
    if(dateDiv){
        dateDiv.innerText = weekday + ", " + day + " de " + month + " de " + year;
    }
}
$(document).ready(function () {
    dateDisplay("current-date");
    $("#time-slider").slick({
        autoplay: true,
        arrows: false,
        infinite: false,
        speed: 500,
        slidesToShow: 1,
    });

    $("#time-area .prev").click(function (e) {
        e.preventDefault();
        $("#time-slider").slick("slickPrev");
    });
    $("#time-area .next").click(function (e) {
        e.preventDefault();
        $("#time-slider").slick("slickNext");
    });

    setInterval(function () {
        $("#time-slider .slide").each(function (index, element) {
            let timeZone = $(this).children(".time-data").data("timezone");
            let countryName = $(this)
                .children(".time-data")
                .children(".time")
                .attr("id");
            timeDisplay(timeZone, countryName);
        });
    }, 1000);

    $('.user-area').click(function (e) { 
        e.preventDefault();
        
        $('.user-menu').toggleClass('active-menu');
        $(this).toggleClass('active-menu');
    });
});

var currCountry = front_info.sessioncountry;
var country;
switch (currCountry) {
    case 'CHL':
        country = 'chile';
        break;
    case 'BOL':
        country = 'bolivia';
        break;
    default:
        country = 'chile';
}

function searchMarcas(pais, tipo) {
    $("#all-marcas .marcas-container .marcas-area").fadeOut(250);
    setTimeout(function() {
        $('.spinner').fadeIn(250);
    }, 251);

    $.ajax({
        method: 'POST',
        url: front_info.site_url+'/wp-json/ccu/filter_marcas',
        data: {
            'pais': pais,
            'tipo': tipo
        },
        success: function(data) {
            posts = data;
            $("#all-marcas .marcas-container .marcas-area").html(posts[1]);
            $('.spinner').fadeOut(250);
            setTimeout(function() {
                $("#all-marcas .marcas-container .marcas-area").fadeIn(250);
            }, 251);

        },
        error: function(data) {
            console.log(data + 'error');
        }
    });
}
function addCommas(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}
$(document).ready(function() {
    $('.spinner').hide();
    $('.mini-news-slide').slick({
        slideToShow: 1,
        arrows: false,
        dots: true
    });
    $('#filter-btn').click(function(e) {
        e.preventDefault();

        let tipoVal = $('#tipo').val();
        let paisVal = $('#pais').val();
        searchMarcas(paisVal, tipoVal);
    });
    $('.btn-ver-todas.tax-link').each(function(index, element) {
        $(this).click(function(e) {
            e.preventDefault();
            let permalink = $(this).attr('href');
            let taxonomia = $(this).data('this-tax');
            $("<form method='POST' action='" + permalink +
                    "'><input type='hidden' name='thistax' value='" + taxonomia + "'/></form>")
                .appendTo("body").submit();
        });
    });
    $("#pais").val(country);

    
    $.getJSON('https://mindicador.cl/api', function(data) {
         var dailyIndicators = data;
         var valoruf = addCommas(dailyIndicators.uf.valor);
         $("#uf-value").html(valoruf + ' CLP');
     }).fail(function() {
         console.log('Error al consumir la API!');
     });
    $('.box-compromiso').hide();
     $('#activar-compromiso').click(function (e) {
         e.preventDefault();
         $('.box-compromiso').slideToggle(750);
     });

     $('.consumo-activar').click(function (e) {
         e.preventDefault();
         let btnText = $(this).text();
        $(this).text(
            btnText == 'Ver Más' ? 'Ver Menos' : 'Ver Más'
        );
     });
});



var googleUser = {};
var startApp = function() {
    gapi.load('auth2', function(){
       gapi.auth2.init({
            client_id: front_info.GOOGLE_CLIENT_ID,
            cookiepolicy: 'single_host_origin',
            //ux_mode: 'redirect',
            //redirect_uri: location.protocol + '//' + location.host + location.pathname,
        }).then(function(e){
            login_element =$('#login-google-btn');
            login_element.hide();
            if(login_element.length){
                attachSignin(login_element[0]);
            }else{
                $('.log-out-load-button').show();
            }
        })

    });
};

url_parser = function(path = null) {
    if (path != null) {
        try {
            const url   = new URL(path, front_info.base_url);
            url.search  = replacer_url(decodeURIComponent(url.search));
            url.hash    = replacer_url(decodeURIComponent(url.hash));
            return replacer_url(url.href);
        } catch (error) {
        }
    }
    return null;
}

replacer_url = function(value = null) {
    if (value != null) {
        patterns = [];
        patterns.push(/\<script\>(.*?)\<\/script\>/gi);
        patterns.push(/src[\r\n]*=[\r\n]*\'(.*?)*\'/gi);
        patterns.push(/src[\r\n]*=[\r\n]*\"(.*?)*\"/gi);
        patterns.push(/\<\/script\>/gi);
        patterns.push(/\<script(.*?)\>/gi);
        patterns.push(/eval\((.*?)\)/gi);
        patterns.push(/expression\((.*?)\)/gi);
        patterns.push(/javascript:/gi);
        patterns.push(/vbscript:/gi);
        patterns.push(/onload(.*?)=/gi);
        for (var i = 0; i < patterns.length; i++) {
            patterns[i];
            value = value.replaceAll(patterns[i], '');
        }
    }
    return value;
}


function attachSignin(element) {
    $(element).click(function(){
        $(this).hide();
        $('#error-login-box').hide();
    });
    gapi.auth2.getAuthInstance().attachClickHandler(element, {},
        onSignIn,
        function(error) {
            $('#error-login-text').html('No se ha podido, iniciar sesión. Intentélo nuevamente.');
            $('#login-google-btn').show();
            $('#error-login-box').show();
        }
    );
    if (gapi.auth2.getAuthInstance().isSignedIn.get()) {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const url_segment = urlParams.get('redirect');
        onSignIn(gapi.auth2.getAuthInstance().currentUser.get(), url_segment);
    }else{
        $(element).show();
    }
}

function onSignIn(googleUser, url = null) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', front_info.network_site_url+'wp-json/ccu/login');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        let resp = null
        try{
            resp = JSON.parse(xhr.responseText);
        }catch(e){
            $('#error-login-text').html('Ha ocurrido un error. Intentélo nuevamente.');
            $('#login-google-btn').show();
            $('#error-login-box').show();
            return
        }
        if(resp != null && resp && typeof resp.status != 'undefined' && resp.status && resp.status == 'ok' ){
            if(url){
                window.location.href = url_parser(url);
            }else{
                window.location.href = front_info.network_site_url;
            }
        }else{
            $('#error-login-text').html('No se ha podido, validar la cuenta. Intentélo nuevamente.');
            $('#login-google-btn').show();
            $('#error-login-box').show();
        }
    };
    xhr.onerror = function(error){
        $('#error-login-text').html('Ha ocurrido un error. Intentélo nuevamente.');
        $('#login-google-btn').show();
        $('#error-login-box').show();
    };
    xhr.send('idtoken=' +  googleUser.getAuthResponse().id_token);
}

function signOut() {
    let auth2SO = gapi.auth2.getAuthInstance();
    auth2SO.signOut().then(function() {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', front_info.network_site_url+'wp-json/ccu/logout');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            window.location.href = front_info.network_site_url;
        };
        xhr.send();
    });
}


$(document).ready(function(){
    AOS.init();
    startApp();

    $('#hamburger').click(function (e) { 
        e.preventDefault();
        $('#side-header').addClass('active-menu');
    });
    $('#close-menu').click(function (e) { 
        e.preventDefault();
        $('#side-header').removeClass('active-menu');
    });

    $('#profile-btn').click(function (e) { 
        e.preventDefault();
        $('#floating-profile').addClass('active-menu');
    });
    $('#close-profile').click(function (e) { 
        e.preventDefault();
        $('#floating-profile').removeClass('active-menu');
    });

    $('#open-search-btn').click(function (e) { 
        e.preventDefault();
        $('#floating-search').addClass('active-menu');
    });
    $('#close-search').click(function (e) { 
        e.preventDefault();
        $('#floating-search').removeClass('active-menu');
    });
})