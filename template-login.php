<?php
/*
* Template Name: Login
*/
get_header('login');
function actual_date ()  
{  
    $week_days = array ("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado");  
    $months = array ("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");  
    $year_now = date ("Y");  
    $month_now = date ("n");  
    $day_now = date ("j");  
    $week_day_now = date ("w");  
    $date = $week_days[$week_day_now] . ", " . $day_now . " de " . $months[$month_now] . " de " . $year_now;   
    return $date;    
}  
?>
<section class="login-screen">
    <?php
        $pageThumbImg = get_the_post_thumbnail_url();
        $pageThumbnailID = get_post_thumbnail_ID();
        $alt = get_post_meta ( $pageThumbnailID, '_wp_attachment_image_alt', true );
        ?>
    <div class="bg-image cover" style="background-image: url(<?php echo $pageThumbImg; ?>)" title="<?php echo $alt; ?>">
        <div class="veil"></div>
    </div>
    <div class="wrap-xl">
        <div class="login-content">
            <div class="flags-date">
                <div class="flags-area">
                    <ul class="flag-list">
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/flag-chile.svg" alt=""
                                class="flag"><span>Chile</span>
                        </li>
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/flag-argentina.svg" alt=""
                                class="flag"><span>Argentina</span>
                        </li>
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/flag-bolivia.svg" alt=""
                                class="flag"><span>Bolivia</span>
                        </li>
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/flag-colombia.svg" alt=""
                                class="flag"><span>Colombia</span>
                        </li>
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/flag-paraguay.svg" alt=""
                                class="flag"><span>Paraguay</span>
                        </li>
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/flag-uruguay.svg" alt=""
                                class="flag"><span>Uruguay</span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="form-login-area">
                <div class="content">
                    <h1>Iniciar Sesión</h1>
                    <div class="form-area">
                        <div class="form-row">
                            <p class="form-instruction">
                                <?php the_field( 'instrucciones' ); ?>
                            </p>
                        </div>
                        <div class="form-row submit-area">
                            <button id="login-google-btn" type="submit"
                                class="btn is-verde-oscuro is-rounded">Acceder</button>
                        </div>
                        <div id="error-login-box" style="display: none;">
                            <div id="error-login-text"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="empresas-ccu-area">
                <?php if ( have_rows( 'logos_empresas' ) ) : ?>
                <div class="empresas-list">
                    <?php while ( have_rows( 'logos_empresas' ) ) : the_row(); ?>
                    <?php if ( get_sub_field( 'logo' ) ) { 
                    ?>
                    <img src="<?php the_sub_field( 'logo' ); ?>" class="logo-empresa"
                        height="<?php the_sub_field('ajuste_altura'); ?>" />
                    <?php } ?>
                    <?php endwhile; ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer('login'); ?>