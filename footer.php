        <footer>
            <p class="copyright">
                ©2021 Desarrollado por el área de Recursos Humanos de CCU y <a href="https://want.cl/"
                    target="_blank">WANT.CL</a>
            </p>
        </footer>
        </main>
        <div id="floating-shortcuts">
            <div class="home-shortcut">
                <a href="<?php echo site_url('/'); ?>" class="link"><i class="icon-home"></i></a>
            </div>
            <div class="search-shortcut">
                <a href="#" id="open-search-btn" class="link"><i class="icon-buscar"></i></a>
            </div>
            <div class="profile-shortcut">
                <div id="profile-btn" class="avatar">
                    <img src="<?php echo get_photo_url($_SESSION['intranet_ccu_session']->EMPLID); ?>" alt=""
                        class="profile">
                </div>
            </div>
        </div>
        <div id="floating-profile">
            <div class="mobile-menu-heading">
                <div class="menu-title">
                    <span>Perfil</span>
                </div>
                <div class="close-menu-area">
                    <span id="close-profile"><i class="icon-equis"></i></span>
                </div>
            </div>
            <div class="user-area">
                <div class="avatar-box">
                    <div class="avatar">
                        <img src="<?php echo get_photo_url($_SESSION['intranet_ccu_session']->EMPLID); ?>" alt=""
                            class="profile">
                    </div>
                </div>
                <div class="info-box">
                    <div class="name">
                        <span><?php echo implode(' ', (array_filter([$_SESSION['intranet_ccu_session']->FIRST_NAME, $_SESSION['intranet_ccu_session']->MIDDLE_NAME, $_SESSION['intranet_ccu_session']->LAST_NAME, $_SESSION['intranet_ccu_session']->SECOND_LAST_NAME]))); ?></span>
                    </div>
                    <div class="range">
                        <span><?php echo $_SESSION['intranet_ccu_session']->JOBCODE_NAME; ?></span>
                    </div>
                </div>
            </div>
            <div class="user-menu">
                <?php if ( have_rows( 'menu_usuario', 'option' ) ) : ?>
                <div class="links">
                    <?php while ( have_rows( 'menu_usuario', 'option' ) ) : the_row(); ?>
                    <?php $link_user_menu = get_sub_field( 'link_user_menu' ); ?>
                    <?php if ( $link_user_menu ) { ?>
                    <a href="<?php echo $link_user_menu['url']; ?>" target="<?php echo $link_user_menu['target']; ?>"
                        class="out-link"><?php echo $link_user_menu['title']; ?></a>
                    <?php } ?>
                    <?php endwhile; ?>
                </div>
                <?php endif; ?>

                <div class="logout-btn">
                    <a onclick="signOut()" style="display: none" class="log-out-load-button">
                        <span>Cerrar Sesión</span>
                    </a>
                </div>
            </div>
        </div>
        <div id="floating-search">
            <div class="mobile-menu-heading">
                <div class="menu-title">
                    <span>Buscar</span>
                </div>
                <div class="close-menu-area">
                    <span id="close-search"><i class="icon-equis"></i></span>
                </div>
            </div>
            <div class="search-area">
                <form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                    <div class="search-input-area">
                        <input type="search"
                            placeholder="<?php echo esc_attr_x( 'Realiza una búsqueda', 'placeholder', 'base' ); ?>"
                            value="<?php echo get_search_query(); ?>" name="s" id="search-box">
                        <button type="submit" class="btn-search"><i class="icon-buscar"></i></button>
                    </div>
                </form>
            </div>
        </div>
        <?php wp_footer() ?>
        </body>

        </html>