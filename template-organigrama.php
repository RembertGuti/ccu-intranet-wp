<?php
/*
* Template Name: Organigrama
*/
get_header();
?>
<script type="text/javascript">
function get_suggestions(searchName) {
    stringSearch = searchName.replace(' ', '');
    if (stringSearch != '') {
        $.ajax({
            type: 'POST',
            data: {
                searchName: searchName
            },
            dataType: 'json',
            url: '<?php echo network_site_url()?>wp-json/ccu/suggestions_organigrama',
            success: function(result) {
                if ($("#search-box-primary").val() != '') {
                    resultSuggestions = '';
                    $('.resultado-sugerencia').show();
                    if (result['status'] == 'ok') {
                        for (i = 0; i < result['data'].length; i++) {
                            resultSuggestions += '<div class="employee" employeeId="' + result['data'][i][
                                'EMPLID'
                            ] + '">' + result['data'][i]['FULL_NAME'] + '</div>';
                        }
                        $('.resultado-sugerencia').html(resultSuggestions);
                        $('.resultado-sugerencia .employee').click(function() {
                            idEmployee = $(this).attr('employeeId');
                            nameEmployee = $(this).text();
                            get_organization_chart(idEmployee);
                            $("#search-box-primary").val(nameEmployee);
                        });
                    } else {
                        $('.resultado-sugerencia').html('No existen coincidencias');
                    }
                } else {
                    $('.resultado-sugerencia').html('').hide();
                }
            }
        });
    } else {
        $('.resultado-sugerencia').html('').hide();
    }
}

function get_organization_chart(idEmployee) {
    $('.resultado-content').html('Cargando...');
    $.ajax({
        type: 'POST',
        data: {
            idEmployee: idEmployee
        },
        dataType: 'json',
        url: '<?php echo network_site_url()?>wp-json/ccu/filter_organigrama',
        success: function(result) {
            console.log(result);
            if (result['status'] == 'ok') {
                $('.resultado-content').html(result['data']);
                $('.resultado-sugerencia').html('').hide();
            } else {
                $('.resultado-content').html(result['message']);
            }
            $('html,body').animate({
                scrollTop: $('.resultado-busqueda').offset().top
            }, 'slow');
        }
    });
}

$(document).ready(function() {
    $('.resultado-sugerencia').hide();
    $("#search-box-primary").keyup(function() {
        if ($(this).val() != '') {
            $('.resultado-sugerencia').html('Cargando...').slideDown('fast');
            get_suggestions($(this).val());
        } else {
            $('.resultado-sugerencia').html('').slideUp('fast');
        }
    });
});
</script>

<section class="section buscador-organigrama">
    <div class="wrap-xl">
        <div class="content">
            <div class="heading-box-area">
                <h3 class="head-title">Organigrama</h3>
            </div>
            <div class="buscador-heading">
                <div class="photo cover"
                    style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/buscador-organigrama.jpg)"
                    title="">
                    <div class="veil"></div>
                </div>
                <div class="buscador-box">
                    <h2>Buscar una persona</h2>
                    <div class="search-input-area">
                        <input type="search" name="global-search" id="search-box-primary" placeholder="Buscar">
                        <a href="#" class="btn-search" data-input="primary"><i class="icon-buscar"></i></a>
                        <div class="resultado-sugerencia" style="display: none;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section resultado-busqueda">
    <div class="wrap-xl">
        <div class="resultado-area">
            <div class="resultado-content">
                <div class="person-data">
                    <div class="person-avatar-box">
                        <div class="profile-img cover"
                            style="background-image: url(<?php echo get_photo_url($_SESSION['intranet_ccu_session']->EMPLID); ?>);"
                            title=""></div>
                    </div>
                    <div class="person-info-box">
                        <h3 class="nombre">
                            <?php echo implode(' ', (array_filter([$_SESSION['intranet_ccu_session']->FIRST_NAME, $_SESSION['intranet_ccu_session']->MIDDLE_NAME, $_SESSION['intranet_ccu_session']->LAST_NAME, $_SESSION['intranet_ccu_session']->SECOND_LAST_NAME]))); ?>
                        </h3>
                        <h4 class="cargo"><?php echo $_SESSION['intranet_ccu_session']->JOBCODE_NAME; ?></h4>
                        <h4><?php echo $_SESSION['intranet_ccu_session']->DEPT_NAME.', '.$_SESSION['intranet_ccu_session']->COUNTRY_NAME; ?>
                        </h4>
                        <div class="contact-info">
                            <a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&to=<?php echo $_SESSION['intranet_ccu_session']->EMAIL; ?>&tf=1"
                                class="contact-link" target="_blank">
                                <span class="icono"><i class="icon-chat"></i></span><span
                                    class="texto"><?php echo $_SESSION['intranet_ccu_session']->EMAIL; ?></span>
                            </a>
                        </div>
                    </div>
                </div>
                <?php 
                $organization_chart_info = get_organization_chart();
                if($organization_chart_info['status'] == 'ok'){ ?>
                <div class="organigrama-data">
                    <div class="organigrama-container">
                        <?php if($organization_chart_info['data']['supervisor']){ 
                                  $supervisor = $organization_chart_info['data']['supervisor'];
                            ?>
                        <div class="person-box level-one">
                            <div class="person-avatar-box">
                                <div class="profile-img cover"
                                    style="background-image: url(<?php echo get_photo_url($supervisor->EMPLID); ?>);"
                                    title=""></div>
                            </div>
                            <div class="person-info-box">
                                <h4 class="cargo"><?php echo $supervisor->JOBCODE_NAME; ?></h4>
                                <h3 class="nombre">
                                    <?php echo implode(' ', (array_filter([$supervisor->FIRST_NAME, $supervisor->MIDDLE_NAME, $supervisor->LAST_NAME, $supervisor->SECOND_LAST_NAME]))); ?>
                                </h3>
                            </div>
                        </div>
                        <?php } ?>

                        <div class="person-box level-two current-person">
                            <div class="person-avatar-box">
                                <div class="profile-img cover"
                                    style="background-image: url(<?php echo get_photo_url($_SESSION['intranet_ccu_session']->EMPLID); ?>);"
                                    title=""></div>
                            </div>
                            <div class="person-info-box">
                                <h4 class="cargo"><?php echo $_SESSION['intranet_ccu_session']->EMAIL; ?></h4>
                                <h3 class="nombre">
                                    <?php echo implode(' ', (array_filter([$_SESSION['intranet_ccu_session']->FIRST_NAME, $_SESSION['intranet_ccu_session']->MIDDLE_NAME, $_SESSION['intranet_ccu_session']->LAST_NAME, $_SESSION['intranet_ccu_session']->SECOND_LAST_NAME]))); ?>
                                </h3>
                            </div>
                        </div>

                        <?php if($organization_chart_info['data']['employee']){ 
                                    $employees = $organization_chart_info['data']['employee'];
                                    foreach ($employees as $key => $employee) { ?>
                        <div class="person-box level-three">
                            <div class="person-avatar-box">
                                <div class="profile-img cover"
                                    style="background-image: url(<?php echo get_photo_url($employee->EMPLID); ?>);"
                                    title=""></div>
                            </div>
                            <div class="person-info-box">
                                <h4 class="cargo"><?php echo $employee->JOBCODE_NAME; ?></h4>
                                <h3 class="nombre">
                                    <?php echo implode(' ', (array_filter([$employee->FIRST_NAME, $employee->MIDDLE_NAME, $employee->LAST_NAME, $employee->SECOND_LAST_NAME]))); ?>
                                </h3>
                            </div>
                        </div>
                        <?php       }
                              } ?>

                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>