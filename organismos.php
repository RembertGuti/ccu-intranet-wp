<?php
/* Template Name: Organismos */
get_header();
?>

	<section id="slider-1">
		<div class="tabs">
			<p class="primary-title">Sliders</p>
			<p class="secondary-title">Slider #1</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#php">PHP</a></li>
				<li><a href="#jquery">jQuery</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block border-radius-s text-left">
					<p>Se pueden añadir 4 tipos de sliders:</p>
					<ol>
						<li>Fotografía de fondo</li>
						<li>Subir video<strong>*</strong>: Al div de clase <strong>.slide</strong>, añadirle la clase <strong>video</strong>.</li>
						<li>Video de YouTube <strong>*</strong>: Al div de clase <strong>.slide</strong>, añadirle la clase <strong>youtube</strong>.</li>
						<li>Video de Vimeo <strong>*</strong>: Al div de clase <strong>.slide</strong>, añadirle la clase <strong>vimeo</strong>.</li>
					</ol>
					<p><strong>*</strong> Se requiere subir una fotografía igualmente para que sea visualizada en dispositivos móviles</p>
				</div>

				<section class="slider-1">
					<div class="slide">
						<div class="content col-50 relative z-index-2">
							<div class="slider-numbers-container relative clearfix">
								<div class="slider-numbers relative"></div>
								<div class="timer relative">
									<figure class="counter col-1 transform-time"></figure>
								</div>
							</div><!-- slider-numbers -->

							<p class="pre-title" data-aos="fade-right">Texto de pre-título</p>
							<p class="title" data-aos="fade-right" data-aos-delay="300">Título principal</p>
							<div class="description" data-aos="fade-right" data-aos-delay="500">
								<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
							</div>
							<a href="#" class="button" data-aos="fade-right" data-aos-delay="650" data-aos-offset="-250">Leer más</a>
						</div><!-- content -->
						<div class="veil"></div>
						<img class="photo cover zoom" src="https://source.unsplash.com/random">
					</div><!-- slide -->

					<div class="slide video">
						<div class="content col-50 relative z-index-2">
							<div class="slider-numbers-container relative clearfix">
								<div class="slider-numbers relative"></div>
								<div class="timer relative">
									<figure class="counter col-1 transform-time"></figure>
								</div>
							</div><!-- slider-numbers -->

							<p class="pre-title" data-aos="fade-right">Texto de pre-título</p>
							<p class="title" data-aos="fade-right" data-aos-delay="300">Título principal Título principal</p>
							<p class="description" data-aos="fade-right" data-aos-delay="500">Reina en mi espíritu una alegría admirable, muy parecida a las dulces alboradas de la primavera, de que gozo aquí con delicia.</p>
							<a href="#" class="button" data-aos="fade-right" data-aos-delay="650" data-aos-offset="-250">Leer más</a>
						</div><!-- content -->
						<div class="veil"></div>

						<video controls loop playsinline muted preload="none">
							<source src="https://want.cl/merlin/wp-content/uploads/2020/01/about-us.mp4" type="video/mp4" autostart="false">
						</video>
						<img class="photo cover zoom" src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-5.jpg">
					</div><!-- slide -->

					<div class="slide youtube">
						<div class="content col-50 relative z-index-2">
							<div class="slider-numbers-container relative clearfix">
								<div class="slider-numbers relative"></div>
								<div class="timer relative">
									<figure class="counter col-1 transform-time"></figure>
								</div>
							</div><!-- slider-numbers -->

							<p class="pre-title" data-aos="fade-right">Texto de pre-título</p>
							<p class="title" data-aos="fade-right" data-aos-delay="300">Título principal Título principal</p>
							<p class="description" data-aos="fade-right" data-aos-delay="500">Reina en mi espíritu una alegría admirable, muy parecida a las dulces alboradas de la primavera, de que gozo aquí con delicia.</p>
							<a href="#" class="button" data-aos="fade-right" data-aos-delay="650" data-aos-offset="-250">Leer más</a>
						</div><!-- content -->
						<div class="veil"></div>
						<iframe class="embed-player" src="https://www.youtube.com/embed/aDaOgu2CQtI?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&rel=0&showinfo=0&loop=1&playlist=aDaOgu2CQtI&start=0&mute=1&autoplay=0" frameborder="0" allowfullscreen></iframe>
						<img class="photo cover zoom" src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-5.jpg">
					</div><!-- slide -->

					<div class="slide vimeo">
						<div class="content col-50 relative z-index-2">
							<div class="slider-numbers-container relative clearfix">
								<div class="slider-numbers relative"></div>
								<div class="timer relative">
									<figure class="counter col-1 transform-time"></figure>
								</div>
							</div><!-- slider-numbers -->

							<p class="pre-title" data-aos="fade-right">Texto de pre-título</p>
							<p class="title" data-aos="fade-right" data-aos-delay="300">Título principal Título principal</p>
							<p class="description" data-aos="fade-right" data-aos-delay="500">Reina en mi espíritu una alegría admirable, muy parecida a las dulces alboradas de la primavera, de que gozo aquí con delicia.</p>
							<a href="#" class="button" data-aos="fade-right" data-aos-delay="650" data-aos-offset="-250">Leer más</a>
						</div><!-- content -->
						<div class="veil"></div>

						<iframe class="embed-player" src="https://player.vimeo.com/video/265045525?api=1&byline=0&portrait=0&title=0&background=1&mute=1&loop=1&autoplay=0&id=265045525" width="980" height="520" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						<img class="photo cover zoom" src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-5.jpg">
					</div><!-- slide -->
				</section><!-- slider-1 -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;section class="slider-1"&gt;
  &lt;div class="slide"&gt;
    &lt;div class="content col-50 relative z-index-2"&gt;
      &lt;div class="slider-numbers-container relative clearfix"&gt;
        &lt;div class="slider-numbers relative"&gt;&lt;/div&gt;
        &lt;div class="timer relative"&gt;
          &lt;figure class="counter col-1 transform-time"&gt;&lt;/figure&gt;
        &lt;/div&gt;
      &lt;/div&gt;&lt;!-- slider-numbers --&gt;

      &lt;p class="pre-title" data-aos="fade-right"&gt;Texto de pre-título&lt;/p&gt;
      &lt;p class="title" data-aos="fade-right" data-aos-delay="300"&gt;Título principal&lt;/p&gt;
      &lt;div class="description" data-aos="fade-right" data-aos-delay="500"&gt;
        &lt;p&gt;Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.&lt;/p&gt;
      &lt;/div&gt;
      &lt;a href="#" class="button" data-aos="fade-right" data-aos-delay="650" data-aos-offset="-250"&gt;Leer más&lt;/a&gt;
    &lt;/div&gt;&lt;!-- content --&gt;
    &lt;div class="veil"&gt;&lt;/div&gt;
    &lt;img class="photo cover zoom" src="https://source.unsplash.com/random"&gt;
  &lt;/div&gt;&lt;!-- slide --&gt;

  &lt;div class="slide video"&gt;
    &lt;div class="content col-50 relative z-index-2"&gt;
      &lt;div class="slider-numbers-container relative clearfix"&gt;
        &lt;div class="slider-numbers relative"&gt;&lt;/div&gt;
        &lt;div class="timer relative"&gt;
          &lt;figure class="counter col-1 transform-time"&gt;&lt;/figure&gt;
        &lt;/div&gt;
      &lt;/div&gt;&lt;!-- slider-numbers --&gt;

      &lt;p class="pre-title" data-aos="fade-right"&gt;Texto de pre-título&lt;/p&gt;
      &lt;p class="title" data-aos="fade-right" data-aos-delay="300"&gt;Título principal Título principal&lt;/p&gt;
      &lt;p class="description" data-aos="fade-right" data-aos-delay="500"&gt;Reina en mi espíritu una alegría admirable, muy parecida a las dulces alboradas de la primavera, de que gozo aquí con delicia.&lt;/p&gt;
      &lt;a href="#" class="button" data-aos="fade-right" data-aos-delay="650" data-aos-offset="-250"&gt;Leer más&lt;/a&gt;
    &lt;/div&gt;&lt;!-- content --&gt;
    &lt;div class="veil"&gt;&lt;/div&gt;

    &lt;video controls loop playsinline muted preload="none"&gt;
      &lt;source src="https://want.cl/merlin/wp-content/uploads/2020/01/about-us.mp4" type="video/mp4" autostart="false"&gt;
    &lt;/video&gt;
    &lt;div class="photo cover zoom" style="background-image:url('https://want.cl/merlin/wp-content/uploads/2019/11/merlin-5.jpg')"&gt;&lt;/div&gt;
  &lt;/div&gt;&lt;!-- slide --&gt;

  &lt;div class="slide youtube"&gt;
    &lt;div class="content col-50 relative z-index-2"&gt;
      &lt;div class="slider-numbers-container relative clearfix"&gt;
        &lt;div class="slider-numbers relative"&gt;&lt;/div&gt;
        &lt;div class="timer relative"&gt;
          &lt;figure class="counter col-1 transform-time"&gt;&lt;/figure&gt;
        &lt;/div&gt;
      &lt;/div&gt;&lt;!-- slider-numbers --&gt;

      &lt;p class="pre-title" data-aos="fade-right"&gt;Texto de pre-título&lt;/p&gt;
      &lt;p class="title" data-aos="fade-right" data-aos-delay="300"&gt;Título principal Título principal&lt;/p&gt;
      &lt;p class="description" data-aos="fade-right" data-aos-delay="500"&gt;Reina en mi espíritu una alegría admirable, muy parecida a las dulces alboradas de la primavera, de que gozo aquí con delicia.&lt;/p&gt;
      &lt;a href="#" class="button" data-aos="fade-right" data-aos-delay="650" data-aos-offset="-250"&gt;Leer más&lt;/a&gt;
    &lt;/div&gt;&lt;!-- content --&gt;
    &lt;div class="veil"&gt;&lt;/div&gt;
    &lt;iframe class="embed-player" src="https://www.youtube.com/embed/aDaOgu2CQtI?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&rel=0&showinfo=0&loop=1&playlist=aDaOgu2CQtI&start=0&mute=1&autoplay=0" frameborder="0" allowfullscreen&gt;&lt;/iframe&gt;
    &lt;div class="photo cover zoom" style="background-image:url('https://want.cl/merlin/wp-content/uploads/2019/11/merlin-5.jpg')"&gt;&lt;/div&gt;
  &lt;/div&gt;&lt;!-- slide --&gt;

  &lt;div class="slide vimeo"&gt;
    &lt;div class="content col-50 relative z-index-2"&gt;
      &lt;div class="slider-numbers-container relative clearfix"&gt;
        &lt;div class="slider-numbers relative"&gt;&lt;/div&gt;
        &lt;div class="timer relative"&gt;
          &lt;figure class="counter col-1 transform-time"&gt;&lt;/figure&gt;
        &lt;/div&gt;
      &lt;/div&gt;&lt;!-- slider-numbers --&gt;

      &lt;p class="pre-title" data-aos="fade-right"&gt;Texto de pre-título&lt;/p&gt;
      &lt;p class="title" data-aos="fade-right" data-aos-delay="300"&gt;Título principal Título principal&lt;/p&gt;
      &lt;p class="description" data-aos="fade-right" data-aos-delay="500"&gt;Reina en mi espíritu una alegría admirable, muy parecida a las dulces alboradas de la primavera, de que gozo aquí con delicia.&lt;/p&gt;
      &lt;a href="#" class="button" data-aos="fade-right" data-aos-delay="650" data-aos-offset="-250"&gt;Leer más&lt;/a&gt;
    &lt;/div&gt;&lt;!-- content --&gt;
    &lt;div class="veil"&gt;&lt;/div&gt;

    &lt;iframe class="embed-player" src="https://player.vimeo.com/video/265045525?api=1&byline=0&portrait=0&title=0&background=1&mute=1&loop=1&autoplay=0&id=265045525" width="980" height="520" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen&gt;&lt;/iframe&gt;
    &lt;div class="photo cover zoom" style="background-image:url('https://want.cl/merlin/wp-content/uploads/2019/11/merlin-5.jpg')"&gt;&lt;/div&gt;
  &lt;/div&gt;&lt;!-- slide --&gt;
&lt;/section&gt;&lt;!-- slider-1 --&gt;
					</code>
				</pre>
			</div><!-- tab-content -->
			
			
			<div id="php" class="tab-content">
				<div class="message-block text-center">
					<p>Este organismo corresponde al ACF <strong>Slider 1</strong>.</p>
				</div>

				<pre>
					<code class="html">
&lt;?php if ( have_rows( 'slider_1' ) ) : ?&gt;
  &lt;section class="slider-1 col-100 left clearfix"&gt;
    &lt;?php while ( have_rows( 'slider_1' ) ) : the_row(); ?&gt;
      &lt;?php
      $slider_1_tipo = get_sub_field( 'slider_1_tipo' );
      $slider_1_video_subir = get_sub_field( 'slider_1_video_subir' );
      $slider_1_youtube = get_sub_field( 'slider_1_youtube' );
      $slider_1_vimeo = get_sub_field( 'slider_1_vimeo' );
      $slider_1_imagen = get_sub_field( 'slider_1_imagen' );
      $slider_1_pre_titulo = get_sub_field( 'slider_1_pre_titulo' );
      $slider_1_titulo = get_sub_field( 'slider_1_titulo' );
      $slider_1_descripcion = get_sub_field( 'slider_1_descripcion' );
      $slider_1_link = get_sub_field( 'slider_1_link' );
      if( $slider_1_link ){
        $slider_1_link_url = $slider_1_link['url'];
        $slider_1_link_title = $slider_1_link['title'];
        $slider_1_link_target = $slider_1_link['target'] ? $slider_1_link['target'] : '_self';
      }
      ?&gt;
      &lt;div class="slide &lt;?php if($slider_1_tipo == 'subir-video'){ echo 'video'; }elseif($slider_1_tipo == 'youtube'){ echo 'youtube'; }elseif($slider_1_tipo == 'vimeo'){ echo 'vimeo'; } ?&gt;"&gt;
        &lt;div class="content col-50 relative z-index-2"&gt;
          &lt;div class="slider-numbers-container relative clearfix"&gt;
            &lt;div class="slider-numbers relative"&gt;&lt;/div&gt;
            &lt;div class="timer relative"&gt;
              &lt;figure class="counter col-1 transform-time"&gt;&lt;/figure&gt;
            &lt;/div&gt;
          &lt;/div&gt;&lt;!-- slider-numbers-container --&gt;

          &lt;?php if($slider_1_pre_titulo){ ?&gt;
            &lt;p class="pre-title" data-aos="fade-right"&gt;&lt;?php echo $slider_1_pre_titulo; ?&gt;&lt;/p&gt;
          &lt;?php } ?&gt;

          &lt;?php if($slider_1_titulo){ ?&gt;
            &lt;p class="title" data-aos="fade-right" data-aos-delay="300"&gt;&lt;?php echo $slider_1_titulo; ?&gt;&lt;/p&gt;
          &lt;?php } ?&gt;

          &lt;?php if($slider_1_descripcion){ ?&gt;
            &lt;div class="description" data-aos="fade-right" data-aos-delay="500"&gt;
              &lt;?php echo $slider_1_descripcion; ?&gt;
            &lt;/div&gt;
          &lt;?php } ?&gt;

          &lt;?php if($slider_1_link){ ?&gt;
            &lt;a href="&lt;?php echo $slider_1_link_url; ?&gt;" class="button" target="&lt;?php echo $slider_1_link_target; ?&gt;" data-aos="fade-right" data-aos-delay="650" data-aos-offset="-250"&gt;
              &lt;?php echo $slider_1_link_title; ?&gt;
            &lt;/a&gt;
          &lt;?php } ?&gt;
        &lt;/div&gt;&lt;!-- content --&gt;
        &lt;div class="veil"&gt;&lt;/div&gt;

        &lt;?php if($slider_1_tipo == 'youtube' && $slider_1_youtube){ ?&gt;
          &lt;iframe class="embed-player" src="https://www.youtube.com/embed/&lt;?php echo $slider_1_youtube; ?&gt;?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&rel=0&showinfo=0&loop=1&playlist=&lt;?php echo $slider_1_youtube; ?&gt;&start=0&mute=1&autoplay=0" frameborder="0" allowfullscreen&gt;&lt;/iframe&gt;
          &lt;img class="photo cover zoom" src="&lt;?php echo $slider_1_imagen; ?&gt;"&gt;
        &lt;?php } ?&gt;

        &lt;?php if($slider_1_tipo == 'vimeo' && $slider_1_vimeo){ ?&gt;
          &lt;iframe class="embed-player" src="https://player.vimeo.com/video/&lt;?php echo $slider_1_vimeo; ?&gt;?api=1&byline=0&portrait=0&title=0&background=1&mute=1&loop=1&autoplay=0&id=&lt;?php echo $slider_1_vimeo; ?&gt;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen&gt;&lt;/iframe&gt;
          &lt;img class="photo cover zoom" src="&lt;?php echo $slider_1_imagen; ?&gt;"&gt;
        &lt;?php } ?&gt;

        &lt;?php if($slider_1_tipo == 'subir-video' && $slider_1_video_subir){ ?&gt;
          &lt;video controls loop playsinline muted preload="none"&gt;
            &lt;source src="&lt;?php echo $slider_1_video_subir; ?&gt;" type="video/mp4" autostart="false"&gt;
          &lt;/video&gt;
          &lt;img class="photo cover zoom" src="&lt;?php echo $slider_1_imagen; ?&gt;"&gt;
        &lt;?php } ?&gt;

        &lt;?php if($slider_1_tipo == 'fotografia' && $slider_1_imagen){ ?&gt;
          &lt;img class="photo cover zoom" src="&lt;?php echo $slider_1_imagen; ?&gt;"&gt;
        &lt;?php } ?&gt;
      &lt;/div&gt;&lt;!-- slide --&gt;
    &lt;?php endwhile; ?&gt;
  &lt;/section&gt;&lt;!-- slider-1 --&gt;
&lt;?php endif; //slider_1 ?&gt;
					</code>
				</pre>
			</div><!-- tab-content -->
			
			<div id="jquery" class="tab-content">
				<pre>
					<code class="js">
//Ubicación: organisms/sliders/01/js/slider-1.js
$(document).ready(function() {
  var slideWrapper = $(".slider-1"),
    iframes = slideWrapper.find('.embed-player'),
    lazyImages = slideWrapper.find('.slide-image'),
    lazyCounter = 0;

  function postMessageToPlayer(player, command){
    if (player == null || command == null) return;
    player.contentWindow.postMessage(JSON.stringify(command), "*");
  }

  // When the slide is changing
  function playPauseVideo(slick, control){
    var currentSlide, slideType, startTime, player, video;

    currentSlide = slick.find(".slick-current");
    slideType = currentSlide.attr("class").split(" ")[1];
    player = currentSlide.find("iframe").get(0);
    startTime = currentSlide.data("video-start");

    if (slideType === "vimeo") {
      switch (control) {
        case "play":
          if ((startTime != null && startTime &gt; 0 ) && !currentSlide.hasClass('started')) {
            currentSlide.addClass('started');
            postMessageToPlayer(player, {
              "method": "setCurrentTime",
              "value" : startTime
            });
          }
          postMessageToPlayer(player, {
            "method": "play",
            "value" : 1
          });
          break;
        case "pause":
          postMessageToPlayer(player, {
            "method": "pause",
            "value": 1
          });
          break;
      }
    } else if (slideType === "youtube") {
      switch (control) {
        case "play":
          postMessageToPlayer(player, {
            "event": "command",
            "func": "mute"
          });
          postMessageToPlayer(player, {
            "event": "command",
            "func": "playVideo"
          });
          break;
        case "pause":
          postMessageToPlayer(player, {
            "event": "command",
            "func": "pauseVideo"
          });
          break;
      }
    } else if (slideType === "video") {
      video = currentSlide.children("video").get(0);
      if (video != null) {
        if (control === "play"){
          video.play();
        } else {
          video.pause();
        }
      }
    }
  }

// DOM Ready
$(function() {
  // Initialize
  slideWrapper.on("init", function(slick){
    slick = $(slick.currentTarget);
    setTimeout(function(){
      playPauseVideo(slick,"play");
    }, 1000);
  });
  slideWrapper.on("beforeChange", function(event, slick) {
    slick = $(slick.$slider);
    playPauseVideo(slick,"pause");
  });
  slideWrapper.on("afterChange", function(event, slick) {
    slick = $(slick.$slider);
    playPauseVideo(slick,"play");
  });
  slideWrapper.on("lazyLoaded", function(event, slick, image, imageSource) {
    lazyCounter++;
    if (lazyCounter === lazyImages.length){
      lazyImages.addClass('show');
      // slideWrapper.slick("slickPlay");
    }
  });

  //start the slider
  slideWrapper.slick({
    dots: true,
    speed: 300,
    arrows: true,
    autoplay: true,
    autoplaySpeed: 10000,
    pauseOnHover: false,
    pauseOnFocus: false,
    responsive: [
    {
      breakpoint: 851,
      settings: {
        arrows: false,
      }
    }
  ],
  }).on({
    afterChange: function(event, slick, nextSlide) {
    $(event.target).find('.slick-current').find('.counter').addClass('transform-time');
    $(event.target).find('.slick-current').find('.counter');
    $('.slick-current.slick-active .pre-title, .slick-current.slick-active .title, .slick-current.slick-active .description, .slick-current.slick-active .button').addClass('aos-animate');
    }
  }).on({
    beforeChange: function(event, slick, currentSlide) {
      $('.slider-1 .slide .pre-title, .slider-1 .slide .title, .slider-1 .slide .description, .slider-1 .slide .button').removeClass('aos-animate');
      $('.counter').removeClass('transform-time');
      $('.counter').removeClass('col-100');
    }
  })
});


  $('.slider-1').on('init', function (event, slick, direction) {
    if (!($('.slider-1 .slick-slide').length &gt; 1)) {
      $('.slick-dots').hide();
    }
  });

  var $status = $('.slider-1 .slider-numbers');
  var $slider = $('.slider-1');
  if($('.slider-1').length ){
    $('.slider-1').on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
       $(event.target).find('.slick-current').find('.counter').addClass('transform-time');
      $(event.target).find('.slick-current').find('.counter').addClass('col-100');
      var i = (currentSlide ? currentSlide : 0) + 1;
      $status.html('&lt;span class="current"&gt;0' + i + '&lt;/span&gt;&lt;span class="total"&gt;0' + slick.slideCount + '&lt;/span&gt;');
    });


  }

  $('.slider-1.slick-initialized').css({
      'opacity': '1',
      'height': 'auto'
  });

  equalOuterHeight($(".slider-1 .slide .content"));
})
					</code>
				</pre>
			</div><!-- tab-content -->
			
			<div id="sass" class="tab-content">
				<pre>
					<code class="scss">
/* Ubicación: organisms/sliders/01/scss/_slider-1.scss */
.slider-1{
  @extend .col-100;
  @extend .left;
  @extend .relative;
  @include clearfix;
  .slide{
    @extend .col-100;
    @extend .left;
    @extend .relative;
    @extend .overflow-hidden;
    @extend .relative;
    @include clearfix;
    .content{
      padding: 200px 0 100px 10%;
      color: $blanco;
      @extend .z-index-3;
      .pre-title{
        @extend .font-18;
      }
      .title{
        margin: 10px 0 20px;
        @extend .font-32;
        @extend .extra-bold;
      }
      .description{
        @extend .font-15;
        p{
          margin-bottom: 15px;
          &:last-child{
            margin-bottom: 0;
          }
        }
      }
      .button,
      a{
        background-color: $blanco;
        color: $negro;
        display: inline-block;
        margin-top: 40px;
        @extend .border-radius-xl;
        @extend .bold;
        @extend .uppercase;
        @extend .transition;
        &:hover{
          background-color: $negro;
          color: $blanco;
        }
      }
    }
    .veil{
      background-color: rgba($negro, 0.5);
    }
    .slider-numbers-container{
      width: 170px;
      height: 25px;
      margin-bottom: 50px;
      .slider-numbers{
        span{
          @extend .absolute;
          @extend .font-18;
        }
        .current{
          left: 0;
        }
        .total{
          right: 0;
        }
      }
      .timer{
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
        right: 0;
        margin: auto;
        height: 1px;
        width: 90px;
        background-color: rgba($blanco,0.25);
        figure{
          background-color: $blanco;
          height: 100%;
          margin: 0;
        }
        .counter{
          &.transform-time {
            transition-duration: 10s;
            transition-timing-function: linear;
          }
        }
      }
    }
    &.video,
    &.vimeo,
    &.youtube{
      video,
      iframe{
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        object-fit: cover;
        @include scale(1.3);
        @extend .absolute;
      }
      .photo{
        display: none;
      }
    }
  }
  ul.slick-dots{
    width: 10px;
    top: 50%;
    right: 10%;
    li{
      margin: 2.5px 0;
    }
  }
}


@media (max-width: $breakpoint-tablet){
  .slider-1{
    .slide{
      .content{
        width: 100%;
        padding: 120px 10%;
      }
      &.video,
      &.vimeo,
      &.youtube{
        iframe,
        video{
          display: none;
        }
        .photo{
          display: block;
        }
      }
    }
    ul.slick-dots{
      width: 80%;
      right: 10%;
      bottom: 50px;
      top: inherit;
      li{
        margin: 0 2.5px;
      }
    }
    .slider-numbers-container{
      margin-bottom: 25px !important;
    }
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #slider-1 -->



	<section id="slider-2">
		<div class="tabs">
			<p class="secondary-title">Slider #2</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#jquery">jQuery</a></li>
				<li><a href="#php">PHP</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<section class="slider-2">
					<div class="slide">
						<img src="https://source.unsplash.com/100x300">
					</div>
					<div class="slide">
						<img src="https://source.unsplash.com/300x100">
					</div>
					<div class="slide">
						<img src="https://source.unsplash.com/300x120">
					</div>
					<div class="slide">
						<img src="https://source.unsplash.com/120x300">
					</div>
					<div class="slide">
						<img src="https://source.unsplash.com/320x130">
					</div>
					<div class="slide">
						<img src="https://source.unsplash.com/150x320">
					</div>
					<div class="slide">
						<img src="https://source.unsplash.com/310x110">
					</div>
					<div class="slide">
						<img src="https://source.unsplash.com/340x150">
					</div>
					<div class="slide">
						<img src="https://source.unsplash.com/160x200">
					</div>
				</section><!-- slider-2 -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;section class="slider-2"&gt;
  &lt;div class="slide"&gt;
    &lt;img src="https://source.unsplash.com/100x300"&gt;
  &lt;/div&gt;
  &lt;div class="slide"&gt;
    &lt;img src="https://source.unsplash.com/300x100"&gt;
  &lt;/div&gt;
  &lt;div class="slide"&gt;
    &lt;img src="https://source.unsplash.com/300x120"&gt;
  &lt;/div&gt;
  &lt;div class="slide"&gt;
    &lt;img src="https://source.unsplash.com/120x300"&gt;
  &lt;/div&gt;
  &lt;div class="slide"&gt;
    &lt;img src="https://source.unsplash.com/320x130"&gt;
  &lt;/div&gt;
  &lt;div class="slide"&gt;
    &lt;img src="https://source.unsplash.com/150x320"&gt;
  &lt;/div&gt;
  &lt;div class="slide"&gt;
    &lt;img src="https://source.unsplash.com/310x110"&gt;
  &lt;/div&gt;
  &lt;div class="slide"&gt;
    &lt;img src="https://source.unsplash.com/340x150"&gt;
  &lt;/div&gt;
  &lt;div class="slide"&gt;
    &lt;img src="https://source.unsplash.com/160x200"&gt;
  &lt;/div&gt;
&lt;/section&gt;&lt;!-- slider-2 --&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="php" class="tab-content">
				<div class="message-block text-center">
					<p>Este organismo corresponde al ACF <strong>Slider 2</strong>.</p>
				</div>

				<pre>
					<code class="html">
&lt;?php if ( have_rows( 'slider_2' ) ) : ?&gt;
  &lt;section class="slider-2 col-100 left clearfix relative"&gt;
    &lt;?php
    while ( have_rows( 'slider_2' ) ) : the_row();
    $slider_2_imagen = get_sub_field( 'slider_2_imagen' );
    ?&gt;
      &lt;div class="slide"&gt;
        &lt;img src="&lt;?php echo $slider_2_imagen; ?&gt;"&gt;
      &lt;/div&gt;
    &lt;?php endwhile; ?&gt;
  &lt;/section&gt;&lt;!-- slider-2 --&gt;
&lt;?php endif; // slider_2 ?&gt;
					</code>
				</pre>
			</div><!-- tab-content -->
			
			<div id="jquery" class="tab-content">
				<pre>
					<code class="js">
//Ubicación: organisms/sliders/02/js/slider-2.js
$(document).ready(function() {
  $('.slider-2').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 1000,
    autoplay: false,
    slidesToShow: 4,
    slidesToScroll: 4,
    pauseOnHover: false,
    pauseOnFocus: false,
    responsive: [
    {
      breakpoint: 851,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: false
      }
    },
    {
      breakpoint: 641,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        arrows: false
      }
    },
    {
      breakpoint: 451,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
      }
    },
    ]
  });
})
					</code>
				</pre>
			</div><!-- tab-content -->
			
			<div id="sass" class="tab-content">
				<pre>
					<code class="scss">
/* Ubicación: organisms/sliders/02/scss/_slider-2.scss */
.slider-2{
  @extend .col-100;
  @extend .left;
  @extend .relative;
  @include clearfix;
  .slick-list{
    margin: 0 100px;
    .slide{
      height: 100px;
      display: flex;
      @extend .align-center;
      @extend .justify-center;
      img{
        width: 80%;
        height: 70%;
        object-fit: contain;
      }
    }
  }
  ul.slick-dots{
    text-align: center;
    width: 100%;
    margin-top: 40px;
    @extend .relative;
    li{
      background-color: rgba($negro, 0.5);
      float: none;
      display: inline-block;
      &.slick-active,
      &:hover{
        background-color: $negro;
      }
    }
  }
}

@media (max-width: $breakpoint-tablet){
  .slider-2{
    .slick-list{
      margin: 0 10%;
    }
  }
  ul.slick-dots{
    margin-top: 20px !important;
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #slider-2 -->



	<section id="slider-3">
		<div class="tabs">
			<p class="secondary-title">Slider #3</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#php">PHP</a></li>
				<li><a href="#jquery">jQuery</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<section class="slider-3-container col-100 left clearfix relative">
					<div class="slider-3 col-100 left clearfix relative">
						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="main-title">Título del módulo</p>
								<p class="year">1990</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
							</div><!-- content -->
							<div class="veil"></div>
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="main-title">Título del módulo</p>
								<p class="year">1991</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
							</div><!-- content -->
							<div class="veil"></div>
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="main-title">Título del módulo</p>
								<p class="year">1992</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
							</div><!-- content -->
							<div class="veil"></div>
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="main-title">Título del módulo</p>
								<p class="year">1993</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
							</div><!-- content -->
							<div class="veil"></div>
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="main-title">Título del módulo</p>
								<p class="year">1994</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
							</div><!-- content -->
							<div class="veil"></div>
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->
					</div><!-- slider-3 -->

					<ul class="slider-nav-3">
						<li>1990</li>
						<li>1991</li>
						<li>1992</li>
						<li>1993</li>
						<li>1994</li>
					</ul>
				</section><!-- slider-3-container -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;section class="slider-3-container col-100 left clearfix relative"&gt;
  &lt;div class="slider-3 col-100 left clearfix relative"&gt;
    &lt;div class="slide col-100 left clearfix relative overflow-hidden"&gt;
      &lt;div class="content"&gt;
        &lt;p class="main-title"&gt;Título del módulo&lt;/p&gt;
        &lt;p class="year"&gt;1990&lt;/p&gt;
        &lt;p class="title"&gt;Título principal&lt;/p&gt;
        &lt;div class="description"&gt;
          &lt;p&gt;Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.&lt;/p&gt;
        &lt;/div&gt;
      &lt;/div&gt;&lt;!-- content --&gt;
      &lt;div class="veil"&gt;&lt;/div&gt;
      &lt;img class="photo cover zoom" src="https://source.unsplash.com/random"&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;

    &lt;div class="slide col-100 left clearfix relative overflow-hidden"&gt;
      &lt;div class="content"&gt;
        &lt;p class="main-title"&gt;Título del módulo&lt;/p&gt;
        &lt;p class="year"&gt;1991&lt;/p&gt;
        &lt;p class="title"&gt;Título principal&lt;/p&gt;
        &lt;div class="description"&gt;
          &lt;p&gt;Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.&lt;/p&gt;
        &lt;/div&gt;
      &lt;/div&gt;&lt;!-- content --&gt;
      &lt;div class="veil"&gt;&lt;/div&gt;
      &lt;img class="photo cover zoom" src="https://source.unsplash.com/random"&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;

    &lt;div class="slide col-100 left clearfix relative overflow-hidden"&gt;
      &lt;div class="content"&gt;
        &lt;p class="main-title"&gt;Título del módulo&lt;/p&gt;
        &lt;p class="year"&gt;1992&lt;/p&gt;
        &lt;p class="title"&gt;Título principal&lt;/p&gt;
        &lt;div class="description"&gt;
          &lt;p&gt;Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.&lt;/p&gt;
        &lt;/div&gt;
      &lt;/div&gt;&lt;!-- content --&gt;
      &lt;div class="veil"&gt;&lt;/div&gt;
      &lt;img class="photo cover zoom" src="https://source.unsplash.com/random"&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;

    &lt;div class="slide col-100 left clearfix relative overflow-hidden"&gt;
      &lt;div class="content"&gt;
        &lt;p class="main-title"&gt;Título del módulo&lt;/p&gt;
        &lt;p class="year"&gt;1993&lt;/p&gt;
        &lt;p class="title"&gt;Título principal&lt;/p&gt;
        &lt;div class="description"&gt;
          &lt;p&gt;Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.&lt;/p&gt;
        &lt;/div&gt;
      &lt;/div&gt;&lt;!-- content --&gt;
      &lt;div class="veil"&gt;&lt;/div&gt;
      &lt;img class="photo cover zoom" src="https://source.unsplash.com/random"&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;

    &lt;div class="slide col-100 left clearfix relative overflow-hidden"&gt;
      &lt;div class="content"&gt;
        &lt;p class="main-title"&gt;Título del módulo&lt;/p&gt;
        &lt;p class="year"&gt;1994&lt;/p&gt;
        &lt;p class="title"&gt;Título principal&lt;/p&gt;
        &lt;div class="description"&gt;
          &lt;p&gt;Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.&lt;/p&gt;
        &lt;/div&gt;
      &lt;/div&gt;&lt;!-- content --&gt;
      &lt;div class="veil"&gt;&lt;/div&gt;
      &lt;img class="photo cover zoom" src="https://source.unsplash.com/random"&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;
  &lt;/div&gt;&lt;!-- slider-3 --&gt;

  &lt;ul class="slider-nav-3"&gt;
    &lt;li&gt;1990&lt;/li&gt;
    &lt;li&gt;1991&lt;/li&gt;
    &lt;li&gt;1992&lt;/li&gt;
    &lt;li&gt;1993&lt;/li&gt;
    &lt;li&gt;1994&lt;/li&gt;
  &lt;/ul&gt;
&lt;/section&gt;&lt;!-- slider-3-container --&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="php" class="tab-content">
				<div class="message-block text-center">
					<p>Este organismo corresponde al ACF <strong>Slider 3</strong>.</p>
				</div>
				<pre>
					<code class="html">
&lt;?php if ( have_rows( 'slider_3' ) ) : ?&gt;
  &lt;section class="slider-3-container col-100 left clearfix relative"&gt;
    &lt;div class="slider-3 col-100 left clearfix relative"&gt;
      &lt;?php while ( have_rows( 'slider_3' ) ) : the_row(); ?&gt;
        &lt;?php
        $slider_3_imagen = get_sub_field( 'slider_3_imagen' );
        $slider_3_pre_titulo = get_sub_field( 'slider_3_pre_titulo' );
        $slider_3_ano = get_sub_field( 'slider_3_ano' );
        $slider_3_titulo = get_sub_field( 'slider_3_titulo' );
        $slider_3_descripcion = get_sub_field( 'slider_3_descripcion' );
        $slider_3_link = get_sub_field( 'slider_3_link' );
        if( $slider_3_link ){
          $slider_3_link_url = $slider_3_link['url'];
          $slider_3_link_title = $slider_3_link['title'];
          $slider_3_link_target = $slider_3_link['target'] ? $slider_3_link['target'] : '_self';
        }
        ?&gt;
        &lt;div class="slide col-100 left clearfix relative overflow-hidden"&gt;
          &lt;div class="content"&gt;
            &lt;?php if($slider_3_pre_titulo){ ?&gt;
              &lt;p class="main-title"&gt;&lt;?php echo $slider_3_pre_titulo; ?&gt;&lt;/p&gt;
            &lt;?php } ?&gt;

            &lt;?php if($slider_3_ano){ ?&gt;
              &lt;p class="year"&gt;&lt;?php echo $slider_3_ano; ?&gt;&lt;/p&gt;
            &lt;?php } ?&gt;

            &lt;?php if($slider_3_titulo){ ?&gt;
              &lt;p class="title"&gt;&lt;?php echo $slider_3_titulo; ?&gt;&lt;/p&gt;
            &lt;?php } ?&gt;

            &lt;?php if($slider_3_descripcion){ ?&gt;
              &lt;div class="description"&gt;
                &lt;?php echo $slider_3_descripcion; ?&gt;
              &lt;/div&gt;
            &lt;?php } ?&gt;

            &lt;?php if($slider_3_link){ ?&gt;
              &lt;a href="&lt;?php echo $slider_3_link_url; ?&gt;" class="button" target="&lt;?php echo $slider_3_link_target; ?&gt;"&gt;
                &lt;?php echo $slider_3_link_title; ?&gt;
              &lt;/a&gt;
            &lt;?php } ?&gt;
          &lt;/div&gt;&lt;!-- content --&gt;
          &lt;div class="veil"&gt;&lt;/div&gt;
          &lt;?php if($slider_3_imagen){ ?&gt;
            &lt;img class="photo cover zoom" src="&lt;?php echo $slider_3_imagen; ?&gt;')"&gt;
          &lt;?php } ?&gt;
        &lt;/div&gt;&lt;!-- slide --&gt;
      &lt;?php endwhile; ?&gt;
    &lt;/div&gt;&lt;!-- slider-3 --&gt;

    &lt;ul class="slider-nav-3"&gt;
      &lt;?php
      while ( have_rows( 'slider_3' ) ) : the_row();
      $slider_3_ano = get_sub_field( 'slider_3_ano' );
      ?&gt;
        &lt;li&gt;&lt;?php echo $slider_3_ano; ?&gt;&lt;/li&gt;
      &lt;?php endwhile; ?&gt;
    &lt;/ul&gt;
  &lt;/section&gt;&lt;!-- slider-3-container --&gt;
					</code>
				</pre>
			</div><!-- tab-content -->
			
			<div id="jquery" class="tab-content">
				<pre>
					<code class="js">
//Ubicación: organisms/sliders/03/js/slider-3.js
$(document).ready(function() {
  $('.slider-3').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: false,
    speed: 700,
    arrows: true,
    asNavFor: '.slider-nav-3',
    responsive: [
    {
      breakpoint: 851,
      settings: {
        arrows: false,
      }
    }
    ]
  });

  $('.slider-nav-3').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider-3',
    dots: false,
    centerMode: true,
    arrows: false,
    focusOnSelect: true,
    vertical: true,
    responsive: [
    {
      breakpoint: 851,
      settings: {
        vertical: false,
      }
    },
    {
      breakpoint: 641,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        vertical: false,
      }
    }
    ]
  });
  equalOuterHeight($(".slider-3 .slide .content"));
})
					</code>
				</pre>
			</div><!-- tab-content -->
			
			<div id="sass" class="tab-content">
				<pre>
					<code class="scss">
/* Ubicación: organisms/sliders/03/scss/_slider-3.scss */
.slider-3-container{
  .slider-3{
    .slide{
      .content{
        padding: 200px 30% 100px 0;
        color: $blanco;
        @extend .col-65;
        @extend .z-index-3;
        @extend .relative;
        @extend .right;
        .pre-title{
          @extend .font-18;
        }
        .year{
          margin: 10px 0 20px;
          @extend .font-32;
          @extend .bold;
        }
        .title{
          @extend .font-18;
          @extend .extra-bold;
        }
        .description{
          @extend .font-15;
        }
        .button{
          background-color: $blanco;
          color: $negro;
          display: inline-block;
          margin-top: 40px;
          @extend .bold;
          @extend .uppercase;
          @extend .transition;
          &:hover{
            background-color: $negro;
            color: $blanco;
          }
        }
      }
      .veil{
        background-color: rgba($negro, 0.5);
      }
    }
    ul.slick-dots{
      width: 10px;
      top: 50%;
      right: 10%;
      li{
        margin: 2.5px 0;
      }
    }
    .slick-prev{
      left: 25%;
    }
    .slick-next{
      right: 20%;
    }
  }
  .slider-nav-3{
    color: $blanco;
    right: 6%;
    width: 10%;
    height: 100%;
    @extend .z-index-4;
    @extend .absolute;
    .slick-list{
      height: 100% !important;
      padding-left: 10px !important;
      li{
        padding: 5vh 0 5vh 15px;
        cursor: pointer;
        border-left: 1px solid rgba($blanco, 0.25);
        color: rgba($blanco, 0.25);
        @extend .font-20;
        @extend .bold;
        @extend .relative;
        @extend .transition;
        &:after{
          width: 8px;
          height: 8px;
          left: -4.5px;
          top: 6vh;
          background-color: rgba($blanco, 0.25);
          content: "";
          @extend .transition;
          @extend .absolute;
        }
        &:hover,
        &.slick-center{
          color: $blanco;
          &:after{
            background-color: $blanco;
          }
        }
      }
    }
  }
}

@media (max-width: $breakpoint-tablet){
  .slider-3-container{
    .slider-3{
      .slide{
        .content{
          padding: 120px 10% 200px;
          width: 100%;
        }
      }
    }
    .slider-nav-3{
      right: 0;
      width: 100%;
      top: inherit;
      bottom: 50px;
      height: auto;
      .slick-list{
        height: 100% !important;
        padding-left: 10px !important;
        li{
          border: none;
          padding: 20px 0 40px;
          text-align: center;
          &::after{
            left: 0;
            top: inherit;
            bottom: 15px;
            right: 0;
            margin: auto;
          }
        }
      }
      &::after{
        width: 100%;
        height: 1px;
        background-color: rgba($blanco, 0.25);
        content: "";
        bottom: 20px;
        position: absolute;
      }
    }
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #slider-3 -->




	<section id="slider-4">
		<div class="tabs">
			<p class="secondary-title">Slider #4</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#php">PHP</a></li>
				<li><a href="#jquery">jQuery</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<section class="slider-4-container">
					<ul class="slider-nav-4">
						<li>
							<div class="photography">
								<img class="cover" src="https://source.unsplash.com/random">
							</div>
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título</p>
							</div>
						</li>

						<li>
							<div class="photography">
								<img class="cover" src="https://source.unsplash.com/random">
							</div>
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título</p>
							</div>
						</li>

						<li>
							<div class="photography">
								<img class="cover" src="https://source.unsplash.com/random">
							</div>
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título</p>
							</div>
						</li>

						<li>
							<div class="photography">
								<img class="cover" src="https://source.unsplash.com/random">
							</div>
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título</p>
							</div>
						</li>

						<li>
							<div class="photography">
								<img class="cover" src="https://source.unsplash.com/random">
							</div>
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título</p>
							</div>
						</li>
					</ul>

					<div class="slider-4 col-100 left clearfix relative">
						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
								<a href="#" class="button">das</a>
							</div><!-- content -->
							<div class="veil"></div>
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
								<a href="#" class="button">das</a>
							</div><!-- content -->
							<div class="veil"></div>
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
								<a href="#" class="button">das</a>
							</div><!-- content -->
							<div class="veil"></div>
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
								<a href="#" class="button">das</a>
							</div><!-- content -->
							<div class="veil"></div>
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->

						<div class="slide col-100 left clearfix relative overflow-hidden">
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
								<a href="#" class="button">das</a>
							</div><!-- content -->
							<div class="veil"></div>
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->
					</div><!-- slider-4 -->
				</section><!-- slider-4-container -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;section class="slider-4-container"&gt;
  &lt;ul class="slider-nav-4"&gt;
    &lt;li&gt;
      &lt;div class="photography"&gt;
        &lt;img class="cover" src="https://source.unsplash.com/random"&gt;
      &lt;/div&gt;
      &lt;div class="content"&gt;
        &lt;p class="pre-title"&gt;Pre-título&lt;/p&gt;
        &lt;p class="title"&gt;Título&lt;/p&gt;
      &lt;/div&gt;
    &lt;/li&gt;

    &lt;li&gt;
      &lt;div class="photography"&gt;
        &lt;img class="cover" src="https://source.unsplash.com/random"&gt;
      &lt;/div&gt;
      &lt;div class="content"&gt;
        &lt;p class="pre-title"&gt;Pre-título&lt;/p&gt;
        &lt;p class="title"&gt;Título&lt;/p&gt;
      &lt;/div&gt;
    &lt;/li&gt;

    &lt;li&gt;
      &lt;div class="photography"&gt;
        &lt;img class="cover" src="https://source.unsplash.com/random"&gt;
      &lt;/div&gt;
      &lt;div class="content"&gt;
        &lt;p class="pre-title"&gt;Pre-título&lt;/p&gt;
        &lt;p class="title"&gt;Título&lt;/p&gt;
      &lt;/div&gt;
    &lt;/li&gt;

    &lt;li&gt;
      &lt;div class="photography"&gt;
        &lt;img class="cover" src="https://source.unsplash.com/random"&gt;
      &lt;/div&gt;
      &lt;div class="content"&gt;
        &lt;p class="pre-title"&gt;Pre-título&lt;/p&gt;
        &lt;p class="title"&gt;Título&lt;/p&gt;
      &lt;/div&gt;
    &lt;/li&gt;

    &lt;li&gt;
      &lt;div class="photography"&gt;
        &lt;img class="cover" src="https://source.unsplash.com/random"&gt;
      &lt;/div&gt;
      &lt;div class="content"&gt;
        &lt;p class="pre-title"&gt;Pre-título&lt;/p&gt;
        &lt;p class="title"&gt;Título&lt;/p&gt;
      &lt;/div&gt;
    &lt;/li&gt;
  &lt;/ul&gt;

  &lt;div class="slider-4 col-100 left clearfix relative"&gt;
    &lt;div class="slide col-100 left clearfix relative overflow-hidden"&gt;
      &lt;div class="content"&gt;
        &lt;p class="pre-title"&gt;Pre-título&lt;/p&gt;
        &lt;p class="title"&gt;Título principal&lt;/p&gt;
        &lt;div class="description"&gt;
          &lt;p&gt;Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.&lt;/p&gt;
        &lt;/div&gt;
        &lt;a href="#" class="button"&gt;das&lt;/a&gt;
      &lt;/div&gt;&lt;!-- content --&gt;
      &lt;div class="veil"&gt;&lt;/div&gt;
      &lt;img class="photo cover zoom" src="https://source.unsplash.com/random"&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;

    &lt;div class="slide col-100 left clearfix relative overflow-hidden"&gt;
      &lt;div class="content"&gt;
        &lt;p class="pre-title"&gt;Pre-título&lt;/p&gt;
        &lt;p class="title"&gt;Título principal&lt;/p&gt;
        &lt;div class="description"&gt;
          &lt;p&gt;Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.&lt;/p&gt;
        &lt;/div&gt;
        &lt;a href="#" class="button"&gt;das&lt;/a&gt;
      &lt;/div&gt;&lt;!-- content --&gt;
      &lt;div class="veil"&gt;&lt;/div&gt;
      &lt;img class="photo cover zoom" src="https://source.unsplash.com/random"&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;

    &lt;div class="slide col-100 left clearfix relative overflow-hidden"&gt;
      &lt;div class="content"&gt;
        &lt;p class="pre-title"&gt;Pre-título&lt;/p&gt;
        &lt;p class="title"&gt;Título principal&lt;/p&gt;
        &lt;div class="description"&gt;
          &lt;p&gt;Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.&lt;/p&gt;
        &lt;/div&gt;
        &lt;a href="#" class="button"&gt;das&lt;/a&gt;
      &lt;/div&gt;&lt;!-- content --&gt;
      &lt;div class="veil"&gt;&lt;/div&gt;
      &lt;img class="photo cover zoom" src="https://source.unsplash.com/random"&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;

    &lt;div class="slide col-100 left clearfix relative overflow-hidden"&gt;
      &lt;div class="content"&gt;
        &lt;p class="pre-title"&gt;Pre-título&lt;/p&gt;
        &lt;p class="title"&gt;Título principal&lt;/p&gt;
        &lt;div class="description"&gt;
          &lt;p&gt;Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.&lt;/p&gt;
        &lt;/div&gt;
        &lt;a href="#" class="button"&gt;das&lt;/a&gt;
      &lt;/div&gt;&lt;!-- content --&gt;
      &lt;div class="veil"&gt;&lt;/div&gt;
      &lt;img class="photo cover zoom" src="https://source.unsplash.com/random"&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;

    &lt;div class="slide col-100 left clearfix relative overflow-hidden"&gt;
      &lt;div class="content"&gt;
        &lt;p class="pre-title"&gt;Pre-título&lt;/p&gt;
        &lt;p class="title"&gt;Título principal&lt;/p&gt;
        &lt;div class="description"&gt;
          &lt;p&gt;Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.&lt;/p&gt;
        &lt;/div&gt;
        &lt;a href="#" class="button"&gt;das&lt;/a&gt;
      &lt;/div&gt;&lt;!-- content --&gt;
      &lt;div class="veil"&gt;&lt;/div&gt;
      &lt;img class="photo cover zoom" src="https://source.unsplash.com/random"&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;
  &lt;/div&gt;&lt;!-- slider-4 --&gt;
&lt;/section&gt;&lt;!-- slider-4-container --&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="php" class="tab-content">
				<div class="message-block text-center">
					<p>Este organismo corresponde al ACF <strong>Slider 4</strong>.</p>
				</div>
				<pre>
					<code class="html">
&lt;?php if ( have_rows( 'slider_4' ) ) : ?&gt;
  &lt;section class="slider-4-container"&gt;
    &lt;ul class="slider-nav-4"&gt;
      &lt;?php
      while ( have_rows( 'slider_4' ) ) : the_row();
      $slider_4_imagen = get_sub_field( 'slider_4_imagen' );
      $slider_4_pre_titulo = get_sub_field( 'slider_4_pre_titulo' );
      $slider_4_titulo = get_sub_field( 'slider_4_titulo' );
      ?&gt;
        &lt;li&gt;
          &lt;?php if($slider_4_imagen){ ?&gt;
            &lt;div class="photography"&gt;
              &lt;img class="cover" src="&lt;?php echo $slider_4_imagen; ?&gt;"&gt;
            &lt;/div&gt;
          &lt;?php } ?&gt;

          &lt;?php if($slider_4_pre_titulo || $slider_4_titulo){ ?&gt;
            &lt;div class="content"&gt;
              &lt;?php if($slider_4_pre_titulo){ ?&gt;
                &lt;p class="pre-title"&gt;&lt;?php echo $slider_4_pre_titulo; ?&gt;&lt;/p&gt;
              &lt;?php } ?&gt;

              &lt;?php if($slider_4_titulo){ ?&gt;
                &lt;p class="title"&gt;&lt;?php echo $slider_4_titulo; ?&gt;&lt;/p&gt;
              &lt;?php } ?&gt;
            &lt;/div&gt;
          &lt;?php } ?&gt;
        &lt;/li&gt;
      &lt;?php endwhile; ?&gt;
    &lt;/ul&gt;

    &lt;div class="slider-4"&gt;
      &lt;?php while ( have_rows( 'slider_4' ) ) : the_row(); ?&gt;
        &lt;?php
        $slider_4_imagen = get_sub_field( 'slider_4_imagen' );
        $slider_4_pre_titulo = get_sub_field( 'slider_4_pre_titulo' );
        $slider_4_titulo = get_sub_field( 'slider_4_titulo' );
        $slider_4_descripcion = get_sub_field( 'slider_4_descripcion' );
        $slider_4_link = get_sub_field( 'slider_4_link' );
        if( $slider_4_link ){
          $slider_4_link_url = $slider_4_link['url'];
          $slider_4_link_title = $slider_4_link['title'];
          $slider_4_link_target = $slider_4_link['target'] ? $slider_4_link['target'] : '_self';
        }
        ?&gt;
        &lt;div class="slide"&gt;
          &lt;div class="content"&gt;
            &lt;?php if($slider_4_pre_titulo){ ?&gt;
              &lt;p class="pre-title"&gt;&lt;?php echo $slider_4_pre_titulo; ?&gt;&lt;/p&gt;
            &lt;?php } ?&gt;

            &lt;?php if($slider_4_titulo){ ?&gt;
              &lt;p class="title"&gt;&lt;?php echo $slider_4_titulo; ?&gt;&lt;/p&gt;
            &lt;?php } ?&gt;

            &lt;?php if($slider_4_descripcion){ ?&gt;
              &lt;div class="description"&gt;
                &lt;?php echo $slider_4_descripcion; ?&gt;
              &lt;/div&gt;
            &lt;?php } ?&gt;

            &lt;?php if($slider_4_link){ ?&gt;
              &lt;a href="&lt;?php echo $slider_4_link_url; ?&gt;" class="button" target="&lt;?php echo $slider_4_link_target; ?&gt;"&gt;
                &lt;?php echo $slider_4_link_title; ?&gt;
              &lt;/a&gt;
            &lt;?php } ?&gt;
          &lt;/div&gt;&lt;!-- content --&gt;
          &lt;div class="veil"&gt;&lt;/div&gt;
          &lt;?php if($slider_4_imagen){ ?&gt;
            &lt;img class="photo cover zoom" src="&lt;?php echo $slider_4_imagen; ?&gt;"&gt;
          &lt;?php } ?&gt;
        &lt;/div&gt;&lt;!-- slide --&gt;
      &lt;?php endwhile; ?&gt;
    &lt;/div&gt;&lt;!-- slider-4 --&gt;
  &lt;/section&gt;&lt;!-- slider-4-container --&gt;
&lt;?php endif; //slider_4 ?&gt;
					</code>
				</pre>
			</div><!-- tab-content -->
			
			<div id="jquery" class="tab-content">
				<pre>
					<code class="js">
//Ubicación: organisms/sliders/04/js/slider-4.js
$(document).ready(function() {
  $('.slider-4').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: false,
    speed: 700,
    arrows: true,
    dots: true,
    asNavFor: '.slider-nav-4',
    responsive: [
    {
      breakpoint: 851,
      settings: {
        arrows: false,
      }
    }
    ]
  });

  $('.slider-nav-4').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider-4',
    dots: false,
    centerMode: true,
    arrows: false,
    focusOnSelect: true,
    responsive: [
    {
      breakpoint: 851,
      settings: {
        arrows: false,
        slidesToShow: 2,
        slidesToScroll: 1,
      }
    }
    ]
  });

  equalOuterHeight($(".slider-4 .slide .content"));
  equalOuterHeight($(".slider-nav-4 li"));
})
					</code>
				</pre>
			</div><!-- tab-content -->
			
			<div id="sass" class="tab-content">
				<pre>
					<code class="scss">
/* Ubicación: organisms/sliders/04/scss/_slider-4.scss */
.slider-4-container{
  @extend .col-100;
  @extend .left;
  @extend .relative;
  @include clearfix;
  .slider-4{
    @extend .col-100;
    @extend .left;
    @extend .relative;
    @include clearfix;
    .slide{
      @extend .col-100;
      @extend .left;
      @extend .relative;
      @extend .overflow-hidden;
      @include clearfix;
      .content{
        padding: 200px 10% 150px 0;
        color: $blanco;
        @extend .col-50;
        @extend .z-index-3;
        @extend .relative;
        @extend .right;
        .pre-title{
          @extend .font-20;
        }
        .title{
          @extend .font-32;
          @extend .extra-bold;
        }
        .description{
          @extend .font-16;
        }
        .button{
          background-color: $blanco;
          color: $negro;
          display: inline-block;
          margin-top: 40px;
          @extend .bold;
          @extend .uppercase;
          @extend .transition;
          &:hover{
            background-color: $negro;
            color: $blanco;
          }
        }
      }
      .veil{
        background-color: rgba($negro, 0.5);
      }
    }
    ul.slick-dots{
      width: 40%;
      bottom: 50px;
      right: 10%;
      li{
        margin: 0 2.5px;
      }
    }
    .slick-prev{
      left: 42.5%;
    }
    .slick-next{
      right: 2.5%;
    }
  }
  .slider-nav-4{
    color: $negro;
    @extend .col-100;
    @extend .left;
    .slick-list{
      padding: 0 !important;
      li{
        padding: 20px;
        cursor: pointer;
        color: $blanco;
        background-color: rgba($negro, 0.25);
        @extend .font-20;
        @extend .bold;
        @extend .relative;
        @extend .transition;
        @include opacity(.5);
        &:hover{
          .photography{
            @include rotate(15deg);
          }
        }
        &:hover,
        &.slick-center{
          color: $blanco;
          @include opacity(1);
        }
        &:hover,
        &.slick-center{
          .photography{
            @extend .no-grayscale;
          }
        }
        .photography{
          width: 50px;
          height: 50px;
          @extend .left;
          @extend .grayscale;
          @extend .transition-slow;
          @extend .overflow-hidden;
          @include border-radius(50px);
        }
        .content{
          width: calc(100% - 65px);
          @extend .right;
          .pre-title{
            @extend .font-12;
            @extend .regular;
          }
          .title{
            @extend .font-15;
          }
        }
      }
    }
  }
}

@media (max-width: $breakpoint-tablet){
  .slider-4-container{
    .slider-4{
      .slide{
        .content{
          padding: 120px 10% 150px;
          width: 100%;
        }
      }
    }
    ul.slick-dots{
      width: 80% !important;
    }
    ul.slider-nav-4{
      li{
        .photography{
          display: none;
        }
        .content{
          width: 100% !important;
        }
      }
    }
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #slider-4 -->


	<section id="slider-5">
		<div class="tabs">
			<p class="secondary-title">Slider #5 (igual al #4, pero con tabs)</p>
			<div id="example" class="tab-content">
				<section class="slider-5-container col-100 left clearfix relative">
					<ul class="slider-nav-5 grid-column-2">
						<li data-tab="0">
							<a href="#uno">
								<div class="photography">
									<img class="cover" src="https://source.unsplash.com/random">
								</div>
								<div class="content">
									<p class="pre-title">Pre-título</p>
									<p class="title">Título</p>
								</div>
							</a>
						</li>

						<li data-tab="1">
							<a href="#dos">
								<div class="photography">
									<img class="cover" src="https://source.unsplash.com/random">
								</div>
								<div class="content">
									<p class="pre-title">Pre-título</p>
									<p class="title">Título</p>
								</div>
							</a>
						</li>
					</ul>

					<div id="uno" class="slider-5">
						<div class="slide">
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
								<a href="#" class="button">das</a>
							</div><!-- content -->
							<div class="veil"></div>
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->

						<div class="slide">
							<div class="content">
								<p class="pre-title">Pre-título</p>
								<p class="title">Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
								<a href="#" class="button">das</a>
							</div><!-- content -->
							<div class="veil"></div>
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->
					</div><!-- #uno -->

					<div id="dos" class="slider-5">
						<div class="slide">
							<div class="content">
								<p class="pre-title">Pre-título 2</p>
								<p class="title">Título principal 2</p>
								<div class="description">
									<p>2 Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
								<a href="#" class="button">das</a>
							</div><!-- content -->
							<div class="veil"></div>
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->

						<div class="slide">
							<div class="content">
								<p class="pre-title">3 Pre-título</p>
								<p class="title">3 Título principal</p>
								<div class="description">
									<p>Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
								</div>
								<a href="#" class="button">das</a>
							</div><!-- content -->
							<div class="veil"></div>
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->
					</div><!-- #dos -->
				</section><!-- slider-5-container -->
			</div><!-- #example -->
		</div><!-- tabs-->
	</section><!-- #slider-5 -->


	<section id="slider-6">
		<div class="tabs">
			<p class="secondary-title">Slider #6</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#php">PHP</a></li>
				<li><a href="#jquery">jQuery</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block border-radius-s text-left">
					<p>Se pueden añadir 4 tipos de sliders:</p>
					<ol>
						<li>Fotografía</li>
						<li>Video de YouTube</li>
						<li>Video de Vimeo</li>
						<li>Archivo de video (.mp4)</li>
					</ol>
				</div>
				
				<section class="slider-6-container">
					<div class="slider-6">
						<div class="slide">
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->

						<div class="slide video">
							<video controls loop playsinline preload="none">
								<source src="https://want.cl/merlin/wp-content/uploads/2020/01/about-us.mp4" type="video/mp4" autostart="false">
							</video>
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->

						<div class="slide">
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->

						<div class="slide vimeo">
							<iframe class="embed-player" src="https://player.vimeo.com/video/265045525?api=1&byline=0&portrait=0&title=0&controls=1&mute=0&loop=1&autoplay=0&id=265045525" width="980" height="520" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div><!-- slide -->

						<div class="slide youtube">
							<iframe class="embed-player" src="https://www.youtube.com/embed/aDaOgu2CQtI?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&showinfo=0&loop=1&playlist=aDaOgu2CQtI&start=0&mute=0&autoplay=0&rel=0&modestbranding=1" frameborder="0" allowfullscreen></iframe>
						</div><!-- slide -->

						<div class="slide youtube">
							<iframe class="embed-player" src="https://www.youtube.com/embed/1_Uk5SrE5tE?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&rel=0&showinfo=0&loop=1&playlist=1_Uk5SrE5tE&start=0&mute=1&autoplay=0" frameborder="0" allowfullscreen></iframe>
						</div><!-- slide -->

						<div class="slide vimeo">
							<iframe class="embed-player" src="https://player.vimeo.com/video/259411563?api=1&byline=0&portrait=0&title=0&controls=1&mute=0&loop=1&autoplay=0&id=259411563" width="980" height="520" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div><!-- slide -->
					</div><!-- slider-6 -->

					<div class="slider-6-nav">
						<div class="slide">
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->

						<div class="slide">
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->

						<div class="slide">
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->

						<div class="slide">
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->

						<div class="slide">
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->

						<div class="slide">
							<img class="photo cover zoom" src="https://source.unsplash.com/random">
						</div><!-- slide -->
					</div><!-- slider-6-nav -->
				</section><!-- slider-6-container -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;section class="slider-6-container"&gt;
  &lt;div class="slider-6"&gt;
    &lt;div class="slide"&gt;
      &lt;img class="photo cover zoom" src="https://source.unsplash.com/random"&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;

    &lt;div class="slide video"&gt;
      &lt;video controls loop playsinline preload="none"&gt;
        &lt;source src="https://want.cl/merlin/wp-content/uploads/2020/01/about-us.mp4" type="video/mp4" autostart="false"&gt;
      &lt;/video&gt;
      &lt;img class="photo cover zoom" src="https://source.unsplash.com/random"&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;

    &lt;div class="slide"&gt;
      &lt;img class="photo cover zoom" src="https://source.unsplash.com/random"&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;

    &lt;div class="slide vimeo"&gt;
      &lt;iframe class="embed-player" src="https://player.vimeo.com/video/265045525?api=1&byline=0&portrait=0&title=0&controls=1&mute=0&loop=1&autoplay=0&id=265045525" width="980" height="520" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen&gt;&lt;/iframe&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;

    &lt;div class="slide youtube"&gt;
      &lt;iframe class="embed-player" src="https://www.youtube.com/embed/aDaOgu2CQtI?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&showinfo=0&loop=1&playlist=aDaOgu2CQtI&start=0&mute=0&autoplay=0&rel=0&modestbranding=1" frameborder="0" allowfullscreen&gt;&lt;/iframe&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;

    &lt;div class="slide youtube"&gt;
      &lt;iframe class="embed-player" src="https://www.youtube.com/embed/1_Uk5SrE5tE?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&rel=0&showinfo=0&loop=1&playlist=1_Uk5SrE5tE&start=0&mute=1&autoplay=0" frameborder="0" allowfullscreen&gt;&lt;/iframe&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;

    &lt;div class="slide vimeo"&gt;
      &lt;iframe class="embed-player" src="https://player.vimeo.com/video/259411563?api=1&byline=0&portrait=0&title=0&controls=1&mute=0&loop=1&autoplay=0&id=259411563" width="980" height="520" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen&gt;&lt;/iframe&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;
  &lt;/div&gt;&lt;!-- slider-6 --&gt;

  &lt;div class="slider-6-nav"&gt;
    &lt;div class="slide"&gt;
      &lt;img class="photo cover zoom" src="https://source.unsplash.com/random"&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;

    &lt;div class="slide"&gt;
      &lt;img class="photo cover zoom" src="https://source.unsplash.com/random"&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;

    &lt;div class="slide"&gt;
      &lt;img class="photo cover zoom" src="https://source.unsplash.com/random"&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;

    &lt;div class="slide"&gt;
      &lt;img class="photo cover zoom" src="https://source.unsplash.com/random"&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;

    &lt;div class="slide"&gt;
      &lt;img class="photo cover zoom" src="https://source.unsplash.com/random"&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;

    &lt;div class="slide"&gt;
      &lt;img class="photo cover zoom" src="https://source.unsplash.com/random"&gt;
    &lt;/div&gt;&lt;!-- slide --&gt;
  &lt;/div&gt;&lt;!-- slider-6-nav --&gt;
&lt;/section&gt;&lt;!-- slider-6-container --&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="php" class="tab-content">
				<div class="message-block text-center">
					<p>Este organismo corresponde al ACF <strong>Slider 6</strong>.</p>
				</div>
				
				<pre>
					<code class="html">
&lt;?php if ( have_rows( 'slider_6' ) ) : ?&gt;
  &lt;section class="slider-6-container"&gt;
    &lt;div class="slider-6"&gt;
      &lt;?php while ( have_rows( 'slider_6' ) ) : the_row(); ?&gt;
        &lt;?php
        $slider_6_tipo = get_sub_field( 'slider_6_tipo' );
        $slider_6_video_subir = get_sub_field( 'slider_6_video_subir' );
        $slider_6_youtube = get_sub_field( 'slider_6_youtube' );
        $slider_6_vimeo = get_sub_field( 'slider_6_vimeo' );
        $slider_6_imagen = get_sub_field( 'slider_6_imagen' );
        ?&gt;
        &lt;div class="slide &lt;?php if($slider_6_tipo == 'subir-video'){ echo 'video'; }elseif($slider_6_tipo == 'youtube'){ echo 'youtube'; }elseif($slider_6_tipo == 'vimeo'){ echo 'vimeo'; } ?&gt;"&gt;
          &lt;?php if($slider_6_tipo == 'youtube' && $slider_6_youtube){ ?&gt;
            &lt;iframe class="embed-player" src="https://www.youtube.com/embed/&lt;?php echo $slider_6_youtube; ?&gt;?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&rel=0&showinfo=0&loop=1&playlist=&lt;?php echo $slider_6_youtube; ?&gt;&start=0&mute=1&autoplay=0" frameborder="0" allowfullscreen&gt;&lt;/iframe&gt;
          &lt;?php } ?&gt;

          &lt;?php if($slider_6_tipo == 'vimeo' && $slider_6_vimeo){ ?&gt;
            &lt;iframe class="embed-player" src="https://player.vimeo.com/video/&lt;?php echo $slider_6_vimeo; ?&gt;?api=1&byline=0&portrait=0&title=0&background=1&mute=1&loop=1&autoplay=0&id=&lt;?php echo $slider_6_vimeo; ?&gt;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen&gt;&lt;/iframe&gt;
          &lt;?php } ?&gt;

          &lt;?php if($slider_6_tipo == 'subir-video' && $slider_6_video_subir){ ?&gt;
            &lt;video controls loop playsinline muted preload="none"&gt;
              &lt;source src="&lt;?php echo $slider_6_video_subir; ?&gt;" type="video/mp4" autostart="false"&gt;
            &lt;/video&gt;
          &lt;?php } ?&gt;

          &lt;?php if($slider_6_tipo == 'fotografia' && $slider_6_imagen){ ?&gt;
            &lt;img class="photo cover zoom" src="&lt;?php echo $slider_6_imagen; ?&gt;"&gt;
          &lt;?php } ?&gt;
        &lt;/div&gt;&lt;!-- slide --&gt;
      &lt;?php endwhile; ?&gt;
    &lt;/div&gt;&lt;!-- slider-6 --&gt;

    &lt;div class="slider-6-nav"&gt;
      &lt;?php while ( have_rows( 'slider_6' ) ) : the_row(); ?&gt;
        &lt;?php
        $slider_6_tipo = get_sub_field( 'slider_6_tipo' );
        $slider_6_video_subir = get_sub_field( 'slider_6_video_subir' );
        $slider_6_youtube = get_sub_field( 'slider_6_youtube' );
        $slider_6_vimeo = get_sub_field( 'slider_6_vimeo' );
        $slider_6_imagen = get_sub_field( 'slider_6_imagen' );
        ?&gt;
        &lt;div class="slide"&gt;
          &lt;?php if($slider_6_tipo == 'youtube' && $slider_6_youtube){ ?&gt;
            &lt;div class="veil"&gt;&lt;/div&gt;
            &lt;iframe src="https://www.youtube.com/embed/&lt;?php echo $slider_6_youtube; ?&gt;?enablejsapi=1&controls=1&fs=0&iv_load_policy=3&rel=0&showinfo=0&loop=1&playlist=&lt;?php echo $slider_6_youtube; ?&gt;&start=0&mute=1&autoplay=0" frameborder="0" allowfullscreen&gt;&lt;/iframe&gt;
          &lt;?php } ?&gt;

          &lt;?php if($slider_6_tipo == 'vimeo' && $slider_6_vimeo){ ?&gt;
            &lt;div class="veil"&gt;&lt;/div&gt;
            &lt;iframe src="https://player.vimeo.com/video/&lt;?php echo $slider_6_vimeo; ?&gt;?api=1&byline=0&portrait=0&title=0&background=1&mute=1&loop=1&autoplay=0&id=&lt;?php echo $slider_6_vimeo; ?&gt;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen&gt;&lt;/iframe&gt;
          &lt;?php } ?&gt;

          &lt;?php if($slider_6_tipo == 'subir-video' && $slider_6_video_subir){ ?&gt;
            &lt;video muted controls="false"&gt;
              &lt;source src="&lt;?php echo $slider_6_video_subir; ?&gt;" type="video/mp4" autostart="false"&gt;
            &lt;/video&gt;
            &lt;div class="veil"&gt;&lt;/div&gt;
          &lt;?php } ?&gt;

          &lt;?php if($slider_6_tipo == 'fotografia' && $slider_6_imagen){ ?&gt;
            &lt;img class="photo cover" src="&lt;?php echo $slider_6_imagen; ?&gt;"&gt;
          &lt;?php } ?&gt;
        &lt;/div&gt;&lt;!-- slide --&gt;
      &lt;?php endwhile; ?&gt;
    &lt;/div&gt;&lt;!-- slider-6-nav --&gt;
  &lt;/section&gt;&lt;!-- slider-6-container --&gt;
&lt;?php endif; //slider_6 ?&gt;
					</code>
				</pre>
			</div><!-- tab-content -->
			
			<div id="jquery" class="tab-content">
				<pre>
					<code class="js">
//Ubicación: organisms/sliders/06/js/slider-6.js
$(document).ready(function() {
  var slideWrapper = $(".slider-6"),
    iframes = slideWrapper.find('.embed-player'),
    lazyImages = slideWrapper.find('.slide-image'),
    lazyCounter = 0;

  function postMessageToPlayer(player, command){
    if (player == null || command == null) return;
    player.contentWindow.postMessage(JSON.stringify(command), "*");
  }

  // When the slide is changing
  function playPauseVideo(slick, control){
    var currentSlide, slideType, startTime, player, video;

    currentSlide = slick.find(".slick-current");
    slideType = currentSlide.attr("class").split(" ")[1];
    player = currentSlide.find("iframe").get(0);
    startTime = currentSlide.data("video-start");

    if (slideType === "vimeo") {
      switch (control) {
        case "play":
          if ((startTime != null && startTime > 0 ) && !currentSlide.hasClass('started')) {
            currentSlide.addClass('started');
            postMessageToPlayer(player, {
              "method": "setCurrentTime",
              "value" : startTime,
            });
          }
          postMessageToPlayer(player, {
            "method": "play",
            "value" : 1,
          });
          break;
        case "pause":
          postMessageToPlayer(player, {
            "method": "pause",
            "value": 1
          });
          break;
      }
    } else if (slideType === "youtube") {
      switch (control) {
        case "play":
          postMessageToPlayer(player, {
            "event": "command",
            "func": "unMute"
          });
          postMessageToPlayer(player, {
            "event": "command",
            "func": "playVideo"
          });
          break;
        case "pause":
          postMessageToPlayer(player, {
            "event": "command",
            "func": "pauseVideo"
          });
          break;
      }
    } else if (slideType === "video") {
      video = currentSlide.children("video").get(0);
      if (video != null) {
        if (control === "play"){
          video.play();
        } else {
          video.pause();
        }
      }
    }
  }

// DOM Ready
$(function() {
  // Initialize
  slideWrapper.on("init", function(slick){
    slick = $(slick.currentTarget);
    setTimeout(function(){
      playPauseVideo(slick,"play");
    }, 1000);
  });
  slideWrapper.on("beforeChange", function(event, slick) {
    slick = $(slick.$slider);
    playPauseVideo(slick,"pause");
  });
  slideWrapper.on("afterChange", function(event, slick) {
    slick = $(slick.$slider);
    playPauseVideo(slick,"play");
  });
  slideWrapper.on("lazyLoaded", function(event, slick, image, imageSource) {
    lazyCounter++;
    if (lazyCounter === lazyImages.length){
      lazyImages.addClass('show');
      // slideWrapper.slick("slickPlay");
    }
  });

  //start the slider
  slideWrapper.slick({
    dots: false,
    speed: 300,
    arrows: true,
    autoplay: false,
    autoplaySpeed: 10000,
    pauseOnHover: false,
    pauseOnFocus: false,
    asNavFor: '.slider-6-nav'
  }).on({
    afterChange: function(event, slick, nextSlide) {
    $(event.target).find('.slick-current').find('.counter').addClass('transform-time');
    $(event.target).find('.slick-current').find('.counter');
    $('.slick-current.slick-active .pre-title, .slick-current.slick-active .title, .slick-current.slick-active .description, .slick-current.slick-active .button').addClass('aos-animate');
    }
  }).on({
    beforeChange: function(event, slick, currentSlide) {
      $('.slider-6 .slide .pre-title, .slider-6 .slide .title, .slider-6 .slide .description, .slider-6 .slide .button').removeClass('aos-animate');
      $('.counter').removeClass('transform-time');
      $('.counter').removeClass('col-100');
    }
  })
});
$('.slider-6-nav').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: '.slider-6',
  dots: false,
  arrows: false,
  centerMode: true,
  focusOnSelect: true,
  responsive: [
    {
      breakpoint: 1250,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 850,
      settings: {
        slidesToShow: 2,
      }
    }
  ]
});

  $('.slider-6').on('init', function (event, slick, direction) {
    if (!($('.slider-6 .slick-slide').length > 1)) {
      $('.slick-dots').hide();
    }
  });

  var $status = $('.slider-6 .slider-numbers');
  var $slider = $('.slider-6');
  if($('.slider-6').length ){
    $('.slider-6').on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
       $(event.target).find('.slick-current').find('.counter').addClass('transform-time');
      $(event.target).find('.slick-current').find('.counter').addClass('col-100');
      var i = (currentSlide ? currentSlide : 0) + 1;
      $status.html('<span class="current">0' + i + '</span><span class="total">0' + slick.slideCount + '</span>');
    });


  }

  $('.slider-6.slick-initialized').css({
      'opacity': '1',
      'height': 'auto'
  });

  equalOuterHeight($(".slider-6 .slide .content"));
})

					</code>
				</pre>
			</div><!-- tab-content -->
			
			<div id="sass" class="tab-content">
				<pre>
					<code class="scss">
/* Ubicación: organisms/sliders/06/scss/slider-6.scss */
.slider-6-container{
  @extend .overflow-hidden;
  @extend .col-100;
  @extend .left;
  @extend .relative;
  @include clearfix;
  .slider-6{
    .slide{
      height: 70vh;
      background-color: $negro;
      @extend .col-100;
      @extend .left;
      @extend .relative;
      @extend .overflow-hidden;
      @extend .relative;
      @include clearfix;
      &.video,
      &.vimeo,
      &.youtube{
        video,
        iframe{
          height: 100%;
          object-fit: contain;
          @extend .col-100;
        }
        .photo{
          display: none;
        }
      }
    }
    ul.slick-dots{
      width: auto;
      top: 20px;
      left: 0;
      right: 0;
      @extend .text-center;
      li{
        display: inline-block;
        margin: 0 1.5px;
        float: none;
      }
    }
  }
  .slider-6-nav{
    height: 150px;
    bottom: -100px;
    left: 0;
    @extend .transition-slower;
    @extend .col-100;
    @extend .absolute;
    @extend .z-index-2;
    .slick-list,
    .slick-list .slick-track{
      height: 100%;
    }
    .slide{
      margin: 0 10px;
      cursor: pointer;
      @extend .col-100;
      @extend .left;
      @extend .relative;
      @extend .overflow-hidden;
      @include clearfix;
      iframe,
      video{
        width: 100%;
        height: 100%;
        object-fit: cover;
         pointer-events: none;
        &::-webkit-media-controls-start-playback-button{
          display: none;
        }
      }
    }
    &:hover{
      bottom: 20px;
    }
  }
}

@media (max-width: $breakpoint-phone){
  .slider-6-container{
    .slider-6-nav{
      display: none;
    }
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #slider-6 -->


	<section id="slider-7">
		<div class="tabs">
			<p class="secondary-title">Slider #7</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#php">PHP</a></li>
				<li><a href="#jquery">jQuery</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block">
					<ul>
						<li>Este organismo presenta la opción de agrupar varios elementos en un slide. Naturalmente la cantidad de ellos dependerá del diseño del proyecto y este valor puede ser modificado por el desarrollador (si es un desarrollo a la medida) ó bien, también cuenta con la opción de añadir una cifra por el editor de Wordpress.</li>
						<li>Existen dos sliders, uno visible para dispositivos de escritorio y otro para móviles.</li>
					</ul>
				</div>
				
				<section class="slider-7-container">
					<div class="slider-7 desktop">
						<div class="grid-column-3 gap-s">
							<div class="block">Bloque 1</div>
							<div class="block">Bloque 2</div>
							<div class="block">Bloque 3</div>
							<div class="block">Bloque 4</div>
							<div class="block">Bloque 5</div>
							<div class="block">Bloque 6</div>
						</div><!-- grid-column-3 -->

						<div class="grid-column-3 gap-s">
							<div class="block">Bloque 7</div>
							<div class="block">Bloque 8</div>
							<div class="block">Bloque 9</div>
							<div class="block">Bloque 10</div>
							<div class="block">Bloque 11</div>
							<div class="block">Bloque 12</div>
						</div><!-- grid-column-3 -->

						<div class="grid-column-3 gap-s">
							<div class="block">Bloque 13</div>
							<div class="block">Bloque 14</div>
							<div class="block">Bloque 15</div>
							<div class="block">Bloque 16</div>
							<div class="block">Bloque 17</div>
							<div class="block">Bloque 18</div>
						</div><!-- grid-column-3 -->

						<div class="grid-column-3 gap-s">
							<div class="block">Bloque 19</div>
							<div class="block">Bloque 20</div>
							<div class="block">Bloque 21</div>
							<div class="block">Bloque 22</div>
							<div class="block">Bloque 23</div>
							<div class="block">Bloque 24</div>
						</div><!-- grid-column-3 -->
					</div><!-- slider-7.desktop -->

					<div class="slider-7 mobile">
						<div class="block">Bloque 1</div>
						<div class="block">Bloque 2</div>
						<div class="block">Bloque 3</div>
						<div class="block">Bloque 4</div>
						<div class="block">Bloque 5</div>
						<div class="block">Bloque 6</div>
						<div class="block">Bloque 7</div>
						<div class="block">Bloque 8</div>
						<div class="block">Bloque 9</div>
						<div class="block">Bloque 10</div>
						<div class="block">Bloque 11</div>
						<div class="block">Bloque 12</div>
						<div class="block">Bloque 13</div>
						<div class="block">Bloque 14</div>
						<div class="block">Bloque 15</div>
						<div class="block">Bloque 16</div>
						<div class="block">Bloque 17</div>
						<div class="block">Bloque 18</div>
						<div class="block">Bloque 19</div>
						<div class="block">Bloque 20</div>
						<div class="block">Bloque 21</div>
						<div class="block">Bloque 22</div>
						<div class="block">Bloque 23</div>
						<div class="block">Bloque 24</div>
					</div><!-- slider-7.mobile -->
					
					<div class="slider-numbers-container relative clearfix">
						<div class="slider-numbers relative"></div>
						<figure class="line"></figure>
					</div><!-- slider-numbers -->
				</section><!-- slider-7-container -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;section class="slider-7-container"&gt;
  &lt;!-- Slider para dispositivos de escritorio --&gt;
  &lt;div class="slider-7 desktop"&gt;
    &lt;div class="grid-column-3 gap-s"&gt;
      &lt;div class="block"&gt;Bloque 1&lt;/div&gt;
      &lt;div class="block"&gt;Bloque 2&lt;/div&gt;
      &lt;div class="block"&gt;Bloque 3&lt;/div&gt;
      &lt;div class="block"&gt;Bloque 4&lt;/div&gt;
      &lt;div class="block"&gt;Bloque 5&lt;/div&gt;
      &lt;div class="block"&gt;Bloque 6&lt;/div&gt;
    &lt;/div&gt;

    &lt;div class="grid-column-3 gap-s"&gt;
      &lt;div class="block"&gt;Bloque 7&lt;/div&gt;
      &lt;div class="block"&gt;Bloque 8&lt;/div&gt;
      &lt;div class="block"&gt;Bloque 9&lt;/div&gt;
      &lt;div class="block"&gt;Bloque 10&lt;/div&gt;
      &lt;div class="block"&gt;Bloque 11&lt;/div&gt;
      &lt;div class="block"&gt;Bloque 12&lt;/div&gt;
    &lt;/div&gt;

    &lt;div class="grid-column-3 gap-s"&gt;
      &lt;div class="block"&gt;Bloque 13&lt;/div&gt;
      &lt;div class="block"&gt;Bloque 14&lt;/div&gt;
      &lt;div class="block"&gt;Bloque 15&lt;/div&gt;
      &lt;div class="block"&gt;Bloque 16&lt;/div&gt;
      &lt;div class="block"&gt;Bloque 17&lt;/div&gt;
      &lt;div class="block"&gt;Bloque 18&lt;/div&gt;
    &lt;/div&gt;

    &lt;div class="grid-column-3 gap-s"&gt;
      &lt;div class="block"&gt;Bloque 19&lt;/div&gt;
      &lt;div class="block"&gt;Bloque 20&lt;/div&gt;
      &lt;div class="block"&gt;Bloque 21&lt;/div&gt;
      &lt;div class="block"&gt;Bloque 22&lt;/div&gt;
      &lt;div class="block"&gt;Bloque 23&lt;/div&gt;
      &lt;div class="block"&gt;Bloque 24&lt;/div&gt;
    &lt;/div&gt;
  &lt;/div&gt;

  &lt;!-- Slider para dispositivos móviles --&gt;
  &lt;div class="slider-7 mobile"&gt;
    &lt;div class="block"&gt;Bloque 1&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 2&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 3&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 4&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 5&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 6&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 7&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 8&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 9&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 10&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 11&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 12&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 13&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 14&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 15&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 16&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 17&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 18&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 19&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 20&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 21&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 22&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 23&lt;/div&gt;
    &lt;div class="block"&gt;Bloque 24&lt;/div&gt;
  &lt;/div&gt;
 
  &lt;!-- Elemento que genera el contador (numérico y lineal) --&gt;  
  &lt;div class="slider-numbers-container relative clearfix"&gt;
    &lt;div class="slider-numbers relative"&gt;&lt;/div&gt;
    &lt;figure class="line"&gt;&lt;/figure&gt;
  &lt;/div&gt;
&lt;/section&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="php" class="tab-content">
				<div class="message-block text-center">
					<p>Este organismo corresponde al ACF <strong>Slider 7</strong>.</p>
				</div>
				<pre>
					<code class="html">
&lt;?php if ( get_row_layout() == 'slider_7' ) : ?&gt;
  &lt;?php
  $count = 0;
  $sliders = get_sub_field('slider_7');
  $slider_7_elementos_por_slide = get_sub_field('slider_7_elementos_por_slide');
  $total_clientes = count($sliders) - 1;

  if ( have_rows( 'slider_7' ) ) : ?&gt;
    &lt;section class="slider-7-container"&gt;
      &lt;div class="slider-7 desktop"&gt;
      &lt;?php
        foreach ($sliders as $key =&gt; $post):
          $slider_7_texto = $post['slider_7_texto'];
          if( $count &lt; $slider_7_elementos_por_slide ){
            if($count == 0){
              echo '&lt;div class="grid-column-2"&gt;';
            }
            ?&gt;
              &lt;div class="block"&gt;&lt;?php echo $slider_7_texto; ?&gt;&lt;/div&gt;
            &lt;?php
            $count++;
            if($count == $slider_7_elementos_por_slide){
              echo '&lt;/div&gt;';
              $count = 0;
            }
          }else{
            echo '&lt;/div&gt;';
            $count = 0;
          }
          if($total_clientes == $key){
            echo '&lt;/div&gt;';
          }
        endforeach;
      ?&gt;

      &lt;/div&gt;&lt;!-- slider-7.desktop --&gt;

      &lt;div class="slider-7 mobile"&gt;
        &lt;?php while ( have_rows( 'slider_7' ) ) : the_row(); ?&gt;
          &lt;div class="block"&gt;&lt;?php the_sub_field( 'slider_7_texto' ); ?&gt;&lt;/div&gt;
        &lt;?php endwhile; ?&gt;
      &lt;/div&gt;&lt;!-- slider-7.mobile --&gt;
        
      &lt;div class="slider-numbers-container relative clearfix"&gt;
        &lt;div class="slider-numbers relative"&gt;&lt;/div&gt;
        &lt;figure class="line"&gt;&lt;/figure&gt;
      &lt;/div&gt;&lt;!-- slider-numbers --&gt;
    
    &lt;/section&gt;&lt;!-- slider-7-container --&gt;
  &lt;?php endif; ?&gt;
					</code>
				</pre>
			</div><!-- tab-content -->
			
			<div id="jquery" class="tab-content">
				<pre>
					<code class="js">
//Ubicación: organisms/sliders/07/js/slider-7.js
$(document).ready(function() {
  var $sliderNumbers = $('.slider-7-container .slider-numbers-container .slider-numbers');
  $('.slider-7.desktop').on('init', function (event, slick, currentSlide, nextSlide) {
    var i = (currentSlide ? currentSlide : 0) + 1;
    initialPercent = (100/(slick.slideCount/1));
    $('.slider-7-container .slider-numbers-container .line').css('width', initialPercent+'%');
    $sliderNumbers.html('<span class="total">0' + i + '</span><span class="current">0' + slick.slideCount + '</span>');
  });

  $('.slider-7.desktop').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 1000,
    autoplay: false,
    pauseOnHover: false,
    pauseOnFocus: false,
    }).on({
      afterChange: function(event, slick, currentSlide, nextSlide) {
        i = (currentSlide ? currentSlide : 0) + 1;
        $sliderNumbers.html('<span class="total">0' + i + '</span><span class="current">0' + slick.slideCount + '</span>');
      }
    }).on({
      beforeChange: function(event, slick, currentSlide, nextSlide) {
        percent = Math.min((100*((nextSlide/1) + 1))/(slick.slideCount/1), 100);
        $('.slider-7-container .slider-numbers-container .line').css('width', percent+'%');
      }
    });

  $('.slider-7.mobile').slick({
    dots: false,
    arrows: false,
    infinite: true,
    speed: 1000,
    autoplay: false,
    pauseOnHover: false,
    pauseOnFocus: false,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
    {
      breakpoint: 550,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    },
    ],
    }).on({
      beforeChange: function(event, slick, currentSlide, nextSlide) {
        percent = Math.min((100*((nextSlide/1) + 1))/(slick.slideCount/1), 100);
        $('.slider-7-container .slider-numbers-container .line').css('width', percent+'%');
      }
  });
})
					</code>
				</pre>
			</div><!-- tab-content -->
			
			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/*Ubicación: organisms/sliders/07/scss/_slider-7.scss*/
.slider-7-container{
  @extend .col-100;
  @extend .left;
  @include clearfix;
  .slider-7{
    &.desktop.slick-initialized .slick-slide{
      display: grid;
    }
    &.mobile{
      display: none;
    }
    ul.slick-dots{
      text-align: center;
      width: 100%;
      margin-top: 40px;
      @extend .relative;
      li{
        background-color: rgba($negro, 0.5);
        float: none;
        display: inline-block;
        &.slick-active,
        &:hover{
          background-color: $negro;
        }
      }
    }
  }
  .slider-numbers-container{
    background-color: rgba($negro,0.1);
    height: 2px;
    @extend .col-100;
    @extend .relative;
    .line{
      left: 0;
      top: 0;
      height: 100%;
      background-color: $negro;
      @extend .transition-slower;
      @extend .absolute;
    }
  }
}
@media (max-width: $breakpoint-tablet){
  .slider-7-container{
    .slider-numbers{
      display: none;
    }
    .slider-7{
      &.desktop{
        display: none;
      }
      &.mobile{
        display: block !important;
      }
    }
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #slider-6 -->


	<section id="block-1">
		<div class="tabs">
			<p class="primary-title">Bloques</p>
			<p class="secondary-title">Bloque #1</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="grid-column-3 gap-l">
					<a href="#" class="block-1">
						<div class="categories">
							<span>Categoría #1</span>
							<span>Categoría #2</span>
						</div>
						<div class="content">
							<p class="date">88 de Noviembre 2020</p>
							<p class="title">Reina en mi espíritu una alegría admirable, muy parecida a las dulces alboradas</p>
							<p class="description">Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
						</div><!-- content -->
						<div class="button">Leer más</div>
						<img class="photo cover" src="https://source.unsplash.com/random">
						<div class="veil"></div>
					</a><!-- block-1 -->

					<a href="#" class="block-1">
						<div class="categories">
							<span>Categoría #1</span>
						</div>
						<div class="content">
							<p class="date">88 de Noviembre 2020</p>
							<p class="title">Reina en mi espíritu una alegría admirable</p>
							<p class="description">Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
						</div><!-- content -->
						<div class="button">Leer más</div>
						<img class="photo cover" src="https://source.unsplash.com/random">
						<div class="veil"></div>
					</a><!-- block-1 -->

					<a href="#" class="block-1">
						<div class="categories">
							<span>Categoría #2</span>
						</div>
						<div class="content">
							<p class="date">88 de Noviembre 2020</p>
							<p class="title">Ahora no sabría dibujar, ni siquiera hacer una línea con el lápiz; y, sin embargo, jamás he sido mejor pintor</p>
							<p class="description">Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.</p>
						</div><!-- content -->
						<div class="button">Leer más</div>
						<img class="photo cover" src="https://source.unsplash.com/random">
						<div class="veil"></div>
					</a><!-- block-1 -->
				</div>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;a href="#" class="block-1"&gt;
  &lt;div class="categories"&gt;
    &lt;span&gt;Categoría #1&lt;/span&gt;
    &lt;span&gt;Categoría #2&lt;/span&gt;
  &lt;/div&gt;
  &lt;div class="content"&gt;
    &lt;p class="date"&gt;88 de Noviembre 2020&lt;/p&gt;
    &lt;p class="title"&gt;Reina en mi espíritu una alegría admirable, muy parecida a las dulces alboradas&lt;/p&gt;
    &lt;p class="description"&gt;Estoy solo, y me felicito de vivir en este país, el más a propósito para almas como la mía, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que no me ocupo de mi arte.&lt;/p&gt;
  &lt;/div&gt;&lt;!-- content --&gt;
  &lt;div class="button"&gt;Leer más&lt;/div&gt;
  &lt;img class="photo cover" src="https://source.unsplash.com/random"&gt;
  &lt;div class="veil"&gt;&lt;/div&gt;
&lt;/a&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="scss">
/*Ubicación: organisms/blocks/01/scss/_block-1.scss*/
.block-1{
  padding: 230px 5% 40px;
  color: $blanco;
  @extend .overflow-hidden;
  @extend .relative;
  @include clearfix;
  .content{
    @extend .z-index-3;
    @extend .relative;
    @extend .transition-slower;
    .date{
      @extend .font-14;
    }
    .title{
      @extend .font-21;
      @extend .bold;
    }
    .description{
      margin-top: 10px;
      @include opacity(0);
      @extend .transition;
      @extend .absolute;
    }
  }
  .button{
    background-color: $blanco;
    color: $negro;
    bottom: 0;
    left: 5%;
    @extend .transition-slow;
    @extend .absolute;
    @extend .z-index-3;
    @include opacity(0);
  }
  .photo{
    @extend .transition-slower;
  }
  .categories{
    width: 100%;
    padding: 20px 5% 0;
    top: 0;
    left: 0;
    @extend .transition;
    @extend .absolute;
    @extend .z-index-3;
    span{
      background-color: $blanco;
      color: $negro;
      margin: 0 5px 5px 0;
      padding: 5px 10px;
      @extend .left;
      @extend .uppercase;
      @extend .bold;
      @extend .font-10;
    }
  }
  .veil{
    background-color: rgba($negro, 0.4);
    @extend .transition-slow;
  }
  &:hover{
    .content{
      @include translateY(calc(-100% - 50px));
      .description{
        @include opacity(1);
      }
    }
    .button{
      bottom: 20px;
      @include opacity(1);
    }
    .photo{
      @include scale(1.2);
    }
    .categories{
      @include opacity(0);
    }
    .veil{
      background-color: rgba($negro, 0.7);
    }
  }
}

@media (max-width: $breakpoint-tablet){
  .block-1{
    .content{
      @include translateY(-50px);
      .description{
        display: none;
      }
    }
    .button{
      opacity: 1 !important;
      bottom: 20px;
    }
    .categories{
      span{
        font-size: 10px;
      }
    }
    &:hover{
      .content{
        @include translateY(-50px);
      }
      .button{
      }
    }
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #block-1 -->




	<section id="block-2">
		<div class="tabs">
			<p class="secondary-title">Bloque #2</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#php">PHP</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block border-radius-s text-center">
					<p>Se puede posicionar la columna principal de texto hacia la izquierda o derecha, agregando la clase <strong>.align-left</strong> ó <strong>.align-right</strong> al elemento de clase <strong>.block-2</strong>.</p>
				</div>

				<section class="block-2 align-left">
					<div class="content-mini">
						<img src="https://source.unsplash.com/100/50">
						<p class="pre-title">Jamás he sido mejor pintor</p>
						<p class="title">Reina en mi espíritu una alegría admirable, muy parecida.</p>
					</div><!-- content-mini -->

					<div class="content">
						<p class="pre-title">Jamás he sido mejor pintor Cuando el valle se vela</p>
						<p class="title">Reina en mi espíritu una alegría admirable, muy parecida a las dulces alboradas de la primavera, de que gozo aquí con delicia.</p>
						<div class="description">
							<p>Ahora no sabría dibujar, ni siquiera hacer una línea con el lápiz; y, sin embargo, jamás he sido mejor pintor Cuando el valle se vela en torno mío con un encaje de vapores; cuando el sol de mediodía centellea sobre la impenetrable sombra de mi bosque sin conseguir otra cosa que filtrar entre las hojas algunos rayos que penetran hasta el fondo del santuario.</p>
						</div><!-- description -->
						<div class="button">Leer más</div>
					</div><!-- content -->

					<img class="photo cover" src="https://source.unsplash.com/random">
					<div class="veil"></div>
				</section><!-- block-2 -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;section class="block-2 align-left"&gt;
  &lt;div class="content-mini"&gt;
    &lt;img src="https://source.unsplash.com/100/50"&gt;
    &lt;p class="pre-title"&gt;Jamás he sido mejor pintor&lt;/p&gt;
    &lt;p class="title"&gt;Reina en mi espíritu una alegría admirable, muy parecida.&lt;/p&gt;
  &lt;/div&gt;&lt;!-- content-mini --&gt;

  &lt;div class="content"&gt;
    &lt;p class="pre-title"&gt;Jamás he sido mejor pintor Cuando el valle se vela&lt;/p&gt;
    &lt;p class="title"&gt;Reina en mi espíritu una alegría admirable, muy parecida a las dulces alboradas de la primavera, de que gozo aquí con delicia.&lt;/p&gt;
    &lt;div class="description"&gt;
      &lt;p&gt;Ahora no sabría dibujar, ni siquiera hacer una línea con el lápiz; y, sin embargo, jamás he sido mejor pintor Cuando el valle se vela en torno mío con un encaje de vapores; cuando el sol de mediodía centellea sobre la impenetrable sombra de mi bosque sin conseguir otra cosa que filtrar entre las hojas algunos rayos que penetran hasta el fondo del santuario.&lt;/p&gt;
    &lt;/div&gt;&lt;!-- description --&gt;
    &lt;div class="button"&gt;Leer más&lt;/div&gt;
  &lt;/div&gt;&lt;!-- content --&gt;

  &lt;img class="photo cover" src="https://source.unsplash.com/random"&gt;
  &lt;div class="veil"&gt;&lt;/div&gt;
&lt;/section&gt;&lt;!-- block-2 --&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="php" class="tab-content">
				<div class="message-block text-center">
					<p>Este organismo corresponde al ACF <strong>Bloque 2</strong>.</p>
				</div>
				<pre>
					<code class="html">
&lt;?php if ( have_rows( 'bloque_2' ) ) : ?&gt;
  &lt;?php
  while ( have_rows( 'bloque_2' ) ) : the_row();
  $bloque_2_imagen_de_fondo = get_sub_field( 'bloque_2_imagen_de_fondo' );
  $bloque_2_disposicion = get_sub_field( 'bloque_2_disposicion' );

  //Contenido principal
  $bloque_2_contenido_pre_titulo = get_sub_field( 'bloque_2_contenido_pre_titulo' );
  $bloque_2_contenido_titulo = get_sub_field( 'bloque_2_contenido_titulo' );
  $bloque_2_contenido_descripcion = get_sub_field( 'bloque_2_contenido_descripcion' );
  $bloque_2_contenido_link = get_sub_field( 'bloque_2_contenido_link' );
  if( $bloque_2_contenido_link ){
    $bloque_2_contenido_link_url = $bloque_2_contenido_link['url'];
    $bloque_2_contenido_link_title = $bloque_2_contenido_link['title'];
    $bloque_2_contenido_link_target = $bloque_2_contenido_link['target'] ? $bloque_2_contenido_link['target'] : '_self';
  }

  //Contenido mini
  $bloque_2_contenido_mini_imagen = get_sub_field( 'bloque_2_contenido_mini_imagen' );
  $bloque_2_contenido_mini_pre_titulo = get_sub_field( 'bloque_2_contenido_mini_pre_titulo' );
  $bloque_2_contenido_mini_titulo = get_sub_field( 'bloque_2_contenido_mini_titulo' );
  ?&gt;

    &lt;section class="block-2 &lt;?php echo $bloque_2_disposicion; ?&gt;"&gt;
      &lt;?php if($bloque_2_contenido_mini_imagen || $bloque_2_contenido_mini_pre_titulo || $bloque_2_contenido_mini_titulo){ ?&gt;
        &lt;div class="content-mini"&gt;
          &lt;?php if($bloque_2_contenido_mini_imagen) { ?&gt;
            &lt;img src="&lt;?php echo $bloque_2_contenido_mini_imagen; ?&gt;"&gt;
          &lt;?php } ?&gt;

          &lt;?php if($bloque_2_contenido_mini_pre_titulo){ ?&gt;
            &lt;p class="pre-title"&gt;&lt;?php echo $bloque_2_contenido_mini_pre_titulo; ?&gt;&lt;/p&gt;
          &lt;?php } ?&gt;

          &lt;?php if($bloque_2_contenido_mini_titulo){ ?&gt;
            &lt;p class="title"&gt;&lt;?php echo $bloque_2_contenido_mini_titulo; ?&gt;&lt;/p&gt;
          &lt;?php } ?&gt;
        &lt;/div&gt;&lt;!-- content-mini --&gt;
      &lt;?php } ?&gt;

      &lt;div class="content"&gt;
        &lt;?php if($bloque_2_contenido_pre_titulo){ ?&gt;
          &lt;p class="pre-title"&gt;&lt;?php echo $bloque_2_contenido_pre_titulo; ?&gt;&lt;/p&gt;
        &lt;?php } ?&gt;

        &lt;?php if($bloque_2_contenido_titulo){ ?&gt;
          &lt;p class="title"&gt;&lt;?php echo $bloque_2_contenido_titulo; ?&gt;&lt;/p&gt;
        &lt;?php } ?&gt;

        &lt;?php if($bloque_2_contenido_descripcion){ ?&gt;
          &lt;div class="description"&gt;
            &lt;?php echo $bloque_2_contenido_descripcion; ?&gt;
          &lt;/div&gt;&lt;!-- description --&gt;
        &lt;?php } ?&gt;

        &lt;?php if($bloque_2_contenido_link){ ?&gt;
          &lt;a href="&lt;?php echo $bloque_2_contenido_link_url; ?&gt;" class="button" target="&lt;?php echo $bloque_2_contenido_link_target; ?&gt;"&gt;
            &lt;?php echo $bloque_2_contenido_link_title; ?&gt;
          &lt;/a&gt;
        &lt;?php } ?&gt;
      &lt;/div&gt;&lt;!-- content --&gt;

      &lt;?php if($bloque_2_imagen_de_fondo){ ?&gt;
        &lt;img class="photo cover" src="&lt;?php echo $bloque_2_imagen_de_fondo; ?&gt;"&gt;
      &lt;?php } ?&gt;

      &lt;div class="veil"&gt;&lt;/div&gt;
    &lt;/section&gt;&lt;!-- block-2 --&gt;
  &lt;?php endwhile; ?&gt;
&lt;?php endif; //bloque_2 ?&gt;
					</code>
				</pre>
			</div><!-- tab-content -->
			
			<div id="sass" class="tab-content">
				<pre>
					<code class="scss">
/*Ubicación: organisms/blocks/02/scss/_block-2.scss*/
.block-2{
  color: $blanco;
  @extend .relative;
  @extend .clearfix;
  @extend .col-100;
  @extend .left;
  .content,
  .content-mini{
    padding: 180px 10% 70px;
    @extend .z-index-3;
    @extend .relative;
  }
  .content{
    @extend .col-60;
    .pre-title{
      @extend .font-18;
    }
    .title{
      @extend .bold;
      @extend .font-28;
    }
    .description{
      margin-top: 20px;
      @extend .font-16;
    }
    .button{
      background-color: $blanco;
      color: $negro;
      display: inline-block;
      margin-top: 20px;
    }
  }
  .content-mini{
    @extend .col-35;
    img{
      width: 100%;
      height: auto;
      margin-bottom: 20px;
    }
    .pre-title{
      @extend .font-16;
    }
    .title{
      @extend .font-21;
      @extend .bold;
    }
  }
  &.align-left{
    .content{
      padding-right: 0;
      @extend .left;
    }
    .content-mini{
      padding-left: 0;
      @extend .right;
      @extend .text-right;
    }
  }
  &.align-right{
    .content{
      padding-left: 0;
      @extend .text-right;
      @extend .right;
    }
    .content-mini{
      padding-right: 0;
      @extend .left;
    }
  }
  .veil{
    background-color: rgba($negro, 0.6);
  }
}

@media (max-width: $breakpoint-tablet){
  .block-2{
    .content,
    .content-mini{
      width: 90%;
      text-align: left !important;
    }
    .content{
      padding-top: 50px;
    }
    .content-mini{
      padding-top: 80px !important;
      padding-bottom: 0;
      img{
        max-height: 150px;
        width: auto;
      }
    }
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #block-2 -->



	<section id="block-3">
		<div class="tabs">
			<p class="secondary-title">Bloque #3</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#php">PHP</a></li>
				<li><a href="#jquery">jQuery</a></li>
				<li><a href="#countup">CountUp.js</a></li>
				<li><a href="#inview">InView.js</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block">
					<ul>
						<li>Para utilizar este módulo, es necesario contar con los scripts <a href="<?php echo get_stylesheet_directory_uri(); ?>/organisms/blocks/03/js/count-up.js" target="_blank">count-up.js</a>, <a href="<?php echo get_stylesheet_directory_uri(); ?>/organisms/blocks/03/js/in-view.js" target="_blank">in-view.js</a>, además del <a href="<?php echo get_stylesheet_directory_uri(); ?>/organisms/blocks/03/js/block-3.js" target="_blank">script propio</a> del módulo.</li>
						<li>
							Existen 3 tipos de visualizaciones:
							<ul>
								<li>Sin ícono ni imagen (la primera columna)</li>
								<li>Con ícono de la familia iconográfica (la segunda columna)</li>
								<li>Con imagen (la tercera columna)</li>
							</ul>
						</li>
					</ul>
				</div>

				<section class="block-3-container">
				  <div class="main-content">
				    <p class="pre-title">pre titulo bloque 3</p>
				    <p class="title">titulo bloque 3</p>
				    <div class="description">
				      <p>Ahora no sabría dibujar, ni siquiera hacer una línea con el lápiz; y, sin embargo, jamás he sido mejor pintor Cuando el valle se vela en torno mío con un encaje de vapores; cuando el sol de mediodía centellea sobre la impenetrable sombra de mi bosque sin conseguir otra cosa que filtrar entre las hojas algunos rayos que penetran hasta el fondo del santuario.</p>
				    </div>
				  </div>
				
				  <div class="gap-l grid-column-3">
				    <div class="block-3">
				      <div class="content">
				        <div class="counter-item">
				          <p class="count-container">
				            <span>USD</span>
				            <span class="count">128</span>
				            <span>%</span>
				          </p>
				          <p class="title">Título número uno</p>
				          <div class="post-title">
				            <p>Texto de descripción uno</p>
				          </div>
				        </div><!-- counter-item -->
				      </div><!-- content -->
				    </div><!-- block-3 -->
				    
				    <div class="block-3">
				      <div class="content">
				        <div class="counter-item">
				          <i class="icon-spotify"></i>
				          <p class="count-container">
				            <span class="count">M10.293</span>
				          </p>
				          <p class="title">Título número dos</p>
				          <div class="post-title">
				            <p>Texto de descripción dos</p>
				          </div>
				        </div><!-- counter-item -->
				      </div><!-- content -->
				    </div><!-- block-3 -->
				    
				    <div class="block-3">
				      <div class="content">
				        <div class="counter-item">
				          <img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-4.jpg">
				          <p class="count-container">
				            <span>MM</span>
				            <span class="count">29</span>
				            <span>USD</span>
				          </p>
				          <p class="title">Título número tres</p>
				          <div class="post-title">
				            <p>Texto de descripción tres</p>
				          </div>
				        </div><!-- counter-item -->
				      </div><!-- content -->
				    </div><!-- block-3 -->
				  </div><!-- grid-column-x -->
				
				  <img class="photo cover" src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-3.jpg">
				  <div class="veil"></div>
				</section>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;section class="block-3-container"&gt;
  &lt;div class="main-content"&gt;
    &lt;p class="pre-title"&gt;pre titulo bloque 3&lt;/p&gt;
    &lt;p class="title"&gt;titulo bloque 3&lt;/p&gt;
    &lt;div class="description"&gt;
      &lt;p&gt;Ahora no sabría dibujar, ni siquiera hacer una línea con el lápiz; y, sin embargo, jamás he sido mejor pintor Cuando el valle se vela en torno mío con un encaje de vapores; cuando el sol de mediodía centellea sobre la impenetrable sombra de mi bosque sin conseguir otra cosa que filtrar entre las hojas algunos rayos que penetran hasta el fondo del santuario.&lt;/p&gt;
    &lt;/div&gt;
  &lt;/div&gt;

  &lt;div class="gap-l grid-column-3"&gt;
    &lt;div class="block-3"&gt;
      &lt;div class="content"&gt;
        &lt;div class="counter-item"&gt;
          &lt;p class="count-container"&gt;
            &lt;span&gt;USD&lt;/span&gt;
            &lt;span class="count"&gt;128&lt;/span&gt;
            &lt;span&gt;%&lt;/span&gt;
          &lt;/p&gt;
          &lt;p class="title"&gt;Título número uno&lt;/p&gt;
          &lt;div class="post-title"&gt;
            &lt;p&gt;Texto de descripción uno&lt;/p&gt;
          &lt;/div&gt;
        &lt;/div&gt;&lt;!-- counter-item --&gt;
      &lt;/div&gt;&lt;!-- content --&gt;
    &lt;/div&gt;&lt;!-- block-3 --&gt;
    
    &lt;div class="block-3"&gt;
      &lt;div class="content"&gt;
        &lt;div class="counter-item"&gt;
          &lt;i class="icon-spotify"&gt;&lt;/i&gt;
          &lt;p class="count-container"&gt;
            &lt;span class="count"&gt;M10.293&lt;/span&gt;
          &lt;/p&gt;
          &lt;p class="title"&gt;Título número dos&lt;/p&gt;
          &lt;div class="post-title"&gt;
            &lt;p&gt;Texto de descripción dos&lt;/p&gt;
          &lt;/div&gt;
        &lt;/div&gt;&lt;!-- counter-item --&gt;
      &lt;/div&gt;&lt;!-- content --&gt;
    &lt;/div&gt;&lt;!-- block-3 --&gt;
    
    &lt;div class="block-3"&gt;
      &lt;div class="content"&gt;
        &lt;div class="counter-item"&gt;
          &lt;img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-4.jpg"&gt;
          &lt;p class="count-container"&gt;
            &lt;span&gt;MM&lt;/span&gt;
            &lt;span class="count"&gt;29&lt;/span&gt;
            &lt;span&gt;USD&lt;/span&gt;
          &lt;/p&gt;
          &lt;p class="title"&gt;Título número tres&lt;/p&gt;
          &lt;div class="post-title"&gt;
            &lt;p&gt;Texto de descripción tres&lt;/p&gt;
          &lt;/div&gt;
        &lt;/div&gt;&lt;!-- counter-item --&gt;
      &lt;/div&gt;&lt;!-- content --&gt;
    &lt;/div&gt;&lt;!-- block-3 --&gt;
  &lt;/div&gt;&lt;!-- grid-column-x --&gt;

  &lt;img class="photo cover" src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-3.jpg"&gt;
  &lt;div class="veil"&gt;&lt;/div&gt;
&lt;/section&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="php" class="tab-content">
				<div class="message-block text-center">
					<p>Este organismo corresponde al ACF <strong>Bloque 3</strong>.</p>
				</div>
				<pre>
					<code class="html">
&lt;section class="block-3-container"&gt;
  &lt;?php
  $bloque_3_columnas = get_sub_field( 'bloque_3_columnas' );
  $bloque_3_imagen = get_sub_field( 'bloque_3_imagen' );
  $bloque_3_pre_titulo = get_sub_field( 'bloque_3_pre_titulo' );
  $bloque_3_titulo = get_sub_field( 'bloque_3_titulo' );
  $bloque_3_descripcion = get_sub_field( 'bloque_3_descripcion' );
  $bloque_3_columnas = get_sub_field( 'bloque_3_columnas' );
  if($bloque_3_pre_titulo || $bloque_3_titulo || $bloque_3_descripcion){
  ?&gt;
    &lt;div class="main-content"&gt;
      &lt;?php if($bloque_3_pre_titulo){ ?&gt;
        &lt;p class="pre-title"&gt;&lt;?php echo $bloque_3_pre_titulo; ?&gt;&lt;/p&gt;
      &lt;?php } ?&gt;

      &lt;?php if($bloque_3_titulo){ ?&gt;
        &lt;p class="title"&gt;&lt;?php echo $bloque_3_titulo; ?&gt;&lt;/p&gt;
      &lt;?php } ?&gt;

      &lt;?php if($bloque_3_descripcion){ ?&gt;
        &lt;div class="description"&gt;
          &lt;?php echo $bloque_3_descripcion; ?&gt;
        &lt;/div&gt;
      &lt;?php } ?&gt;
    &lt;/div&gt;
  &lt;?php } ?&gt;

  &lt;?php if ( have_rows( 'bloque_3_cifras' ) ) : ?&gt;
    &lt;div class="gap-l &lt;?php echo $bloque_3_columnas; ?&gt;"&gt;
      &lt;?php
      while ( have_rows( 'bloque_3_cifras' ) ) : the_row();
      $bloque_3_cifras_tipo = get_sub_field( 'bloque_3_cifras_tipo' );
      $bloque_3_cifras_icono = get_sub_field( 'bloque_3_cifras_icono' );
      $bloque_3_cifras_imagen = get_sub_field( 'bloque_3_cifras_imagen' );
      $bloque_3_cifras_precifra = get_sub_field( 'bloque_3_cifras_precifra' );
      $bloque_3_cifras_cifra = get_sub_field( 'bloque_3_cifras_cifra' );
      $bloque_3_cifras_postcifra = get_sub_field( 'bloque_3_cifras_postcifra' );
      $bloque_3_cifras_titulo = get_sub_field( 'bloque_3_cifras_titulo' );
      $bloque_3_cifras_post_titulo = get_sub_field( 'bloque_3_cifras_post_titulo' );
      ?&gt;
      &lt;div class="block-3"&gt;
        &lt;div class="content"&gt;
          &lt;?php if($bloque_3_cifras_precifra || $bloque_3_cifras_cifra || $bloque_3_cifras_postcifra || $bloque_3_cifras_titulo || $bloque_3_cifras_post_titulo){ ?&gt;
            &lt;div class="counter-item"&gt;
              &lt;?php if($bloque_3_cifras_tipo == 'cifra-icono' && $bloque_3_cifras_icono){ ?&gt;
                &lt;i class="&lt;?php echo $bloque_3_cifras_icono; ?&gt;"&gt;&lt;/i&gt;
              &lt;?php } ?&gt;

              &lt;?php if($bloque_3_cifras_tipo == 'cifra-imagen' && $bloque_3_cifras_imagen){ ?&gt;
                &lt;img src="&lt;?php echo $bloque_3_cifras_imagen; ?&gt;"&gt;
              &lt;?php } ?&gt;

              &lt;?php if($bloque_3_cifras_precifra || $bloque_3_cifras_cifra || $bloque_3_cifras_postcifra){ ?&gt;
                &lt;p class="count-container"&gt;
                  &lt;?php if($bloque_3_cifras_precifra){ ?&gt;
                    &lt;span&gt;&lt;?php echo $bloque_3_cifras_precifra; ?&gt;&lt;/span&gt;
                  &lt;?php } ?&gt;

                  &lt;?php if($bloque_3_cifras_cifra){ ?&gt;
                    &lt;span class="count"&gt;&lt;?php echo $bloque_3_cifras_cifra; ?&gt;&lt;/span&gt;
                  &lt;?php } ?&gt;

                  &lt;?php if($bloque_3_cifras_postcifra){ ?&gt;
                    &lt;span&gt;&lt;?php echo $bloque_3_cifras_postcifra; ?&gt;&lt;/span&gt;
                  &lt;?php } ?&gt;
                &lt;/p&gt;
              &lt;?php } ?&gt;

              &lt;?php if($bloque_3_cifras_titulo){ ?&gt;
                &lt;p class="title"&gt;&lt;?php echo $bloque_3_cifras_titulo; ?&gt;&lt;/p&gt;
              &lt;?php } ?&gt;

              &lt;?php if($bloque_3_cifras_post_titulo){ ?&gt;
                &lt;div class="post-title"&gt;&lt;?php echo $bloque_3_cifras_post_titulo; ?&gt;&lt;/div&gt;
              &lt;?php } ?&gt;
            &lt;/div&gt;&lt;!-- counter-item --&gt;
          &lt;?php } ?&gt;
        &lt;/div&gt;&lt;!-- content --&gt;
      &lt;/div&gt;&lt;!-- block-3 --&gt;
      &lt;?php endwhile; ?&gt;
    &lt;/div&gt;&lt;!-- grid-column-x --&gt;
  &lt;?php endif; //bloque_3_cifras ?&gt;

  &lt;?php if($bloque_3_imagen){ ?&gt;
    &lt;img class="photo cover" src="&lt;?php echo $bloque_3_imagen; ?&gt;"&gt;
  &lt;?php } ?&gt;
  &lt;div class="veil"&gt;&lt;/div&gt;
&lt;/section&gt;&lt;!-- block-3-container --&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="jquery" class="tab-content">
				<pre>
					<code class="js">
// Ubicación: organisms/blocks/03/js/block-03.js
$(document).ready(function() {
  var i = 0;
  $( ".count" ).each(function() {
    var idCounter = "counter"+ ++i;
    var cifra1 = $(this).text(); // Cifra ingresada back
    var primerCaracter = cifra1.charAt(0); // Primer carácter de la cifra
    var ultimoCaracter = cifra1.charAt(cifra1.length-1);  // Úlitmo carácter de la cifra
    var cifraClean = cifra1.replace(/[^0-9]/g,''); // Cifra sanitizada (solo números)
    var CifraConPunto = cifra1.replace(/,/g, '.'); // Cifra con coma pasada a puntos
    var StrAfterComa = cifra1.substr(cifra1.indexOf(",") + 1); // string despues de la coma
    var StrAfterComaClean = StrAfterComa.replace(/[^0-9]/g,''); // string sanitizado despues de la coma
    var decimalPlaces = StrAfterComaClean.replace(/ /g,'').length // número de digitos después de la coma

    //Si el primer caracter es numérico pasa a ser prefijo :
    if($.isNumeric(primerCaracter)){
        var prefix = "";
    }else {
        var prefix = primerCaracter;
    }

    //Si el último caracter es numérico pasa a ser sufijo :
    if($.isNumeric(ultimoCaracter)){
        var sufix = "";
    }else {
        var sufix = ultimoCaracter;
    }

    //Si la cifra contiene una coma :
    if (cifra1.indexOf(',') !== -1){
        var separator = "";
        var numeroDecimales = decimalPlaces;
        var decimal = ","
        var CifraFinal = CifraConPunto
        // tiene ,
    }else {
        var separator = ".";
        var numeroDecimales = "";
        var decimal = "";
        var CifraFinal = cifraClean
    }

    // counter, cifrainicial, cifrafinal, numerodecimales, duración
    var c = new CountUp(this,0,CifraFinal,numeroDecimales,0,{
        separator: separator,
        decimal: decimal,
        prefix: prefix,
        suffix: sufix

    });

    $(this).one('inview', function(event, isInView) {
      if (isInView) {
        c.start();
      }//endif
    });

  });
})
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="countup" class="tab-content">
				<pre>
					<code class="js">
// Ubicación: organisms/blocks/03/js/count-up.js
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(require, exports, module);
  } else {
    root.CountUp = factory();
  }
}(this, function(require, exports, module) {

/*

  countUp.js
  by @inorganik

*/

// target = id of html element or var of previously selected html element where counting occurs
// startVal = the value you want to begin at
// endVal = the value you want to arrive at
// decimals = number of decimal places, default 0
// duration = duration of animation in seconds, default 2
// options = optional object of options (see below)

var CountUp = function(target, startVal, endVal, decimals, duration, options) {

  var self = this;
  self.version = function () { return '1.9.3'; };

  // default options
  self.options = {
    useEasing: true, // toggle easing
    useGrouping: true, // 1,000,000 vs 1000000
    separator: ',', // character to use as a separator
    decimal: '.', // character to use as a decimal
    easingFn: easeOutExpo, // optional custom easing function, default is Robert Penner's easeOutExpo
    formattingFn: formatNumber, // optional custom formatting function, default is formatNumber above
    prefix: '', // optional text before the result
    suffix: '', // optional text after the result
    numerals: [] // optionally pass an array of custom numerals for 0-9
  };

  // extend default options with passed options object
  if (options && typeof options === 'object') {
    for (var key in self.options) {
      if (options.hasOwnProperty(key) && options[key] !== null) {
        self.options[key] = options[key];
      }
    }
  }

  if (self.options.separator === '') {
    self.options.useGrouping = false;
  }
  else {
    // ensure the separator is a string (formatNumber assumes this)
    self.options.separator = '' + self.options.separator;
  }

  // make sure requestAnimationFrame and cancelAnimationFrame are defined
  // polyfill for browsers without native support
  // by Opera engineer Erik MÃ¶ller
  var lastTime = 0;
  var vendors = ['webkit', 'moz', 'ms', 'o'];
  for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
    window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
  }
  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = function(callback, element) {
      var currTime = new Date().getTime();
      var timeToCall = Math.max(0, 16 - (currTime - lastTime));
      var id = window.setTimeout(function() { callback(currTime + timeToCall); }, timeToCall);
      lastTime = currTime + timeToCall;
      return id;
    };
  }
  if (!window.cancelAnimationFrame) {
    window.cancelAnimationFrame = function(id) {
      clearTimeout(id);
    };
  }

  function formatNumber(num) {
    var neg = (num < 0),
          x, x1, x2, x3, i, len;
    num = Math.abs(num).toFixed(self.decimals);
    num += '';
    x = num.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? self.options.decimal + x[1] : '';
    if (self.options.useGrouping) {
      x3 = '';
      for (i = 0, len = x1.length; i < len; ++i) {
        if (i !== 0 && ((i % 3) === 0)) {
          x3 = self.options.separator + x3;
        }
        x3 = x1[len - i - 1] + x3;
      }
      x1 = x3;
    }
    // optional numeral substitution
    if (self.options.numerals.length) {
      x1 = x1.replace(/[0-9]/g, function(w) {
        return self.options.numerals[+w];
      })
      x2 = x2.replace(/[0-9]/g, function(w) {
        return self.options.numerals[+w];
      })
    }
    return (neg ? '-' : '') + self.options.prefix + x1 + x2 + self.options.suffix;
  }
  // Robert Penner's easeOutExpo
  function easeOutExpo(t, b, c, d) {
    return c * (-Math.pow(2, -10 * t / d) + 1) * 1024 / 1023 + b;
  }
  function ensureNumber(n) {
    return (typeof n === 'number' && !isNaN(n));
  }

  self.initialize = function() {
    if (self.initialized) return true;

    self.error = '';
    self.d = (typeof target === 'string') ? document.getElementById(target) : target;
    if (!self.d) {
      self.error = '[CountUp] target is null or undefined'
      return false;
    }
    self.startVal = Number(startVal);
    self.endVal = Number(endVal);
    // error checks
    if (ensureNumber(self.startVal) && ensureNumber(self.endVal)) {
      self.decimals = Math.max(0, decimals || 0);
      self.dec = Math.pow(10, self.decimals);
      self.duration = Number(duration) * 1000 || 2000;
      self.countDown = (self.startVal > self.endVal);
      self.frameVal = self.startVal;
      self.initialized = true;
      return true;
    }
    else {
      self.error = '[CountUp] startVal ('+startVal+') or endVal ('+endVal+') is not a number';
      return false;
    }
  };

  // Print value to target
  self.printValue = function(value) {
    var result = self.options.formattingFn(value);

    if (self.d.tagName === 'INPUT') {
      this.d.value = result;
    }
    else if (self.d.tagName === 'text' || self.d.tagName === 'tspan') {
      this.d.textContent = result;
    }
    else {
      this.d.innerHTML = result;
    }
  };

  self.count = function(timestamp) {

    if (!self.startTime) { self.startTime = timestamp; }

    self.timestamp = timestamp;
    var progress = timestamp - self.startTime;
    self.remaining = self.duration - progress;

    // to ease or not to ease
    if (self.options.useEasing) {
      if (self.countDown) {
        self.frameVal = self.startVal - self.options.easingFn(progress, 0, self.startVal - self.endVal, self.duration);
      } else {
        self.frameVal = self.options.easingFn(progress, self.startVal, self.endVal - self.startVal, self.duration);
      }
    } else {
      if (self.countDown) {
        self.frameVal = self.startVal - ((self.startVal - self.endVal) * (progress / self.duration));
      } else {
        self.frameVal = self.startVal + (self.endVal - self.startVal) * (progress / self.duration);
      }
    }

    // don't go past endVal since progress can exceed duration in the last frame
    if (self.countDown) {
      self.frameVal = (self.frameVal < self.endVal) ? self.endVal : self.frameVal;
    } else {
      self.frameVal = (self.frameVal > self.endVal) ? self.endVal : self.frameVal;
    }

    // decimal
    self.frameVal = Math.round(self.frameVal*self.dec)/self.dec;

    // format and print value
    self.printValue(self.frameVal);

    // whether to continue
    if (progress < self.duration) {
      self.rAF = requestAnimationFrame(self.count);
    } else {
      if (self.callback) self.callback();
    }
  };
  // start your animation
  self.start = function(callback) {
    if (!self.initialize()) return;
    self.callback = callback;
    self.rAF = requestAnimationFrame(self.count);
  };
  // toggles pause/resume animation
  self.pauseResume = function() {
    if (!self.paused) {
      self.paused = true;
      cancelAnimationFrame(self.rAF);
    } else {
      self.paused = false;
      delete self.startTime;
      self.duration = self.remaining;
      self.startVal = self.frameVal;
      requestAnimationFrame(self.count);
    }
  };
  // reset to startVal so animation can be run again
  self.reset = function() {
    self.paused = false;
    delete self.startTime;
    self.initialized = false;
    if (self.initialize()) {
      cancelAnimationFrame(self.rAF);
      self.printValue(self.startVal);
    }
  };
  // pass a new endVal and start animation
  self.update = function (newEndVal) {
    if (!self.initialize()) return;
    newEndVal = Number(newEndVal);
    if (!ensureNumber(newEndVal)) {
      self.error = '[CountUp] update() - new endVal is not a number: '+newEndVal;
      return;
    }
    self.error = '';
    if (newEndVal === self.frameVal) return;
    cancelAnimationFrame(self.rAF);
    self.paused = false;
    delete self.startTime;
    self.startVal = self.frameVal;
    self.endVal = newEndVal;
    self.countDown = (self.startVal > self.endVal);
    self.rAF = requestAnimationFrame(self.count);
  };

  // format startVal on initialization
  if (self.initialize()) self.printValue(self.startVal);
};

return CountUp;

}));
					</code>
				</pre>
			</div><!-- tab-content -->
			
			<div id="inview" class="tab-content">
				<pre>
					<code class="js">
// Ubicación: organisms/blocks/03/js/in-view.js
$(document).ready(function() {
/**
 * author Christopher Blum
 *    - based on the idea of Remy Sharp, http://remysharp.com/2009/01/26/element-in-view-event-plugin/
 *    - forked from http://github.com/zuk/jquery.inview/
 */
(function (factory) {
  if (typeof define == 'function' && define.amd) {
    // AMD
    define(['jquery'], factory);
  } else if (typeof exports === 'object') {
    // Node, CommonJS
    module.exports = factory(require('jquery'));
  } else {
      // Browser globals
    factory(jQuery);
  }
}(function ($) {

  var inviewObjects = [], viewportSize, viewportOffset,
      d = document, w = window, documentElement = d.documentElement, timer;

  $.event.special.inview = {
    add: function(data) {
      inviewObjects.push({ data: data, $element: $(this), element: this });
      // Use setInterval in order to also make sure this captures elements within
      // "overflow:scroll" elements or elements that appeared in the dom tree due to
      // dom manipulation and reflow
      // old: $(window).scroll(checkInView);
      //
      // By the way, iOS (iPad, iPhone, ...) seems to not execute, or at least delays
      // intervals while the user scrolls. Therefore the inview event might fire a bit late there
      //
      // Don't waste cycles with an interval until we get at least one element that
      // has bound to the inview event.
      if (!timer && inviewObjects.length) {
         timer = setInterval(checkInView, 250);
      }
    },

    remove: function(data) {
      for (var i=0; i<inviewObjects.length; i++) {
        var inviewObject = inviewObjects[i];
        if (inviewObject.element === this && inviewObject.data.guid === data.guid) {
          inviewObjects.splice(i, 1);
          break;
        }
      }

      // Clear interval when we no longer have any elements listening
      if (!inviewObjects.length) {
         clearInterval(timer);
         timer = null;
      }
    }
  };

  function getViewportSize() {
    var mode, domObject, size = { height: w.innerHeight, width: w.innerWidth };

    // if this is correct then return it. iPad has compat Mode, so will
    // go into check clientHeight/clientWidth (which has the wrong value).
    if (!size.height) {
      mode = d.compatMode;
      if (mode || !$.support.boxModel) { // IE, Gecko
        domObject = mode === 'CSS1Compat' ?
          documentElement : // Standards
          d.body; // Quirks
        size = {
          height: domObject.clientHeight,
          width:  domObject.clientWidth
        };
      }
    }

    return size;
  }

  function getViewportOffset() {
    return {
      top:  w.pageYOffset || documentElement.scrollTop   || d.body.scrollTop,
      left: w.pageXOffset || documentElement.scrollLeft  || d.body.scrollLeft
    };
  }

  function checkInView() {
    if (!inviewObjects.length) {
      return;
    }

    var i = 0, $elements = $.map(inviewObjects, function(inviewObject) {
      var selector  = inviewObject.data.selector,
          $element  = inviewObject.$element;
      return selector ? $element.find(selector) : $element;
    });

    viewportSize   = viewportSize   || getViewportSize();
    viewportOffset = viewportOffset || getViewportOffset();

    for (; i<inviewObjects.length; i++) {
      // Ignore elements that are not in the DOM tree
      if (!$.contains(documentElement, $elements[i][0])) {
        continue;
      }

      var $element      = $($elements[i]),
          elementSize   = { height: $element[0].offsetHeight, width: $element[0].offsetWidth },
          elementOffset = $element.offset(),
          inView        = $element.data('inview');

      // Don't ask me why because I haven't figured out yet:
      // viewportOffset and viewportSize are sometimes suddenly null in Firefox 5.
      // Even though it sounds weird:
      // It seems that the execution of this function is interferred by the onresize/onscroll event
      // where viewportOffset and viewportSize are unset
      if (!viewportOffset || !viewportSize) {
        return;
      }

      if (elementOffset.top + elementSize.height > viewportOffset.top &&
          elementOffset.top < viewportOffset.top + viewportSize.height &&
          elementOffset.left + elementSize.width > viewportOffset.left &&
          elementOffset.left < viewportOffset.left + viewportSize.width) {
        if (!inView) {
          $element.data('inview', true).trigger('inview', [true]);
        }
      } else if (inView) {
        $element.data('inview', false).trigger('inview', [false]);
      }
    }
  }

  $(w).bind("scroll resize scrollstop", function() {
    viewportSize = viewportOffset = null;
  });

  // IE < 9 scrolls to focused elements without firing the "scroll" event
  if (!documentElement.addEventListener && documentElement.attachEvent) {
    documentElement.attachEvent("onfocusin", function() {
      viewportOffset = null;
    });
  }
}));
})
					</code>
				</pre>
			</div><!-- tab-content -->
							
							
			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: organisms/blocks/03/scss/_block-3.scss */
.block-3-container{
  padding: 100px 10%;
  color: $blanco;
  @extend .col-100;
  @extend .left;
  @extend .relative;
  @include clearfix();
  .block-3,
  .main-content{
    @extend .relative;
    @extend .z-index-3;
  }
  .block-3{
    @extend .text-center;
    .content{
      i,
      .title,
      .pre-title{
        @extend .col-100;
        @extend .left;
      }
      i,
      img{
        margin-bottom: 20px;
      }
      i{
        @extend .font-42;
      }
      img{
        max-width: 100px;
        max-height: 100px;
        object-fit: contain;
      }
      .count-container{
        @extend .font-32;
        @extend .bold;
      }
      .title{
        @extend .font-21;
      }
    }
  }
  .main-content{
    margin-bottom: 80px;
    @extend .wrap-s;
    @extend .text-center;
    .pre-title{
      @extend .font-16;
      @extend .bold;
    }
    .title{
      @extend .font-30;
      @extend .bold;
    }
    .description{
      p{
        margin-bottom: 15px;
        @extend .font-16;
        &:last-child{
          margin-bottom: 0;
        }
      }
    }
  }
  .veil{
    background-color: rgba($negro, 0.5);
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #block-3 -->


	<section id="block-4">
		<div class="tabs">
			<p class="secondary-title">Bloque #4</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#php">PHP</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<section class="block-4">
					<div class="content">
						<p>Ahora no sabría dibujar, ni siquiera hacer una línea con el lápiz; y, sin embargo, jamás he sido mejor pintor Cuando el valle se vela en torno mío con un encaje de vapores; cuando el sol de mediodía centellea sobre la impenetrable sombra de mi bosque sin conseguir otra cosa que filtrar entre las hojas algunos rayos que penetran hasta el fondo del santuario.</p>
						<p class="author">Merlín, el Mago</p>
					</div>
					<img class="parallax" src="https://source.unsplash.com/random">
					<div class="veil"></div>
				</section><!-- block-4 -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;section class="block-4"&gt;
  &lt;div class="content"&gt;
    &lt;p&gt;Ahora no sabría dibujar, ni siquiera hacer una línea con el lápiz; y, sin embargo, jamás he sido mejor pintor Cuando el valle se vela en torno mío con un encaje de vapores; cuando el sol de mediodía centellea sobre la impenetrable sombra de mi bosque sin conseguir otra cosa que filtrar entre las hojas algunos rayos que penetran hasta el fondo del santuario.&lt;/p&gt;
    &lt;p class="author"&gt;Merlín, el Mago&lt;/p&gt;
  &lt;/div&gt;
  &lt;img class="parallax" src="https://source.unsplash.com/random"&gt;
  &lt;div class="veil"&gt;&lt;/div&gt;
&lt;/section&gt;&lt;!-- block-4 --&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="php" class="tab-content">
				<div class="message-block text-center">
					<p>Este organismo corresponde al ACF <strong>Bloque 4</strong>.</p>
				</div>

				<pre>
					<code class="html">
&lt;?php elseif ( get_row_layout() == 'bloque_4' ) :
$bloque_4_imagen = get_sub_field( 'bloque_4_imagen' );
$bloque_4_autor = get_sub_field( 'bloque_4_autor' );
$bloque_4_cita = get_sub_field( 'bloque_4_cita' );
?&gt;
&lt;section class="block-4"&gt;
  &lt;div class="content"&gt;
    &lt;?php echo $bloque_4_cita; ?&gt;
    &lt;?php if($bloque_4_autor) { ?&gt;
      &lt;p class="author"&gt;&lt;?php echo $bloque_4_autor; ?&gt;&lt;/p&gt;
    &lt;?php } ?&gt;
  &lt;/div&gt;

  &lt;?php if($bloque_4_imagen) { ?&gt;
    &lt;img class="parallax" src="&lt;?php echo $bloque_4_imagen; ?&gt;"&gt;
  &lt;?php } ?&gt;

  &lt;div class="veil"&gt;&lt;/div&gt;
&lt;/section&gt;&lt;!-- block-4 --&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: organisms/blocks/04/scss/_block-4.scss */
.block-4{
  padding: 180px 10%;
  color: $blanco;
  @extend .col-100;
  @extend .left;
  @extend .relative;
  @include clearfix();
  .content{
    @extend .z-index-3;
    @extend .relative;
    @extend .wrap-m;
    p{
      margin-bottom: 20px;
      @extend .font-24;
      @extend .bold;
      @extend .italic;
      &:last-child{
        margin-bottom: 0;
      }
      &.author{
        font-style: inherit;
        margin: 45px 0 0;
        @extend .font-17;
        @extend .text-right;
      }
    }
  }
  .veil{
    background-color: rgba($negro, 0.5);
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #block-4 -->


	<section id="block-5">
		<div class="tabs">
			<p class="secondary-title">Bloque #5</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#php">PHP</a></li>
				<li><a href="#jquery">jQuery</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="block-5">
					<li>
						<div class="trigger">
							Pregunta
							<i class="icon-mas"></i>
						</div>

						<div class="wysiwyg">
							<p>Respuesta</p>
						</div>
					</li>

					<li>
						<div class="trigger">
							Pregunta 2
							<i class="icon-mas"></i>
						</div>

						<div class="wysiwyg">
							<p>Respuesta 2</p>
						</div>
					</li>

					<li>
						<div class="trigger">
							Pregunta 3
							<i class="icon-mas"></i>
						</div>

						<div class="wysiwyg">
							<p>Respuesta 3</p>
						</div>
					</li>
				</ul>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;ul class="block-5"&gt;
  &lt;li&gt;
    &lt;div class="trigger"&gt;
      Pregunta
      &lt;i class="icon-mas"&gt;&lt;/i&gt;
    &lt;/div&gt;

    &lt;div class="wysiwyg"&gt;
      &lt;p&gt;Respuesta&lt;/p&gt;
    &lt;/div&gt;
  &lt;/li&gt;

  &lt;li&gt;
    &lt;div class="trigger"&gt;
      Pregunta 2
      &lt;i class="icon-mas"&gt;&lt;/i&gt;
    &lt;/div&gt;

    &lt;div class="wysiwyg"&gt;
      &lt;p&gt;Respuesta 2&lt;/p&gt;
    &lt;/div&gt;
  &lt;/li&gt;

  &lt;li&gt;
    &lt;div class="trigger"&gt;
      Pregunta 3
      &lt;i class="icon-mas"&gt;&lt;/i&gt;
    &lt;/div&gt;

    &lt;div class="wysiwyg"&gt;
      &lt;p&gt;Respuesta 3&lt;/p&gt;
    &lt;/div&gt;
  &lt;/li&gt;
&lt;/ul&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="php" class="tab-content">
				<div class="message-block text-center">
					<p>Este organismo corresponde al ACF <strong>Bloque 5</strong>.</p>
				</div>

				<pre>
					<code class="html">
&lt;?php if ( have_rows( 'bloque_5' ) ) : ?&gt;
  &lt;ul class="block-5"&gt;
  &lt;?php
  while ( have_rows( 'bloque_5' ) ) : the_row();
  $bloque_5_pregunta = get_sub_field( 'bloque_5_pregunta' );
  $bloque_5_respuesta = get_sub_field( 'bloque_5_respuesta' );
  ?&gt;
    &lt;li&gt;
      &lt;?php if($bloque_5_pregunta){ ?&gt;
        &lt;div class="trigger"&gt;
          &lt;?php echo $bloque_5_pregunta; ?&gt;
          &lt;i class="icon-mas"&gt;&lt;/i&gt;
        &lt;/div&gt;
      &lt;?php } ?&gt;

      &lt;?php if($bloque_5_pregunta){ ?&gt;
        &lt;div class="wysiwyg"&gt;
          &lt;p&gt;&lt;?php echo $bloque_5_pregunta; ?&gt;&lt;/p&gt;
        &lt;/div&gt;
      &lt;?php } ?&gt;
    &lt;/li&gt;
  &lt;?php endwhile; ?&gt;
  &lt;/ul&gt;
&lt;?php endif; ?&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="jquery" class="tab-content">
				<pre>
					<code class="js">
// Ubicación: organisms/blocks/05/js/block-5.js
$(document).ready(function() {
  //Acordion (FAQ)
  $(function() {
    $( ".block-5" ).accordion({ active: false, collapsible: true, heightStyle: "content" });
  });
})
					</code>
				</pre>
			</div>
			
			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: organisms/blocks/05/scss/_block-5.scss */
.block-5{
  @extend .left;
  @extend .col-100;
  @include clearfix;
  li{
    margin-top: 20px;
    @extend .left;
    @extend .col-100;
    @include clearfix;
    .trigger{
      padding: 15px 50px 15px 15px;
      color: $negro;
      border: 1px solid $negro;
      cursor: pointer;
      @extend .transition;
      @extend .relative;
      @extend .font-18;
      @extend .left;
      @extend .col-100;
      @include clearfix;
      i{
        right: 15px;
        top: 13px;
        font-size: 25px;
        @extend .absolute;
        @extend .transition-slower;
      }
      &.ui-state-active{
        i{
          @include rotate(45deg);
        }
      }
    }
    &:first-child{
      margin-top: 0;
    }
    .wysiwyg{
      padding: 15px;
      @extend .left;
      @extend .col-100;
      @include clearfix;
      &.ui-accordion-content-active{
        border: 1px solid $negro;
        margin-top: -1px;
      }
    }
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #block-5 -->


	<section id="block-6">
		<div class="tabs">
			<p class="secondary-title">Bloque #6</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#php">PHP</a></li>
				<li><a href="#jquery">jQuery</a></li>
				<li><a href="#countup">CountUp.js</a></li>
				<li><a href="#inview">In-View.js</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block">
					<ul>
						<li>Este módulo hereda archivos javascript del <a href="#block-3" class="scroll-move">Bloque #3</a> para hacer el conteo de cifras en caso que se requiera. Éstos son: <a href="<?php echo get_stylesheet_directory_uri(); ?>/organisms/blocks/03/js/count-up.js" target="_blank">count-up.js</a>, <a href="<?php echo get_stylesheet_directory_uri(); ?>/organisms/blocks/03/js/in-view.js" target="_blank">in-view.js</a>, además del <a href="<?php echo get_stylesheet_directory_uri(); ?>/organisms/blocks/03/js/block-3.js" target="_blank">script propio</a> del módulo anteriormente mencionado.</li>
						<li>
							Cada bloque tiene 4 posibilidades de visualización:
							<ul>
								<li><strong>Numeración automática:</strong> Dispondrá el número a gran tamaño, correspondiente al elemento (primer bloque).</li>
								<li><strong>Ícono: Posibilidad de colocar una imagen (segundo bloque).</strong></li>
								<li><strong>Cifra:</strong> Posibilidad de añadir un valor manualmente (tercer bloque).</li>
								<li><strong>Sin imagen ni numeración:</strong> No aparecerá ninguna de las opciones anteriores (cuarto bloque).</li>
							</ul>
						</li>
					</ul>
				</div>

				<section class="block-6 col-100 left clearfix">
					<div class="grid-column-2 gap-l">
						<div class="block">
							<p class="number count">1</p>
							<p class="title">Título uno</p>
							<div class="description">
								<p>Y, viéndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: Sábete, Sancho, que no es un hombre más que otro si no hace más que otro.</p>
							</div>
						</div>
						
						<div class="block">
							<img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-4.jpg">
							<p class="title">Título 2</p>
							<div class="description">
								<p>Y, viéndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: Sábete, Sancho, que no es un hombre más que otro si no hace más que otro.</p>
							</div>
						</div>
						
						<div class="block">
							<p class="number">
								<span>MM</span>
								<span class="count">98</span>
								<span>USD</span>
							</p>
							<p class="title">Título tres</p>
							<div class="description">
								<p>Y, viéndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: Sábete, Sancho, que no es un hombre más que otro si no hace más que otro.</p>
							</div>
						</div>
						
						<div class="block">
							<p class="title">Título cuatro</p>
							<div class="description">
								<p>Y, viéndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: Sábete, Sancho, que no es un hombre más que otro si no hace más que otro.</p>
							</div>
						</div>
					</div>
				</section>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;section class="block-6 col-100 left clearfix"&gt
  &lt;div class="grid-column-2 gap-l"&gt
    &lt;div class="block"&gt
      &lt;p class="number count"&gt1&lt;/p&gt
      &lt;p class="title"&gtTítulo uno&lt;/p&gt
      &lt;div class="description"&gt
        &lt;p&gtY, viéndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: Sábete, Sancho, que no es un hombre más que otro si no hace más que otro.&lt;/p&gt
      &lt;/div&gt
    &lt;/div&gt
    
    &lt;div class="block"&gt
      &lt;img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-4.jpg"&gt
      &lt;p class="title"&gtTítulo 2&lt;/p&gt
      &lt;div class="description"&gt
        &lt;p&gtY, viéndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: Sábete, Sancho, que no es un hombre más que otro si no hace más que otro.&lt;/p&gt
      &lt;/div&gt
    &lt;/div&gt
    
    &lt;div class="block"&gt
      &lt;p class="number"&gt
        &lt;span&gtMM&lt;/span&gt
        &lt;span class="count"&gt98&lt;/span&gt
        &lt;span&gtUSD&lt;/span&gt
      &lt;/p&gt
      &lt;p class="title"&gtTítulo tres&lt;/p&gt
      &lt;div class="description"&gt
        &lt;p&gtY, viéndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: Sábete, Sancho, que no es un hombre más que otro si no hace más que otro.&lt;/p&gt
      &lt;/div&gt
    &lt;/div&gt
    
    &lt;div class="block"&gt
      &lt;p class="title"&gtTítulo cuatro&lt;/p&gt
      &lt;div class="description"&gt
        &lt;p&gtY, viéndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: Sábete, Sancho, que no es un hombre más que otro si no hace más que otro.&lt;/p&gt
      &lt;/div&gt
    &lt;/div&gt
  &lt;/div&gt
&lt;/section&gt
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="php" class="tab-content">
				<div class="message-block text-center">
					<p>Este organismo corresponde al ACF <strong>Bloque 6</strong>.</p>
				</div>

				<pre>
					<code class="html">
&lt;?php
if ( get_row_layout() == 'bloque_6' ) :
  $bloque_6_columnas = get_sub_field('bloque_6_columnas');
?&gt;
  &lt;section class="block-6 col-100 left clearfix"&gt;
    &lt;?php if ( have_rows( 'bloque_6' ) ) : ?&gt;
      &lt;div class="gap-l &lt;?php echo $bloque_6_columnas; ?&gt;"&gt;
        &lt;?php
        while (have_rows('bloque_6')) : the_row();
        $bloque_6_estilo = get_sub_field( 'bloque_6_estilo' );
        $bloque_6_icono = get_sub_field( 'bloque_6_icono' );
        $bloque_6_precifra = get_sub_field( 'bloque_6_precifra' );
        $bloque_6_cifra = get_sub_field( 'bloque_6_cifra' );
        $bloque_6_postcifra = get_sub_field( 'bloque_6_postcifra' );
        $bloque_6_titulo = get_sub_field('bloque_6_titulo');
        $bloque_6_descripcion = get_sub_field('bloque_6_descripcion');
        ?&gt;
          &lt;div class="block"&gt;
            &lt;?php if($bloque_6_estilo == 'icono' && $bloque_6_icono){ ?&gt;
              &lt;img src="&lt;?php echo $bloque_6_icono; ?&gt;"&gt;
            &lt;?php }elseif($bloque_6_estilo == 'numeracion-automatica'){ ?&gt;
              &lt;p class="number count"&gt;&lt;?php echo $i; ?&gt;&lt;/p&gt;
            &lt;?php }elseif($bloque_6_estilo == 'cifra'){ ?&gt;
              &lt;p class="number"&gt;
                &lt;?php if($bloque_6_precifra){ ?&gt;
                  &lt;span&gt;&lt;?php echo $bloque_6_precifra; ?&gt;&lt;/span&gt;
                &lt;?php } ?&gt;
                
                &lt;span class="count"&gt;&lt;?php echo $bloque_6_cifra; ?&gt;&lt;/span&gt;
                
                &lt;?php if($bloque_6_postcifra){ ?&gt;
                  &lt;span&gt;&lt;?php echo $bloque_6_postcifra; ?&gt;&lt;/span&gt;
                &lt;?php } ?&gt;
              &lt;/p&gt;
            &lt;?php } ?&gt;

            &lt;?php if($bloque_6_titulo){ ?&gt;
              &lt;p class="title"&gt;&lt;?php echo $bloque_6_titulo; ?&gt;&lt;/p&gt;
            &lt;?php } ?&gt;

            &lt;?php if($bloque_6_descripcion){ ?&gt;
              &lt;div class="description"&gt;&lt;?php echo $bloque_6_descripcion; ?&gt;&lt;/div&gt;
            &lt;?php } ?&gt;
          &lt;/div&gt;
        &lt;?php
        endwhile;
        ?&gt;
      &lt;/div&gt;
    &lt;?php endif; ?&gt;
  &lt;/section&gt;
&lt;?php endif; ?&gt;
					</code>
				</pre>
			</div><!-- tab-content -->
			
			<div id="jquery" class="tab-content">
				<pre>
					<code class="js">
// Ubicación: organisms/blocks/03/js/block-03.js
$(document).ready(function() {
  var i = 0;
  $( ".count" ).each(function() {
    var idCounter = "counter"+ ++i;
    var cifra1 = $(this).text(); // Cifra ingresada back
    var primerCaracter = cifra1.charAt(0); // Primer carácter de la cifra
    var ultimoCaracter = cifra1.charAt(cifra1.length-1);  // Úlitmo carácter de la cifra
    var cifraClean = cifra1.replace(/[^0-9]/g,''); // Cifra sanitizada (solo números)
    var CifraConPunto = cifra1.replace(/,/g, '.'); // Cifra con coma pasada a puntos
    var StrAfterComa = cifra1.substr(cifra1.indexOf(",") + 1); // string despues de la coma
    var StrAfterComaClean = StrAfterComa.replace(/[^0-9]/g,''); // string sanitizado despues de la coma
    var decimalPlaces = StrAfterComaClean.replace(/ /g,'').length // número de digitos después de la coma

    //Si el primer caracter es numérico pasa a ser prefijo :
    if($.isNumeric(primerCaracter)){
        var prefix = "";
    }else {
        var prefix = primerCaracter;
    }

    //Si el último caracter es numérico pasa a ser sufijo :
    if($.isNumeric(ultimoCaracter)){
        var sufix = "";
    }else {
        var sufix = ultimoCaracter;
    }

    //Si la cifra contiene una coma :
    if (cifra1.indexOf(',') !== -1){
        var separator = "";
        var numeroDecimales = decimalPlaces;
        var decimal = ","
        var CifraFinal = CifraConPunto
        // tiene ,
    }else {
        var separator = ".";
        var numeroDecimales = "";
        var decimal = "";
        var CifraFinal = cifraClean
    }

    // counter, cifrainicial, cifrafinal, numerodecimales, duración
    var c = new CountUp(this,0,CifraFinal,numeroDecimales,0,{
        separator: separator,
        decimal: decimal,
        prefix: prefix,
        suffix: sufix

    });

    $(this).one('inview', function(event, isInView) {
      if (isInView) {
        c.start();
      }//endif
    });

  });
})
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="countup" class="tab-content">
				<pre>
					<code class="js">
// Ubicación: organisms/blocks/03/js/count-up.js
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(require, exports, module);
  } else {
    root.CountUp = factory();
  }
}(this, function(require, exports, module) {

/*

  countUp.js
  by @inorganik

*/

// target = id of html element or var of previously selected html element where counting occurs
// startVal = the value you want to begin at
// endVal = the value you want to arrive at
// decimals = number of decimal places, default 0
// duration = duration of animation in seconds, default 2
// options = optional object of options (see below)

var CountUp = function(target, startVal, endVal, decimals, duration, options) {

  var self = this;
  self.version = function () { return '1.9.3'; };

  // default options
  self.options = {
    useEasing: true, // toggle easing
    useGrouping: true, // 1,000,000 vs 1000000
    separator: ',', // character to use as a separator
    decimal: '.', // character to use as a decimal
    easingFn: easeOutExpo, // optional custom easing function, default is Robert Penner's easeOutExpo
    formattingFn: formatNumber, // optional custom formatting function, default is formatNumber above
    prefix: '', // optional text before the result
    suffix: '', // optional text after the result
    numerals: [] // optionally pass an array of custom numerals for 0-9
  };

  // extend default options with passed options object
  if (options && typeof options === 'object') {
    for (var key in self.options) {
      if (options.hasOwnProperty(key) && options[key] !== null) {
        self.options[key] = options[key];
      }
    }
  }

  if (self.options.separator === '') {
    self.options.useGrouping = false;
  }
  else {
    // ensure the separator is a string (formatNumber assumes this)
    self.options.separator = '' + self.options.separator;
  }

  // make sure requestAnimationFrame and cancelAnimationFrame are defined
  // polyfill for browsers without native support
  // by Opera engineer Erik MÃ¶ller
  var lastTime = 0;
  var vendors = ['webkit', 'moz', 'ms', 'o'];
  for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
    window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
  }
  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = function(callback, element) {
      var currTime = new Date().getTime();
      var timeToCall = Math.max(0, 16 - (currTime - lastTime));
      var id = window.setTimeout(function() { callback(currTime + timeToCall); }, timeToCall);
      lastTime = currTime + timeToCall;
      return id;
    };
  }
  if (!window.cancelAnimationFrame) {
    window.cancelAnimationFrame = function(id) {
      clearTimeout(id);
    };
  }

  function formatNumber(num) {
    var neg = (num < 0),
          x, x1, x2, x3, i, len;
    num = Math.abs(num).toFixed(self.decimals);
    num += '';
    x = num.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? self.options.decimal + x[1] : '';
    if (self.options.useGrouping) {
      x3 = '';
      for (i = 0, len = x1.length; i < len; ++i) {
        if (i !== 0 && ((i % 3) === 0)) {
          x3 = self.options.separator + x3;
        }
        x3 = x1[len - i - 1] + x3;
      }
      x1 = x3;
    }
    // optional numeral substitution
    if (self.options.numerals.length) {
      x1 = x1.replace(/[0-9]/g, function(w) {
        return self.options.numerals[+w];
      })
      x2 = x2.replace(/[0-9]/g, function(w) {
        return self.options.numerals[+w];
      })
    }
    return (neg ? '-' : '') + self.options.prefix + x1 + x2 + self.options.suffix;
  }
  // Robert Penner's easeOutExpo
  function easeOutExpo(t, b, c, d) {
    return c * (-Math.pow(2, -10 * t / d) + 1) * 1024 / 1023 + b;
  }
  function ensureNumber(n) {
    return (typeof n === 'number' && !isNaN(n));
  }

  self.initialize = function() {
    if (self.initialized) return true;

    self.error = '';
    self.d = (typeof target === 'string') ? document.getElementById(target) : target;
    if (!self.d) {
      self.error = '[CountUp] target is null or undefined'
      return false;
    }
    self.startVal = Number(startVal);
    self.endVal = Number(endVal);
    // error checks
    if (ensureNumber(self.startVal) && ensureNumber(self.endVal)) {
      self.decimals = Math.max(0, decimals || 0);
      self.dec = Math.pow(10, self.decimals);
      self.duration = Number(duration) * 1000 || 2000;
      self.countDown = (self.startVal > self.endVal);
      self.frameVal = self.startVal;
      self.initialized = true;
      return true;
    }
    else {
      self.error = '[CountUp] startVal ('+startVal+') or endVal ('+endVal+') is not a number';
      return false;
    }
  };

  // Print value to target
  self.printValue = function(value) {
    var result = self.options.formattingFn(value);

    if (self.d.tagName === 'INPUT') {
      this.d.value = result;
    }
    else if (self.d.tagName === 'text' || self.d.tagName === 'tspan') {
      this.d.textContent = result;
    }
    else {
      this.d.innerHTML = result;
    }
  };

  self.count = function(timestamp) {

    if (!self.startTime) { self.startTime = timestamp; }

    self.timestamp = timestamp;
    var progress = timestamp - self.startTime;
    self.remaining = self.duration - progress;

    // to ease or not to ease
    if (self.options.useEasing) {
      if (self.countDown) {
        self.frameVal = self.startVal - self.options.easingFn(progress, 0, self.startVal - self.endVal, self.duration);
      } else {
        self.frameVal = self.options.easingFn(progress, self.startVal, self.endVal - self.startVal, self.duration);
      }
    } else {
      if (self.countDown) {
        self.frameVal = self.startVal - ((self.startVal - self.endVal) * (progress / self.duration));
      } else {
        self.frameVal = self.startVal + (self.endVal - self.startVal) * (progress / self.duration);
      }
    }

    // don't go past endVal since progress can exceed duration in the last frame
    if (self.countDown) {
      self.frameVal = (self.frameVal < self.endVal) ? self.endVal : self.frameVal;
    } else {
      self.frameVal = (self.frameVal > self.endVal) ? self.endVal : self.frameVal;
    }

    // decimal
    self.frameVal = Math.round(self.frameVal*self.dec)/self.dec;

    // format and print value
    self.printValue(self.frameVal);

    // whether to continue
    if (progress < self.duration) {
      self.rAF = requestAnimationFrame(self.count);
    } else {
      if (self.callback) self.callback();
    }
  };
  // start your animation
  self.start = function(callback) {
    if (!self.initialize()) return;
    self.callback = callback;
    self.rAF = requestAnimationFrame(self.count);
  };
  // toggles pause/resume animation
  self.pauseResume = function() {
    if (!self.paused) {
      self.paused = true;
      cancelAnimationFrame(self.rAF);
    } else {
      self.paused = false;
      delete self.startTime;
      self.duration = self.remaining;
      self.startVal = self.frameVal;
      requestAnimationFrame(self.count);
    }
  };
  // reset to startVal so animation can be run again
  self.reset = function() {
    self.paused = false;
    delete self.startTime;
    self.initialized = false;
    if (self.initialize()) {
      cancelAnimationFrame(self.rAF);
      self.printValue(self.startVal);
    }
  };
  // pass a new endVal and start animation
  self.update = function (newEndVal) {
    if (!self.initialize()) return;
    newEndVal = Number(newEndVal);
    if (!ensureNumber(newEndVal)) {
      self.error = '[CountUp] update() - new endVal is not a number: '+newEndVal;
      return;
    }
    self.error = '';
    if (newEndVal === self.frameVal) return;
    cancelAnimationFrame(self.rAF);
    self.paused = false;
    delete self.startTime;
    self.startVal = self.frameVal;
    self.endVal = newEndVal;
    self.countDown = (self.startVal > self.endVal);
    self.rAF = requestAnimationFrame(self.count);
  };

  // format startVal on initialization
  if (self.initialize()) self.printValue(self.startVal);
};

return CountUp;

}));
					</code>
				</pre>
			</div><!-- tab-content -->
			
			<div id="inview" class="tab-content">
				<pre>
					<code class="js">
// Ubicación: organisms/blocks/03/js/in-view.js
$(document).ready(function() {
/**
 * author Christopher Blum
 *    - based on the idea of Remy Sharp, http://remysharp.com/2009/01/26/element-in-view-event-plugin/
 *    - forked from http://github.com/zuk/jquery.inview/
 */
(function (factory) {
  if (typeof define == 'function' && define.amd) {
    // AMD
    define(['jquery'], factory);
  } else if (typeof exports === 'object') {
    // Node, CommonJS
    module.exports = factory(require('jquery'));
  } else {
      // Browser globals
    factory(jQuery);
  }
}(function ($) {

  var inviewObjects = [], viewportSize, viewportOffset,
      d = document, w = window, documentElement = d.documentElement, timer;

  $.event.special.inview = {
    add: function(data) {
      inviewObjects.push({ data: data, $element: $(this), element: this });
      // Use setInterval in order to also make sure this captures elements within
      // "overflow:scroll" elements or elements that appeared in the dom tree due to
      // dom manipulation and reflow
      // old: $(window).scroll(checkInView);
      //
      // By the way, iOS (iPad, iPhone, ...) seems to not execute, or at least delays
      // intervals while the user scrolls. Therefore the inview event might fire a bit late there
      //
      // Don't waste cycles with an interval until we get at least one element that
      // has bound to the inview event.
      if (!timer && inviewObjects.length) {
         timer = setInterval(checkInView, 250);
      }
    },

    remove: function(data) {
      for (var i=0; i<inviewObjects.length; i++) {
        var inviewObject = inviewObjects[i];
        if (inviewObject.element === this && inviewObject.data.guid === data.guid) {
          inviewObjects.splice(i, 1);
          break;
        }
      }

      // Clear interval when we no longer have any elements listening
      if (!inviewObjects.length) {
         clearInterval(timer);
         timer = null;
      }
    }
  };

  function getViewportSize() {
    var mode, domObject, size = { height: w.innerHeight, width: w.innerWidth };

    // if this is correct then return it. iPad has compat Mode, so will
    // go into check clientHeight/clientWidth (which has the wrong value).
    if (!size.height) {
      mode = d.compatMode;
      if (mode || !$.support.boxModel) { // IE, Gecko
        domObject = mode === 'CSS1Compat' ?
          documentElement : // Standards
          d.body; // Quirks
        size = {
          height: domObject.clientHeight,
          width:  domObject.clientWidth
        };
      }
    }

    return size;
  }

  function getViewportOffset() {
    return {
      top:  w.pageYOffset || documentElement.scrollTop   || d.body.scrollTop,
      left: w.pageXOffset || documentElement.scrollLeft  || d.body.scrollLeft
    };
  }

  function checkInView() {
    if (!inviewObjects.length) {
      return;
    }

    var i = 0, $elements = $.map(inviewObjects, function(inviewObject) {
      var selector  = inviewObject.data.selector,
          $element  = inviewObject.$element;
      return selector ? $element.find(selector) : $element;
    });

    viewportSize   = viewportSize   || getViewportSize();
    viewportOffset = viewportOffset || getViewportOffset();

    for (; i<inviewObjects.length; i++) {
      // Ignore elements that are not in the DOM tree
      if (!$.contains(documentElement, $elements[i][0])) {
        continue;
      }

      var $element      = $($elements[i]),
          elementSize   = { height: $element[0].offsetHeight, width: $element[0].offsetWidth },
          elementOffset = $element.offset(),
          inView        = $element.data('inview');

      // Don't ask me why because I haven't figured out yet:
      // viewportOffset and viewportSize are sometimes suddenly null in Firefox 5.
      // Even though it sounds weird:
      // It seems that the execution of this function is interferred by the onresize/onscroll event
      // where viewportOffset and viewportSize are unset
      if (!viewportOffset || !viewportSize) {
        return;
      }

      if (elementOffset.top + elementSize.height > viewportOffset.top &&
          elementOffset.top < viewportOffset.top + viewportSize.height &&
          elementOffset.left + elementSize.width > viewportOffset.left &&
          elementOffset.left < viewportOffset.left + viewportSize.width) {
        if (!inView) {
          $element.data('inview', true).trigger('inview', [true]);
        }
      } else if (inView) {
        $element.data('inview', false).trigger('inview', [false]);
      }
    }
  }

  $(w).bind("scroll resize scrollstop", function() {
    viewportSize = viewportOffset = null;
  });

  // IE < 9 scrolls to focused elements without firing the "scroll" event
  if (!documentElement.addEventListener && documentElement.attachEvent) {
    documentElement.attachEvent("onfocusin", function() {
      viewportOffset = null;
    });
  }
}));
})
					</code>
				</pre>
			</div><!-- tab-content -->
							
							
			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: organisms/blocks/06/scss/_block-6.scss */
.block-6{
  .block{
    padding: 20px;
    @extend .text-center;
    p,.description{
      @extend .col-100;
      @extend .left;
      @include clearfix;
    }
    img{
      max-width: 50%;
      object-fit: contain;
      display: block;
      margin: 0 auto 20px;
    }
    .number{
      @extend .bold;
      @extend .font-70;
    }
    .title{
      @extend .semibold;
      @extend .font-28;
    }
    .description{
      margin-top: 20px;
      p{
        @extend .font-15;
        @extend .regular;
        &:not(:last-child){
          margin-bottom: 20px;
        }
      }
    }
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #block-6 -->
<?php get_footer(); ?>
