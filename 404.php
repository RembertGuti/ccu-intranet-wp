<?php
/*
Template Name: 404
*/
?>


<?php get_header('login'); ?>

<section class="error-screen">
    <?php
        $bg_404 = get_field( 'bg_404', 'option' );
        ?>
    <div class="bg-image cover" style="background-image: url(<?php echo $bg_404['url']; ?>)"
        title="<?php echo $bg_404['alt']; ?>">
        <div class="veil"></div>
    </div>
    <div class="wrap-xl">
        <div class="content-area">
            <div class="content">
                <h1>Error 404</h1>
                <p class="intro">Lo sentimos, pero la página que buscas no existe :-(</p>
                <p class="bajada">Te invitamos a seguir navegando por nuestro sitio web y refrescándote con todos sus
                    contenidos:</p>
                <div class="exits-area">
                    <div class="home-link">
                        <a href="<?php echo site_url('/'); ?>" class="btn size-xl is-verde is-rounded">Volver al
                            inicio</a>
                    </div>
                    <div class="search-form">
                        <form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <div class="search-input-area">
                                <input type="search"
                                    placeholder="<?php echo esc_attr_x( 'Realiza una búsqueda', 'placeholder', 'base' ); ?>"
                                    value="<?php echo get_search_query(); ?>" name="s" id="search-box">
                                <button type="submit" class="btn-search"><i class="icon-buscar"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php get_footer('login') ?>