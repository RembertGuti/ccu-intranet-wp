<?php
/*
* Template Name: Página Tipo
*/
get_header();
?>
<?php $tipoHeading = get_field( 'tipo_heading' ); ?>
<?php if($tipoHeading == 'simple') { ?>
<section class="section gob-content-area">
    <div class="wrap-xl">
        <div class="head-page">
            <h1><?php the_field( 'titulo_simple' ); ?></h1>
        </div>
    </div>
</section>
<?php } elseif ($tipoHeading == 'fondo') { ?>
<section class="section">
    <div class="wrap-xl">
        <div class="page-heading consumo-heading">
            <?php $fondo_full = get_field( 'fondo_full' ); ?>
            <div class="bg-image cover" style="background-image: url(<?php echo $fondo_full['url']; ?>)"
                title="<?php echo $fondo_full['alt']; ?>">
                <div class="veil"></div>
            </div>
            <div class="content">
                <h1><?php the_field( 'titulo_full' ); ?></h1>
            </div>
        </div>
    </div>
</section>
<?php } elseif ($tipoHeading == 'full') { ?>
<section class="section">
    <div class="wrap-xl">
        <div class="page-heading consumo-heading">
            <?php $fondo_full = get_field( 'fondo_full' ); ?>
            <div class="bg-image cover" style="background-image: url(<?php echo $fondo_full['url']; ?>)"
                title="<?php echo $fondo_full['alt']; ?>">
                <div class="veil"></div>
            </div>
            <div class="content">
                <h1><?php the_field( 'titulo_full' ); ?></h1>
                <div class="intro-page">
                    <p><?php the_field( 'bajada_full' ); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } else { ?>
<section class="section gob-content-area">
    <div class="wrap-xl">
        <div class="head-page">
            <h1><?php the_title(); ?></h1>
        </div>
    </div>
</section>
<?php } ?>
<?php if ( have_rows( 'documentos_descargables' ) ) : ?>
<section class="section">
    <div class="wrap-xl">
        <div class="content">
            <?php while ( have_rows( 'documentos_descargables' ) ) : the_row(); ?>
            <div class="heading-box-area">
                <h3 class="head-title"><?php the_sub_field( 'titulo_caja_doc' ); ?></h3>
            </div>
            <?php if ( have_rows( 'documentos' ) ) : ?>
            <div class="files-area">
                <?php while ( have_rows( 'documentos' ) ) : the_row(); ?>
                <div class="file-box small">
                    <div class="file-size">
                        <div class="icono">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/file-icon.svg" alt="">
                        </div>
                        <div class="data">
                            <?php
                            $archivo_doc = get_sub_field( 'archivo_doc' );
                            $urlDoc = wp_get_attachment_url( $archivo_doc );
                            $titleDoc = get_the_title( $archivo_doc );
                            $filesizeDoc = filesize( get_attached_file( $archivo_doc ) );
                            $filesizeDoc = size_format($filesizeDoc, 2);
                            $path_infoDoc = pathinfo( get_attached_file( $archivo_doc ) );
                            ?>
                            <span class="size"><?php echo $path_infoDoc['extension']; ?>
                                <?php echo $filesizeDoc; ?></span>
                        </div>
                    </div>
                    <div class="file-info">
                        <div class="file-main-data">
                            <h4 class="file-name"><?php the_sub_field( 'titulo_doc' ); ?></h4>
                        </div>
                        <div class="file-link">
                            <a href="<?php echo $urlDoc; ?>"
                                class="btn is-verde size-xs is-rounded is-bordered has-icon"><i
                                    class="icon-download"></i><span>Descargar</span></a>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
            <?php endif; ?>
            <?php endwhile; ?>
        </div>
    </div>
</section>
<?php endif; ?>
<?php if ( have_rows( 'contenido' ) ): ?>
<?php while ( have_rows( 'contenido' ) ) : the_row(); ?>
<?php if ( get_row_layout() == 'texto_destacado' ) : ?>
<section class="section">
    <div class="wrap-xl">
        <div class="block-content feat-block">
            <?php the_sub_field( 'parrafo' ); ?>
        </div>
    </div>
</section>
<?php elseif ( get_row_layout() == 'contenido_wysiwyg' ) : ?>
<section class="section">
    <div class="wrap-xl">
        <div class="block-content wysiwyg">
            <?php the_sub_field( 'contenido' ); ?>
        </div>
    </div>
</section>
<?php elseif ( get_row_layout() == 'cajas_popup' ) : ?>
<section class="section manuales-section">
    <div class="wrap-xl">
        <div class="content">
            <?php $tituloCaja = get_sub_field( 'titulo_caja' ); ?>
            <?php if($tituloCaja) { ?>
            <div class="heading-box-area">
                <h3 class="head-title"><?php echo $tituloCaja; ?></h3>
            </div>
            <?php } ?>
            <?php if ( have_rows( 'cajas_archivos_externos_bi' ) ) : $m = 1;?>
            <div class="manuales-area">
                <?php while ( have_rows( 'cajas_archivos_externos_bi' ) ) : the_row(); ?>
                <?php if ( get_sub_field( 'archivo_ae_bi' ) ) { ?>
                <a href="<?php the_sub_field( 'archivo_ae_bi' ); ?>" target="_blank" class="manual-box">
                    <?php }else{  ?>
                    <a href="#" class="manual-box modal-trigger" data-id="inciativa-modal-<?php echo $m; ?>">
                        <?php }?>
                        <?php $imagen_rel_ae_bi = get_sub_field( 'imagen_rel_ae_bi' ); ?>
                        <div class="rel-image cover"
                            style="background-image: url(<?php echo $imagen_rel_ae_bi['url']; ?>);"
                            title="<?php echo $imagen_rel_ae_bi['alt']; ?>">
                            <?php $icono_rel_ae_bi = get_sub_field( 'icono_rel_ae_bi' ); ?>
                            <?php if ( $icono_rel_ae_bi ) { ?>
                            <div class="icono-box">
                                <img src="<?php echo $icono_rel_ae_bi['url']; ?>"
                                    alt="<?php echo $icono_rel_ae_bi['alt']; ?>" />
                            </div>
                            <?php } ?>
                        </div>
                        <div class="title-box">
                            <h2><?php the_sub_field( 'titulo_ae_bi' ); ?></h2>
                        </div>
                    </a>
                    <?php $m++; endwhile; ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php if ( have_rows( 'cajas_archivos_externos_bi' ) ) : $i = 1;?>
<?php while ( have_rows( 'cajas_archivos_externos_bi' ) ) : the_row(); ?>
<div data-id="inciativa-modal-<?php echo $i; ?>" class="modal modal-iniciativa">
    <i class="close icon-equis"></i>
    <div class="content-modal">
        <div class="modal-heading" style="background-color: <?php the_sub_field( 'color_rel_ae_bi' ); ?>;">
            <div class="title-box">
                <h4><?php the_sub_field( 'titulo_ae_bi' ); ?></h4>
            </div>
            <?php $icono_rel_ae_bi = get_sub_field( 'icono_rel_ae_bi' ); ?>
            <?php if ( $icono_rel_ae_bi ) { ?>
            <div class="icono-box">
                <img src="<?php echo $icono_rel_ae_bi['url']; ?>" alt="<?php echo $icono_rel_ae_bi['alt']; ?>" />
            </div>
            <?php } ?>
        </div>
        <div class="modal-contenido wysiwyg">
            <?php the_sub_field( 'intro_ae_bi' ); ?>
            <?php the_sub_field( 'descripcion_ae_bi' ); ?>
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<?php $i++; endwhile; ?>
<?php endif; ?>
<?php elseif ( get_row_layout() == 'docs_descargables_box' ) : ?>
<section class="section">
    <div class="wrap-xl">
        <div class="content">
            <?php $tituloCaja = get_sub_field( 'titulo_caja' ); ?>
            <?php if($tituloCaja) { ?>
            <div class="heading-box-area">
                <h3 class="head-title"><?php echo $tituloCaja; ?></h3>
            </div>
            <?php } ?>
            <?php if ( have_rows( 'box_documentos' ) ) : ?>
            <div class="files-area">
                <?php while ( have_rows( 'box_documentos' ) ) : the_row(); ?>
                <div class="file-box small">
                    <div class="file-size">
                        <div class="icono">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/file-icon.svg" alt="">
                        </div>
                        <div class="data">
                            <?php
                                        $archivo_doc = get_sub_field( 'archivo_doc' );
                                        $urlDoc = wp_get_attachment_url( $archivo_doc );
                                        $titleDoc = get_the_title( $archivo_doc );
                                        $filesizeDoc = filesize( get_attached_file( $archivo_doc ) );
                                        $filesizeDoc = size_format($filesizeDoc, 2);
                                        $path_infoDoc = pathinfo( get_attached_file( $archivo_doc ) );
                                        ?>
                            <span class="size"><?php echo $path_infoDoc['extension']; ?>
                                <?php echo $filesizeDoc; ?></span>
                        </div>
                    </div>
                    <div class="file-info">
                        <div class="file-main-data">
                            <h4 class="file-name"><?php the_sub_field( 'titulo_doc' ); ?></h4>
                        </div>
                        <div class="file-link">
                            <a href="<?php echo $urlDoc; ?>"
                                class="btn is-verde size-xs is-rounded is-bordered has-icon"><i
                                    class="icon-download"></i><span>Descargar</span></a>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php elseif ( get_row_layout() == 'docs_descargables_lista' ) : ?>
<section class="section">
    <div class="wrap-xl">
        <div class="content">
            <?php $tituloCaja = get_sub_field( 'titulo_caja' ); ?>
            <?php if($tituloCaja) { ?>
            <div class="heading-box-area">
                <h3 class="head-title"><?php echo $tituloCaja; ?></h3>
            </div>
            <?php } ?>
            <?php if ( have_rows( 'list_documentos' ) ) : ?>
            <div class="doc-list-area">
                <?php while ( have_rows( 'list_documentos' ) ) : the_row(); ?>
                <div class="doc-file-row">
                    <?php
                    $archivo_doc = get_sub_field( 'archivo_doc' );
                    $urlDoc = wp_get_attachment_url( $archivo_doc );
                    $titleDoc = get_the_title( $archivo_doc );
                    $filesizeDoc = filesize( get_attached_file( $archivo_doc ) );
                    $filesizeDoc = size_format($filesizeDoc, 2);
                    $path_infoDoc = pathinfo( get_attached_file( $archivo_doc ) );
                    ?>
                    <h4 class="file-name"><?php the_sub_field( 'titulo_doc' ); ?></h4>
                    <span class="size"><?php echo $path_infoDoc['extension']; ?>
                        <?php echo $filesizeDoc; ?></span>
                    <div class="file-link">
                        <a href="<?php echo $urlDoc; ?>" class="btn is-verde size-xs is-rounded has-icon"><i
                                class="icon-download"></i><span>Descargar</span></a>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php endif; ?>
<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>