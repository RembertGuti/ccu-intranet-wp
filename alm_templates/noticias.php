<?php
$newsThumbImg = get_the_post_thumbnail_url();
$newsThumbnailID = get_post_thumbnail_ID();
$alt = get_post_meta ( $newsThumbnailID, '_wp_attachment_image_alt', true );
$thumbPos = get_field( 'bg_posicion' );

if(empty($thumbPos)) {
    $bgPos = 'center';
} else {
    $bgPos = $thumbPos;
}
?>
<a href="<?php the_permalink(); ?>" class="small-news-area border-radius-m">
    <div class="photo"
        style="background-image: url(<?php echo $newsThumbImg; ?>); background-position: <?php echo $bgPos; ?>;"
        title="<?php echo $alt; ?>">
        <div class="veil"></div>
    </div>
    <div class="content">
        <div class="post-cat-area">
            <?php
            $categories = get_the_category();
            $comma      = ' ';
            $output     = '';
            
            if ( $categories ) {
                foreach ( $categories as $category ) {
                    $output .= '<span class="category">#' . $category->cat_name . '</span>' . $comma;
                }
                echo trim( $output, $comma );
            } ?>
        </div>
        <div class="content-area">
            <div class="post-info">
                <span class="fecha"><?php echo get_the_date('j \d\e F \d\e Y', $post->ID); ?></span>
                <h3 class="post-title">
                    <?php the_title(); ?>
                </h3>
            </div>
            <div class="button-area">
                <span class="btn is-verde is-rounded"><?php _e('Ver Más', 'ccu-intranet'); ?></span>
            </div>
        </div>
    </div>
</a>