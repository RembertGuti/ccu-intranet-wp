<?php
/*
* Template Name: Nuestras Marcas
*/
get_header();

?>
<section class="section">
    <div class="wrap-xl">
        <div class="heading-image bodegon">
            <?php
            $pageThumbImg = get_the_post_thumbnail_url();
            $pageThumbnailID = get_post_thumbnail_ID();
            $alt = get_post_meta ( $pageThumbnailID, '_wp_attachment_image_alt', true );
            ?>
            <div class="bg-image cover" style="background-image: url(<?php echo $pageThumbImg; ?>)"
                title="<?php echo $alt; ?>">
                <div class="veil"></div>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="wrap-xl">
        <div class="wysiwyg">
            <?php the_field( 'introduccion_nuestras_marcas_page_2' ); ?>
        </div>
    </div>
</section>

<section class="section">
    <div class="wrap-xl">
        <div class="filters-area">
            <h1 class="filter-title">Nuestro Portafolio de Marcas</h1>
            <div id="filters">
                <div class="filter-box">
                    <div class="select-box">
                        <?php
                        $filter_terms = get_terms( array(
                            'taxonomy'      => 'pais',
                            'parent'        => '0',
                            'hide_empty'    => false,
                        ) );
                        ?>
                        <?php if ($filter_terms) { ?>
                        <select name="pais" id="pais" class="select-input">
                            <?php foreach ($filter_terms as $filter_term) { 
                                $taxSlug = $filter_term->slug;
                                $taxName = $filter_term->name;
                            ?>
                            <option value="<?php echo $taxSlug; ?>"><?php echo $taxName; ?></option>
                            <?php } ?>
                        </select>
                        <span class="icon"><i class="icon-chevron-down"></i></span>
                        <?php } ?>
                    </div>
                </div>
                <div class="filter-box">
                    <div class="select-box">
                        <?php
                        $filter_terms = get_terms( array(
                            'taxonomy'      => 'tipo',
                            'parent'        => '0',
                            'hide_empty'    => false,
                        ) );
                        ?>
                        <?php if ($filter_terms) { ?>
                        <select name="tipo" id="tipo" class="select-input">
                            <option value="*">Tipo</option>
                            <?php foreach ($filter_terms as $filter_term) { 
                                $taxSlug = $filter_term->slug;
                                $taxName = $filter_term->name;
                            ?>
                            <option value="<?php echo $taxSlug; ?>"><?php echo $taxName; ?></option>
                            <?php } ?>
                        </select>
                        <span class="icon"><i class="icon-chevron-down"></i></span>
                        <?php } ?>
                    </div>
                </div>
                <div class="filter-box">
                    <a href="#" id="filter-btn" class="btn is-verde-oscuro is-rounded">Filtrar</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section">
    <div class="wrap-xl">
        <div id="all-marcas">
            <div class="marcas-container">
                <div class="marcas-area"></div>
                <div class="spinner">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/spinner.svg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<?php $experiencias_noticias = get_field( 'experiencias_noticias' ); ?>
<?php $concursos_noticias = get_field( 'concursos_noticias' ); ?>
<?php if($experiencias_noticias || $concursos_noticias) { ?>
<section class="section">
    <div class="wrap-xl">
        <div class="all-marcas-news">
            <?php if($experiencias_noticias) { ?>
            <div class="marcas-news-area">
                <div class="content">
                    <div class="heading-box-area">
                        <h3 class="head-title"><?php the_field( 'titulo_slide_left' ); ?></h3>
                        <?php $link_cat_exp_term = get_field( 'link_cat_exp' ); ?>
                        <?php if ( $link_cat_exp_term) { ?>
                        <a href="<?php echo site_url('/'); ?>repositorio/"
                            data-this-tax="<?php echo $link_cat_exp_term->slug; ?>"
                            class="btn-ver-todas tax-link"><span>Ver
                                Todas</span><i class="icon-chevron-right"></i></a>
                        <?php } ?>
                    </div>
                </div>

                <?php if ( $experiencias_noticias ): ?>
                <div class="mini-news-slide">
                    <?php foreach ( $experiencias_noticias as $post ):  ?>
                    <?php setup_postdata ( $post );
                    $newsThumbImg = get_the_post_thumbnail_url();
                    $newsThumbnailID = get_post_thumbnail_ID();
                    $alt = get_post_meta ( $newsThumbnailID, '_wp_attachment_image_alt', true );
                    $thumbPos = get_field( 'bg_posicion' );

                    if(empty($thumbPos)) {
                        $bgPos = 'cover';
                    } else {
                        $bgPos = $thumbPos;
                    }
                    ?>
                    <div class="slide">
                        <div class="news-box">
                            <div class="photo"
                                style="background-image: url(<?php echo $newsThumbImg; ?>); background-position: <?php echo $bgPos; ?>;"
                                title="<?php echo $alt; ?>">
                                <div class="veil"></div>
                            </div>
                            <div class="content">
                                <div class="post-cat-area">
                                    <?php
                                    $categories = get_the_category();
                                    $comma      = ' ';
                                    $output     = '';
                                    
                                    if ( $categories ) {
                                        foreach ( $categories as $category ) {
                                            $output .= '<span class="category">#' . $category->cat_name . '</span>' . $comma;
                                        }
                                        echo trim( $output, $comma );
                                    } ?>
                                </div>
                                <div class="content-area">
                                    <span class="fecha"><?php the_date(); ?></span>
                                    <h3 class="post-title">
                                        <?php the_title(); ?>
                                    </h3>
                                    <div class="button-area">
                                        <a href="<?php the_permalink(); ?>" class="btn is-verde is-rounded">Ver Más</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>
                </div>
                <?php endif; ?>
            </div>
            <?php } ?>
            <?php if($concursos_noticias) { ?>
            <div class="marcas-news-area">
                <div class="content">
                    <div class="heading-box-area">
                        <h3 class="head-title"><?php the_field( 'titulo_slide_right' ); ?></h3>
                        <?php $link_cat_con_term = get_field( 'link_cat_exp' ); ?>
                        <?php if ( $link_cat_con_term) { ?>
                        <a href="<?php echo site_url('/'); ?>repositorio/"
                            data-this-tax="<?php echo $link_cat_con_term->slug; ?>"
                            class="btn-ver-todas tax-link"><span>Ver
                                Todas</span><i class="icon-chevron-right"></i></a>
                        <?php } ?>
                    </div>
                </div>

                <?php if ( $concursos_noticias ): ?>
                <div class="mini-news-slide">
                    <?php foreach ( $concursos_noticias as $post ):  ?>
                    <?php setup_postdata ( $post );
                    $newsThumbImg = get_the_post_thumbnail_url();
                    $newsThumbnailID = get_post_thumbnail_ID();
                    $alt = get_post_meta ( $newsThumbnailID, '_wp_attachment_image_alt', true );
                    $thumbPos = get_field( 'bg_posicion' );

                    if(empty($thumbPos)) {
                        $bgPos = 'cover';
                    } else {
                        $bgPos = $thumbPos;
                    }
                    ?>
                    <div class="slide">
                        <div class="news-box">
                            <div class="photo"
                                style="background-image: url(<?php echo $newsThumbImg; ?>); background-position: <?php echo $bgPos; ?>;"
                                title="<?php echo $alt; ?>">
                                <div class="veil"></div>
                            </div>
                            <div class="content">
                                <div class="post-cat-area">
                                    <?php
                                    $categories = get_the_category();
                                    $comma      = ' ';
                                    $output     = '';
                                    
                                    if ( $categories ) {
                                        foreach ( $categories as $category ) {
                                            $output .= '<span class="category">#' . $category->cat_name . '</span>' . $comma;
                                        }
                                        echo trim( $output, $comma );
                                    } ?>
                                </div>
                                <div class="content-area">
                                    <span class="fecha"><?php the_date(); ?></span>
                                    <h3 class="post-title">
                                        <?php the_title(); ?>
                                    </h3>
                                    <div class="button-area">
                                        <a href="<?php the_permalink(); ?>" class="btn is-verde is-rounded">Ver Más</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>
                </div>
                <?php endif; ?>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
<?php } ?>
<?php get_footer(); ?>