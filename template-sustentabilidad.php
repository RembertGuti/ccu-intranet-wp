<?php
/*
* Template Name: Sustentabilidad
*/
get_header();
?>
<section class="section">
    <div class="wrap-xl">
        <div class="page-heading consumo-heading">
            <?php
            $pageThumbImg = get_the_post_thumbnail_url();
            $pageThumbnailID = get_post_thumbnail_ID();
            $alt = get_post_meta ( $pageThumbnailID, '_wp_attachment_image_alt', true );
            ?>
            <div class="bg-image cover" style="background-image: url(<?php echo $pageThumbImg; ?>)"
                title="<?php echo $alt; ?>">
                <div class="veil"></div>
            </div>
            <div class="content">
                <h1><?php the_title(); ?></h1>
                <div class="intro-page">
                    <h2><?php the_field( 'intro_titulo' ); ?></h2>
                    <p><?php the_field( 'intro_texto' ); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section page-content-area">
    <div class="wrap-xl">
        <div class="page-content">
            <div class="wysiwyg">
                <?php the_field( 'parrafo_intro' ); ?>
                <div id="modelo-gestion">
                    <?php $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
                if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false) || preg_match('/Edge/i', $ua) ) { ?>
                    <style type="text/css">
                    .pie {
                        clip: rect(0em, 30vh, 60vh, 0em);
                    }

                    .hold {
                        clip: rect(0em, 60vh, 60vh, 30vh);
                    }
                    </style>
                    <?php } ?>

                    <?php if( have_rows('item_grafico_sustentabilidad')): ?>
                    <div class="col-100 left clearfix relative text-center background-gray">
                        <div class="consumo-responsable relative text-center">
                            <?php
                            $items = get_field('item_grafico_sustentabilidad');
                            $size = 360/count($items);
                            $slice_rotate = 0;
                            $count = 0;
                            ?>

                            <div class="box-pie col-100 clearfix text-center transition-slower">
                                <div class="velo-container-characteristics"></div>

                                <?php if ( get_field('svg_modelo_sustentabilidad') ) { ?>
                                <div id="model" class="col-100 left transition-slower">
                                    <div class="wrap clearfix">
                                        <div class="img">
                                            <?php $svg_modelo = get_field('svg_modelo_sustentabilidad'); ?>
                                            <p><?php echo file_get_contents( $svg_modelo ); ?></p>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>

                            </div>

                            <div class="container-characteristics">
                                <?php
                                wp_reset_postdata();
                                $count = 1;
                                while ( have_rows('item_grafico_sustentabilidad') ) : the_row(); ?>

                                <div class="characteristics characteristics-sustentability-<?php echo $count; ?>">
                                    <span class="close transition"
                                        style="color: <?php echo get_sub_field('color_item'); ?>;">
                                        <i class="icon-equis"></i>
                                    </span>
                                    <div class="title">
                                        <div class="icono-area">
                                            <img src="<?php echo get_sub_field('icono_item'); ?>" alt="">
                                        </div>
                                        <div class="title-area">
                                            <h1 style="color: <?php echo get_sub_field('color_item'); ?>;">
                                                <?php echo get_sub_field('titulo_item'); ?>
                                            </h1>
                                        </div>

                                    </div>

                                    <?php
                                        if( have_rows('caracteristicas')): ?>
                                    <ul class="list roboto regular">
                                        <?php
                                            while ( have_rows('caracteristicas') ) : the_row(); ?>
                                        <li>
                                            <a
                                                href="<?php the_sub_field( 'enlace' ); ?>"><?php echo get_sub_field('caracteristica'); ?></a>
                                        </li>
                                        <?php endwhile; ?>
                                    </ul>
                                    <?php
                                        endif; ?>
                                    <style>
                                    .characteristics-sustentability-<?php echo $count.' ';

                                    ?>ul li:before {
                                        background-color: <?php echo get_sub_field('color_item');
                                        ?> !important;
                                    }
                                    </style>
                                </div>

                                <?php $count++; endwhile; ?>
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">
                    $(document).ready(function() {
                        $(".item-sustentabilidad").click(function(argument) {
                            dataItem = $(this).attr('data-item');
                            console.log(dataItem);
                            $('#model').addClass('activePie');
                            $(".characteristics").hide();
                            $('.characteristics-sustentability-' + dataItem)
                                .fadeIn();
                            $('.velo-container-characteristics').addClass('aparecer');
                        });

                        $('.container-characteristics .close').click(function() {
                            $('#model').removeClass('activePie');
                            $('.item-graphic').removeClass('active-item');
                            $('.characteristics').fadeOut();
                            $('.velo-container-characteristics').removeClass('aparecer');
                        });
                    });
                    </script>
                    <?php endif; ?>
                </div>
                <?php $cta_boton = get_field( 'cta_boton' ); ?>
                <?php $cta_logos = get_field( 'cta_logos' ); ?>
                <?php if ( $cta_logos ) { ?>
                <p class="text-center"><img class="logo-ftse4good" src="<?php echo $cta_logos['url']; ?>"
                        alt="<?php echo $cta_logos['alt']; ?>" /></p>
                <?php } ?>
                <?php if ( $cta_boton ) { ?>
                <p class="text-center">
                    <a href="#" id="activar-compromiso"
                        class="btn is-rounded is-verde size-s"><?php echo $cta_boton; ?></a>
                </p>
                <?php } ?>
                <div class="box-compromiso">
                    <div class="intro-compromiso">
                        <?php the_field( 'intro_compromiso' ); ?>
                    </div>
                    <?php if ( have_rows( 'cajas_ods' ) ) : ?>
                    <div class="ods-boxes">
                        <?php while ( have_rows( 'cajas_ods' ) ) : the_row(); ?>
                        <?php $imagen_ods = get_sub_field( 'imagen_ods' ); ?>
                        <?php if ( $imagen_ods ) { ?>
                        <img src="<?php echo $imagen_ods['url']; ?>" alt="<?php echo $imagen_ods['alt']; ?>"
                            class="ods-box" />
                        <?php } ?>
                        <?php endwhile; ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="shortcuts-area">
                <h4><?php the_field( 'titulo_indicadores' ); ?></h4>
                <div class="shortcuts-boxes">

                    <?php $tipoBtnPersonas = get_field( 'tipo_btn_personas' ); ?>
                    <?php if($tipoBtnPersonas == 'archivo') { ?>
                    <?php $boton_eje_personas = get_field( 'boton_eje_personas' ); ?>
                    <?php if ( $boton_eje_personas ) { ?>
                    <div class="shortcut-box">
                        <a href="<?php echo $boton_eje_personas['url']; ?>"
                            target="<?php echo $boton_eje_personas['target']; ?>"
                            class="btn size-s is-rounded is-burdeo"><?php echo $boton_eje_personas['title']; ?></a>
                    </div>
                    <?php } ?>
                    <?php } else if($tipoBtnPersonas == 'popup') { ?>
                    <?php $texto_boton_eje_personas = get_field( 'texto_boton_eje_personas' ); ?>
                    <?php if ( $texto_boton_eje_personas ) { ?>
                    <div class="shortcut-box">
                        <a href="#" class="btn size-s is-rounded is-burdeo modal-trigger"
                            data-id="modal-personas"><?php echo $texto_boton_eje_personas; ?></a>
                    </div>
                    <?php } ?>
                    <?php } ?>

                    <?php $tipoBtnMarcas = get_field( 'tipo_btn_marcas' ); ?>
                    <?php if($tipoBtnMarcas == 'archivo') { ?>
                    <?php $boton_eje_marcas = get_field( 'boton_eje_marcas' ); ?>
                    <?php if ( $boton_eje_marcas ) { ?>
                    <div class="shortcut-box">
                        <a href="<?php echo $boton_eje_marcas['url']; ?>"
                            target="<?php echo $boton_eje_marcas['target']; ?>"
                            class="btn size-s is-rounded is-celeste"><?php echo $boton_eje_marcas['title']; ?></a>
                    </div>
                    <?php } ?>
                    <?php } else if($tipoBtnMarcas == 'popup') { ?>
                    <?php $texto_boton_eje_marcas = get_field( 'texto_boton_eje_marcas' ); ?>
                    <?php if ( $texto_boton_eje_marcas ) { ?>
                    <div class="shortcut-box">
                        <a href="#" class="btn size-s is-rounded is-celeste modal-trigger"
                            data-id="modal-marcas"><?php echo $texto_boton_eje_marcas; ?></a>
                    </div>
                    <?php } ?>
                    <?php } ?>

                    <?php $tipoBtnPlaneta = get_field( 'tipo_btn_planeta' ); ?>
                    <?php if($tipoBtnPlaneta == 'archivo') { ?>
                    <?php $boton_eje_planeta = get_field( 'boton_eje_planeta' ); ?>
                    <?php if ( $boton_eje_planeta ) { ?>
                    <div class="shortcut-box">
                        <a href="<?php echo $boton_eje_planeta['url']; ?>"
                            target="<?php echo $boton_eje_planeta['target']; ?>"
                            class="btn size-s is-rounded is-amarillo"><?php echo $boton_eje_planeta['title']; ?></a>
                    </div>
                    <?php } ?>
                    <?php } else if($tipoBtnPlaneta == 'popup') { ?>
                    <?php $texto_boton_eje_planeta = get_field( 'texto_boton_eje_planeta' ); ?>
                    <?php if ( $texto_boton_eje_planeta ) { ?>
                    <div class="shortcut-box">
                        <a href="#" class="btn size-s is-rounded is-amarillo modal-trigger"
                            data-id="modal-planeta"><?php echo $texto_boton_eje_planeta; ?></a>
                    </div>
                    <?php } ?>
                    <?php } ?>

                </div>
            </div>
            <div class="content">
                <div class="heading-box-area">
                    <h3 class="head-title"><?php the_field( 'titulo_informes' ); ?></h3>
                    <?php $link_ver_todos_sus = get_field( 'link_ver_todos_sus' ); ?>
                    <?php if ( $link_ver_todos_sus ) { ?>
                    <a href="<?php echo $link_ver_todos_sus['url']; ?>"
                        target="<?php echo $link_ver_todos_sus['target']; ?>" class="btn-ver-todas">
                        <span><?php echo $link_ver_todos_sus['title']; ?></span><i class="icon-chevron-right"></i>
                    </a>
                    <?php } ?>
                </div>
                <?php if ( have_rows( 'archivo_informes' ) ) : ?>
                <div class="informes-area">
                    <?php while ( have_rows( 'archivo_informes' ) ) : the_row(); ?>
                    <?php if ( get_sub_field( 'archivo_pdf' ) ) { ?>
                    <a href="<?php the_sub_field( 'archivo_pdf' ); ?>" download="download" class="informe-box">
                        <?php $portada = get_sub_field( 'portada' ); ?>
                        <div class="portada">
                            <img src="<?php echo $portada['url']; ?>" alt="<?php echo $portada['alt']; ?>">
                        </div>
                        <div class="titulo">
                            <?php $nombreFile = get_sub_field( 'nombre_file' ); ?>
                            <?php if($nombreFile) { ?>
                            <h4><?php echo $nombreFile; ?></h4>
                            <?php } else { ?>
                            <h4>Informe de Sustentabilidad</h4>
                            <?php } ?>
                            <span class="btn is-verde size-s is-rounded">Descargar</span>
                        </div>
                    </a>
                    <?php } ?>
                    <?php endwhile; ?>
                </div>
                <?php endif; ?>
            </div>
            <div class="content videos-charlas-area">
                <div class="news-area layout-one-four-feat">
                    <div class="content">
                        <div class="heading-box-area">
                            <h3 class="head-title"><?php the_field( 'titulo_charlas' ); ?></h3>
                            <?php $link_todos_v_term = get_field( 'link_todos_v' ); ?>
                            <?php if ( $link_todos_v_term) { ?>
                            <a href="<?php echo site_url('/'); ?>repositorio-videos/"
                                data-this-tax="<?php echo $link_todos_v_term->slug; ?>"
                                class="btn-ver-todas tax-link"><span>Ver
                                    Todas</span><i class="icon-chevron-right"></i></a>
                            <?php } ?>
                        </div>

                        <div class="layout-news-area">
                            <?php $videos_charlas = get_field( 'videos_charlas' ); ?>
                            <?php if ( $videos_charlas ): ?>
                            <?php foreach ( $videos_charlas as $post ): $c = 1; ?>
                            <?php setup_postdata ( $post );
                            $newsThumbImg = get_the_post_thumbnail_url();
                            $newsThumbnailID = get_post_thumbnail_ID();
                            $alt = get_post_meta ( $newsThumbnailID, '_wp_attachment_image_alt', true );
                            ?>
                            <a href="<?php the_permalink($post); ?>" class="small-news-area border-radius-m">
                                <div class="photo cover" style="background-image: url(<?php echo $newsThumbImg; ?>);"
                                    title="<?php echo $alt; ?>">
                                    <div class="veil"></div>
                                </div>
                                <div class="content">
                                    <div class="post-cat-area">
                                        <?php
                                        $categories = get_the_category();
                                        $comma      = ' ';
                                        $output     = '';
                                        
                                        if ( $categories ) {
                                            foreach ( $categories as $category ) {
                                                $output .= '<span>#' . $category->cat_name . '</span>' . $comma;
                                            }
                                            echo trim( $output, $comma );
                                        } ?>
                                    </div>
                                    <div class="content-area">
                                        <div class="play-area modal-trigger" data-id="modal-video-<?php echo $c; ?>"
                                            data-video-url="<?php the_field('id_video_youtube'); ?>">
                                            <span class="tooltip-video">Ver video</span>
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/play.svg" alt="">
                                        </div>
                                        <div class="post-info">
                                            <h3 class="post-title">
                                                <?php the_title(); ?>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <?php $c++; endforeach; ?>
                            <?php wp_reset_postdata(); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content noticias-sustentabilidad-area">
                <div class="news-area layout-one-four-feat">
                    <div class="content">
                        <div class="heading-box-area">
                            <h3 class="head-title"><?php the_field( 'titulo_noticias_sus' ); ?></h3>
                            <?php $news_cat_ids = get_field( 'news_cat' ); ?>
                            <?php if ( $news_cat_ids) { ?>
                            <a href="<?php echo site_url('/'); ?>repositorio/"
                                data-this-tax="<?php echo $news_cat_ids->slug; ?>"
                                class="btn-ver-todas tax-link"><span>Ver
                                    Todas</span><i class="icon-chevron-right"></i></a>
                            <?php } ?>
                        </div>

                        <div class="layout-news-area">
                            <?php $noticias_sus = get_field( 'noticias_sus' ); ?>
                            <?php if ( $noticias_sus ): ?>
                            <?php foreach ( $noticias_sus as $post ):  ?>
                            <?php setup_postdata ( $post );
                            $newsThumbImg = get_the_post_thumbnail_url();
                            $newsThumbnailID = get_post_thumbnail_ID();
                            $alt = get_post_meta ( $newsThumbnailID, '_wp_attachment_image_alt', true );
                            ?>
                            <div class="small-news-area border-radius-m">
                                <div class="photo cover" style="background-image: url(<?php echo $newsThumbImg; ?>);"
                                    title="<?php echo $alt; ?>">
                                    <div class="veil"></div>
                                </div>
                                <div class="content">
                                    <div class="post-cat-area">
                                        <?php
                                        $categories = get_the_category();
                                        $comma      = ' ';
                                        $output     = '';
                                        
                                        if ( $categories ) {
                                            foreach ( $categories as $category ) {
                                                $output .= '<span>#' . $category->cat_name . '</span>' . $comma;
                                            }
                                            echo trim( $output, $comma );
                                        } ?>
                                    </div>
                                    <div class="content-area">
                                        <div class="post-info">
                                            <span class="fecha"><?php the_date(); ?></span>
                                            <h3 class="post-title">
                                                <?php the_title(); ?>
                                            </h3>
                                        </div>
                                        <div class="button-area">
                                            <a href="<?php the_permalink(); ?>"
                                                class="btn is-verde is-rounded"><?php _e('Ver Más', 'ccu-intranet'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                            <?php wp_reset_postdata(); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="comite-area">
                <h4 class="titulo"><?php the_field( 'titulo_comite' ); ?></h4>
                <div class="descripcion">
                    <?php the_field( 'parrafo_comite' ); ?>
                </div>
                <?php if ( have_rows( 'personas_comite' ) ) : ?>
                <div class="person-group-area">
                    <?php while ( have_rows( 'personas_comite' ) ) : the_row(); ?>
                    <div class="person-box">
                        <?php $foto_pc = get_sub_field( 'foto_pc' ); ?>
                        <div class="person-img cover" style="background-image: url(<?php echo $foto_pc['url']; ?>)"
                            title="<?php echo $foto_pc['alt']; ?>">
                        </div>
                        <div class="person-info">
                            <span class="cargo"><?php the_sub_field( 'cargo_pc' ); ?></span>
                            <h4 class="nombre"><?php the_sub_field( 'nombre_pc' ); ?></h4>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php $videos_gal_modal = get_field( 'videos_charlas' ); ?>
<?php if ( $videos_gal_modal ): $c = 1; ?>
<?php foreach ( $videos_gal_modal as $post ):  ?>
<?php setup_postdata ( $post ); ?>
<div data-id="modal-video-<?php echo $c; ?>" class="modal">
    <i class="close icon-equis"></i>
    <div class="content-modal contenido wp-content">
        <div class="iframeVideo relative">
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<?php $c++; endforeach; ?>
<?php wp_reset_postdata(); ?>
<?php endif; ?>
<?php $ejePersonas = get_field('popup_eje_personas'); ?>
<?php if($ejePersonas) { ?>
<div data-id="modal-personas" class="modal modal-eje">
    <i class="close icon-equis"></i>
    <div class="content-modal">
        <div class="modal-contenido wysiwyg">
            <?php echo $ejePersonas; ?>
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<?php } ?>
<?php $ejeMarcas = get_field('popup_eje_marcas'); ?>
<?php if($ejeMarcas) { ?>
<div data-id="modal-marcas" class="modal modal-eje">
    <i class="close icon-equis"></i>
    <div class="content-modal">
        <div class="modal-contenido wysiwyg">
            <?php echo $ejeMarcas; ?>
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<?php } ?>
<?php $ejePlaneta = get_field('popup_eje_planeta'); ?>
<?php if($ejePlaneta) { ?>
<div data-id="modal-planeta" class="modal modal-eje">
    <i class="close icon-equis"></i>
    <div class="content-modal">
        <div class="modal-contenido wysiwyg">
            <?php echo $ejePlaneta; ?>
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<?php } ?>
<script>
$(document).ready(function() {
    $('.btn-ver-todas.tax-link').each(function(index, element) {
        $(this).click(function(e) {
            e.preventDefault();
            let permalink = $(this).attr('href');
            let taxonomia = $(this).data('this-tax');
            $("<form method='POST' action='" + permalink +
                    "'><input type='hidden' name='thistax' value='" + taxonomia + "'/></form>")
                .appendTo("body").submit();
        });
    });
});
</script>
<?php get_footer(); ?>