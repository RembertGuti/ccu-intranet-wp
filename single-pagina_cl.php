<?php get_header() ?>

<?php 
	$current_id = get_the_ID();

	if($current_id == '111'){ //Template Name: Home (Inicio)
		include 'template-home.php';
	}
	if($current_id == '981'){ //Template Name: Ser CCU
		include 'template-serccu.php';
	}
	if($current_id == '982'){ //Template Name: Al Día CCU
		include 'template-aldiaccu.php';
	}
	if($current_id == '983'){ //Template Name: Consumo Responsable
		include 'template-consumo.php';
	}
	if($current_id == '984'){ //Template Name: Escríbenos
		include 'template-escribenos.php';
	}
	if($current_id == '985'){ //Template Name: Galeria de Videos
		include 'template-galeria-videos.php';
	}
	if($current_id == '986'){ //Template Name: Innovación
		include 'template-innovacion.php';
	}
	if($current_id == '987'){ //Template Name: Manual de Marcas
		include 'template-manual-marca.php';
	}
	if($current_id == '988' || $current_id == '999'){ //Template Name: Repositorio
		include 'template-repositorio.php';
	}
	if($current_id == '989'){ //Template Name: Nuestras Marcas
		include 'template-marcas.php';
	}
	if($current_id == '990'){ //Template Name: Organigrama
		include 'template-organigrama.php';
	}
	if($current_id == '991'){ //Template Name: Recursos Humanos
		include 'template-rrhh.php';
	}
	if($current_id == '992' || $current_id == '993' || $current_id == '994' || $current_id == '132'){ //Template Name: Recursos Humanos - Area
		include 'template-rrhh-areas.php';
	}
	if($current_id == '995' || $current_id == '996'){ //Template Name: Recursos Humanos - Contacto
		include 'template-rrhh-contacto.php';
	}
	if($current_id == '1000'){ //Template Name: Repositorio de Videos
		include 'template-repositorio-videos.php';
	}
	if($current_id == '1001'){ //Template Name: Sustentabilidad
		include 'template-sustentabilidad.php';
	}
	if($current_id == '1002'){ //Template Name: Sustentabilidad - Subpágina
		include 'template-sustentabilidad-sub.php';
	}
?>

<?php get_footer() ?>