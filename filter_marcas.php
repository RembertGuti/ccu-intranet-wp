<?php
    require_once("../../../wp-load.php");

    $pais = $_POST["pais"];
    $tipo = $_POST['tipo'];
    $subtipo = $_POST['subtipo'];

    $paises = get_terms('pais');
    $tipos = get_terms('tipo');

    $paises_ids = wp_list_pluck( $paises, 'slug' );
    $tipos_ids = wp_list_pluck($tipos, 'slug');

    if($pais != '*') {
        $pais = $pais;
    } else {
        $pais = $paises_ids;
    }

    if($tipo != '*') {
        $tipo = $tipo;
    } else {
        $tipo = $tipos_ids;
    }

    $posts_marcas = array();

    $args = array(
                'post_type' => array('marcas_cl', 'marcas_ar', 'marcas_bo', 'marcas_co', 'marcas_py', 'marcas_uy'),
                'posts_per_page' => -1,
                'tax_query' => array(
                    'relation' => 'AND',
                    array(
                        'taxonomy' => 'pais',
                        'field'    => 'slug',
                        'terms'    => $pais,
                    ),
                    array(
                        'taxonomy' => 'tipo',
                        'field'    => 'slug',
                        'terms'    => $tipo,
                    )
                ),
            );
    $the_query = new WP_Query($args);

    if ( $the_query->have_posts() ) :
        $text = "";

        while ( $the_query->have_posts() ) : $the_query->the_post();
            $nameMarca = get_the_title();
            $introMarca = get_field( 'intro_marca' );
            $logo = get_field( 'logo' );
            $text.= '<a href="'.get_the_permalink().'" class="marca-box">
                        <div class="marca-info">
                            <h5 class="name">'.$nameMarca.'</h5>
                            <p class="description">'.$introMarca.'</p>
                            <span class="btn size-s is-verde is-rounded is-bordered is-transparent">Ver
                                Producto</span>
                        </div>
                        <div class="marca-data">
                            <div class="img">
                                <img src="'.$logo['url'].'" alt="'.$logo['alt'].'"
                                    class="logo">
                            </div>
                            <span class="name">'.$nameMarca.'</span>
                        </div>
                    </a>';
        endwhile;
        
        $posts_marcas[0] = $the_query->post_count;
        $posts_marcas[1] = $text;
        wp_reset_query();
    else:
        $posts_marcas[0] = 0;
        $posts_marcas[1] = '<p class="content-not-found">'.__('No hay marcas', 'ccu-intranet').'</p>';
    endif;


echo json_encode($posts_marcas);