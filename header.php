<!doctype html>
<html lang="es" <?php language_attributes() ?>>

<head profile="http://gmpg.org/xfn/11">
    <title><?php echo esc_html( get_bloginfo('name'), 1 ); wp_title( '|', true, 'left' ); ?></title>
    <meta http-equiv="content-type" content="<?php bloginfo('html_type') ?>; charset=<?php bloginfo('charset') ?>" />
    <meta name="google-signin-client_id" content="<?php echo GOOGLE_CLIENT_ID?>">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url') ?>" />
    <?php if ( get_field( 'favicon', 'option' ) ) { ?>
    <link rel="shortcut icon" href="<?php the_field( 'favicon', 'option' ); ?>" type="image/x-icon">
    <?php } else { ?>
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">
    <?php } ?>

    <?php wp_head() ?>

    <script src="https://apis.google.com/js/api:client.js"></script>

    <script src="https://www.youtube.com/iframe_api"></script>

    <link rel="stylesheet" type="text/css"
        href="<?php echo get_stylesheet_directory_uri(); ?>/components/custom/js/highlight/androidstudio.css" />
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/components/custom/js/highlight/highlight.pack.js"
        type="text/javascript"></script>
    <script>
    hljs.initHighlightingOnLoad();
    </script>

    <meta name="viewport"
        content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />
    <script>
    $(document).ready(function() {
        var currCountry = '<?php echo $_SESSION['intranet_ccu_session']->COUNTRY; ?>';

        $.ajax({
            type: 'POST',
            data: {
                country: "CHL"
            },
            url: '<?php echo network_site_url()?>wp-json/ccu/get_dolar',
            success: function(data) {
                var source = '';
                var quantity = '';
                var amount = '';
                var target = '';

                if (typeof data != 'undefined' && typeof data.result != 'undefined') {
                    var source = data.result.source;
                    var quantity = data.result.quantity;
                    var amount = data.result.amount;
                    var target = data.result.target;
                }

                $('#dolar-value').text(quantity + ' ' + source);
                $('#currency-value').text(amount + ' ' + target);
            }
        });
    });
    </script>

    <?php the_field('ga_code', 'option'); ?>
</head>

<body>
    <?php
    $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
    if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0; rv:11.0') !== false)) { ?>
    <div class="not-compatible">
        <div class="box-info">
            <p class="disclaimer">Lo sentimos, tu navegador <b>no es compatible con este sitio.</b><br>Por favor
                descarga uno
                de los siguientes navegadores para visualizar el sitio:</p>
            <div class="browsers-area">
                <div class="browser-box">
                    <a href="https://www.google.com/chrome/" target="_blank" class="browser-link">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/chrome-logo.png" alt=""
                            class="logo-browser">
                    </a>
                </div>
                <div class="browser-box">
                    <a href="https://www.mozilla.org/es-CL/firefox/new/" target="_blank" class="browser-link">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/firefox-logo.png" alt=""
                            class="logo-browser">
                    </a>
                </div>
                <div class="browser-box">
                    <a href="https://www.microsoft.com/es-es/edge" target="_blank" class="browser-link">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/edge-logo.png" alt=""
                            class="logo-browser">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <header id="side-header">
        <div class="mobile-menu-heading">
            <div class="menu-title">
                <span>Menú</span>
            </div>
            <div class="close-menu-area">
                <span id="close-menu"><i class="icon-equis"></i></span>
            </div>
        </div>
        <div class="brand-area">
            <a href="<?php echo site_url('/'); ?>">
                <?php $logo_sitio = get_field( 'logo_sitio', 'option' ); ?>
                <?php if ( $logo_sitio ) { ?>
                <img src="<?php echo $logo_sitio['url']; ?>" alt="<?php echo $logo_sitio['alt']; ?>"
                    class="logo-site" />
                <?php } ?>
            </a>
        </div>
        <div class="main-menu-area">
            <ul class="main-menu">
                <?php
                wp_nav_menu(array('items_wrap' => '%3$s', 'theme_location'=> 'Menu principal', 'container_class' => false, 'container' => false));
                ?>
            </ul>
        </div>
        <div class="extra-links-area">
            <div class="menu-heading">
                <p>Link de Interés</p>
            </div>
            <div class="extra-menu-area">
                <ul class="extra-menu">
                    <?php
                    wp_nav_menu(array('items_wrap' => '%3$s', 'theme_location'=> 'Menu extra', 'container_class' => false, 'container' => false));?>
                </ul>
            </div>
        </div>
    </header>

    <main>
        <div class="top-header">
            <div class="mobile-pre-header">
                <div class="brand-area">
                    <a href="<?php echo site_url('/'); ?>">
                        <?php $logo_sitio = get_field( 'logo_sitio', 'option' ); ?>
                        <?php if ( $logo_sitio ) { ?>
                        <img src="<?php echo $logo_sitio['url']; ?>" alt="<?php echo $logo_sitio['alt']; ?>"
                            class="logo-site" />
                        <?php } ?>
                    </a>
                </div>
                <div id="hamburger" class="menu-mobile-open">
                    <span class="line-1"></span>
                    <span class="line-2"></span>
                    <span class="line-3"></span>
                </div>
            </div>
            <div class="pre-head">
                <div class="wrap-xl">
                    <div class="content">
                        <div class="currency-area">
                            <div class="uf-valor">
                                <span class="name">Valor UF</span>
                                <span class="value">1 UF</span>
                                <i class="icon-arrow-right"></i>
                                <span id="uf-value" class="value">...</span>
                            </div>
                        </div>
                        <div id="time-area" class="time-area">
                            <i class="icon-clock"></i>
                            <a href="#" class="prev arrow"><i class="icon-chevron-left"></i></a>
                            <div class="time-slider-area">
                                <div id="time-slider">
                                    <div class="slide">
                                        <div class="time-data" data-timezone="0">
                                            <span>Hora Local</span>
                                            <span class="country">Chile</span>
                                            <span id="chile" class="time"></span>
                                        </div>
                                    </div>
                                    <div class="slide">
                                        <div class="time-data" data-timezone="0">
                                            <span>Hora Local</span>
                                            <span class="country">Argentina</span>
                                            <span id="argentina" class="time"></span>
                                        </div>
                                    </div>
                                    <div class="slide">
                                        <div class="time-data" data-timezone="-1">
                                            <span>Hora Local</span>
                                            <span class="country">Bolivia</span>
                                            <span id="bolivia" class="time"></span>
                                        </div>
                                    </div>
                                    <div class="slide">
                                        <div class="time-data" data-timezone="-2">
                                            <span>Hora Local</span>
                                            <span class="country">Colombia</span>
                                            <span id="colombia" class="time"></span>
                                        </div>
                                    </div>
                                    <div class="slide">
                                        <div class="time-data" data-timezone="0">
                                            <span>Hora Local</span>
                                            <span class="country">Paraguay</span>
                                            <span id="paraguay" class="time"></span>
                                        </div>
                                    </div>
                                    <div class="slide">
                                        <div class="time-data" data-timezone="0">
                                            <span>Hora Local</span>
                                            <span class="country">Uruguay</span>
                                            <span id="uruguay" class="time"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a href="#" class="next arrow"><i class="icon-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mid-head">
                <div class="wrap-xl">
                    <div class="content">
                        <div class="today-div">
                            <span id="current-date"></span>
                        </div>
                        <div class="countries-area">
                            <p class="slogan">Nos apasiona crear experiencia para compartir juntos un mejor vivir</p>
                            <div class="flags-area">
                                <ul class="flag-list">
                                    <li <?php if($_SESSION['intranet_ccu_session']->COUNTRY =='CHL'){ ?>class="current-flag"
                                        <?php } ?>>
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/flag-chile.svg" alt=""
                                            class="flag"><span>Chile</span>
                                    </li>
                                    <li <?php if($_SESSION['intranet_ccu_session']->COUNTRY =='ARG'){ ?>class="current-flag"
                                        <?php } ?>>
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/flag-argentina.svg"
                                            alt="" class="flag"><span>Argentina</span>
                                    </li>
                                    <li <?php if($_SESSION['intranet_ccu_session']->COUNTRY =='BOL'){ ?>class="current-flag"
                                        <?php } ?>>
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/flag-bolivia.svg"
                                            alt="" class="flag"><span>Bolivia</span>
                                    </li>
                                    <li <?php if($_SESSION['intranet_ccu_session']->COUNTRY =='COL'){ ?>class="current-flag"
                                        <?php } ?>>
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/flag-colombia.svg"
                                            alt="" class="flag"><span>Colombia</span>
                                    </li>
                                    <li <?php if($_SESSION['intranet_ccu_session']->COUNTRY =='PAR'){ ?>class="current-flag"
                                        <?php } ?>>
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/flag-paraguay.svg"
                                            alt="" class="flag"><span>Paraguay</span>
                                    </li>
                                    <li <?php if($_SESSION['intranet_ccu_session']->COUNTRY =='URU'){ ?>
                                        class="current-flag" <?php } ?>>
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/flag-uruguay.svg"
                                            alt="" class="flag"><span>Uruguay</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bot-head">
                <div class="wrap-xl">
                    <div class="content">
                        <form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <div class="search-input-area">
                                <input type="search"
                                    placeholder="<?php echo esc_attr_x( 'Realiza una búsqueda', 'placeholder', 'base' ); ?>"
                                    value="<?php echo get_search_query(); ?>" name="s" id="search-box">
                                <button type="submit" class="btn-search"><i class="icon-buscar"></i></button>
                            </div>
                        </form>
                        <div class="user-area">
                            <div class="avatar-box">
                                <div class="avatar">
                                    <img src="<?php echo get_photo_url($_SESSION['intranet_ccu_session']->EMPLID); ?>"
                                        alt="" class="profile">
                                </div>
                            </div>
                            <div class="info-box">
                                <div class="name">
                                    <span><?php echo implode(' ', (array_filter([$_SESSION['intranet_ccu_session']->FIRST_NAME, $_SESSION['intranet_ccu_session']->MIDDLE_NAME, $_SESSION['intranet_ccu_session']->LAST_NAME, $_SESSION['intranet_ccu_session']->SECOND_LAST_NAME]))); ?></span><span
                                        class="icono"><i class="icon-chevron-down"></span></i>
                                </div>
                                <div class="range">
                                    <span><?php echo $_SESSION['intranet_ccu_session']->JOBCODE_NAME; ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="user-menu">
                            <?php if ( have_rows( 'menu_usuario', 'option' ) ) : ?>
                            <div class="links">
                                <?php while ( have_rows( 'menu_usuario', 'option' ) ) : the_row(); ?>
                                <?php $link_user_menu = get_sub_field( 'link_user_menu' ); ?>
                                <?php if ( $link_user_menu ) { ?>
                                <a href="<?php echo $link_user_menu['url']; ?>"
                                    target="<?php echo $link_user_menu['target']; ?>" class="out-link"><i
                                        class="icon-out-link"></i><span><?php echo $link_user_menu['title']; ?></span></a>
                                <?php } ?>
                                <?php endwhile; ?>
                            </div>
                            <?php endif; ?>

                            <div class="text-center logout-btn">
                                <a onclick="signOut()" style="display: none" class="log-out-load-button">
                                    <span>Cerrar Sesión</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>