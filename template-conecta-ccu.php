<?php
/*
* Template Name: Conecta CCU
*/
get_header();
?>
<section class="section">
    <div class="wrap-xl">
        <div class="page-heading conecta-heading">
            <?php
            $pageThumbImg = get_the_post_thumbnail_url();
            $pageThumbnailID = get_post_thumbnail_ID();
            $alt = get_post_meta ( $pageThumbnailID, '_wp_attachment_image_alt', true );
            ?>
            <div class="bg-image cover" style="background-image: url(<?php echo $pageThumbImg; ?>)"
                title="<?php echo $alt; ?>">
                <div class="veil"></div>
            </div>
            <div class="content">
                <h1><span><?php the_title(); ?></span><img
                        src="<?php echo get_template_directory_uri(); ?>/img/logo-ccu.svg" alt=""></h1>
                <div class="intro-page">
                    <?php the_field( 'introduccion' ); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if ( have_rows( 'contenido_pagina' ) ): ?>
<section class="section content-page-area">
    <div class="wrap-xl">
        <div class="block-content wysiwyg">
            <?php while ( have_rows( 'contenido_pagina' ) ) : the_row(); ?>
            <?php if ( get_row_layout() == 'bloque_de_texto' ) : ?>
            <?php the_sub_field( 'bloque_contenido' ); ?>
            <?php elseif ( get_row_layout() == 'contenido_destacado' ) : ?>
            <div class="feat-block">
                <div class="feat-content">
                    <h4 class="title"><?php the_sub_field( 'titulo_cd' ); ?></h4>
                    <?php the_sub_field( 'bajada_cd' ); ?>
                </div>
                <div class="feat-image">
                    <?php $imagen_rel_cd = get_sub_field( 'imagen_rel_cd' ); ?>
                    <div class="photo cover" style="background-image: url(<?php echo $imagen_rel_cd['url']; ?>);"
                        title="<?php echo $imagen_rel_cd['alt']; ?>"></div>
                </div>
            </div>
            <?php elseif ( get_row_layout() == 'equipo' ) : ?>
            <?php if ( have_rows( 'personas_e' ) ) : ?>
            <div class="person-group-area small-group">
                <?php while ( have_rows( 'personas_e' ) ) : the_row(); ?>
                <div class="person-box">
                    <?php $foto_p_e = get_sub_field( 'foto_p_e' ); ?>
                    <div class="person-img cover" style="background-image: url(<?php echo $foto_p_e['url']; ?>)"
                        title="<?php echo $foto_p_e['alt']; ?>">
                    </div>
                    <div class="person-info">
                        <span class="cargo"><?php the_sub_field( 'cargo_p_e' ); ?></span>
                        <h4 class="nombre"><?php the_sub_field( 'nombre_p_e' ); ?></h4>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
            <?php endif; ?>
            <?php endif; ?>
            <?php endwhile; ?>
        </div>
    </div>
</section>
<?php endif; ?>
<?php get_footer(); ?>