<?php

//SESIONES NO TOCAR
function ccu_start_session() {
    if(!session_id()) {
        session_start();
    }
}
add_action('init', 'ccu_start_session', 1);



function is_logged_in(){
    return (
        !empty($_SESSION['intranet_ccu_session']) &&
        !empty($_SESSION['intranet_ccu_session']->EMAIL)
    );
}

function get_country_code(){
    if(!empty($_SESSION['intranet_ccu_session']->COUNTRY) && in_array($_SESSION['intranet_ccu_session']->COUNTRY,  get_country_bd_array())){
        return get_country_bd_array('code')[$_SESSION['intranet_ccu_session']->COUNTRY];
    }
    return null;
}

function get_country_bd_array($mode = false){
    $countries      =
    [
        [
            'country'   => 'CHL',
            'code'      => 'cl',
            'homepage'  => 'chile',
            'types'     =>
            [
                'marcas_cl',
                'noticias_cl',
                'pagina_cl',
                'videos_cl',
                'cambios',
            ],
        ],
        [
            'country'   => 'ARG',
            'code'      => 'ar',
            'homepage'  => null,
            'types'     =>
            [
                'marcas_ar',
                'noticias_ar',
                'pagina_ar',
                'cambios_ar',
            ],
        ],
        [
            'country'   => 'BOL',
            'code'      => 'bo',
            'homepage'  => null,
            'types'     =>
            [
                'marcas_bo',
                'noticias_bo',
                'pagina_bo',
                'cambios_bo',
            ],
        ],
        [
            'country'   => 'COL',
            'code'      => 'co',
            'homepage'  => null,
            'types'     =>
            [
                'marcas_co',
                'noticias_co',
                'pagina_co',
                'cambios_co',
            ],
        ],
        [
            'country'   => 'PAR',
            'code'      => 'py',
            'homepage'  => null,
            'types'     =>
            [
                'marcas_py',
                'noticias_py',
                'pagina_py',
                'cambios_py',
            ],
        ],
        [
            'country'   => 'URU',
            'code'      =>'uy',
            'homepage'  => null,
            'types'     =>
            [
                'marcas_uy',
                'noticias_uy',
                'pagina_uy',
                'cambios_uy',
            ],
        ],
    ];

    if($mode === 'code'){
        return array_column($countries, 'code', 'country');
    }
    if($mode === 'homepage'){
        return array_column($countries, 'homepage', 'country');
    }
    if($mode === 'types'){
        return array_column($countries, 'types', 'country');
    }
    return array_column($countries, 'country');
}

function get_country_home(){
    if(!empty($_SESSION['intranet_ccu_session']->COUNTRY) && in_array($_SESSION['intranet_ccu_session']->COUNTRY,  get_country_bd_array())){
        return get_country_bd_array('homepage')[$_SESSION['intranet_ccu_session']->COUNTRY];
    }
    return null;
}

function get_photo_url($emplid){
    if(!empty($emplid) && file_exists(ABSPATH.'/wp-content/uploads/ccu_avatars/'.$emplid.'.JPG')){
        return network_site_url().'/wp-content/uploads/ccu_avatars/'.$emplid.'.JPG';
    }else{
        return get_template_directory_uri().'/img/user-img-default.jpg';
    }
}

function valid_type($type){
    if(!empty($_SESSION['intranet_ccu_session']->COUNTRY) && in_array($_SESSION['intranet_ccu_session']->COUNTRY,  get_country_bd_array())){
        return in_array($type, get_country_bd_array('types')[$_SESSION['intranet_ccu_session']->COUNTRY]);
    }
    return false;
}

function can_pass(){
    $is_logged_in = is_logged_in();
    if(
        $is_logged_in  &&
        in_array(
            (!empty($_SERVER['HTTPS'])?'https://':'http://').$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
            [
                get_site_url().'/login',
                get_site_url().'/login/'
            ]
        )
    ){
        wp_redirect(get_site_url());
        exit;
    }elseif(
        $is_logged_in ||
        in_array(
            explode('?',(!empty($_SERVER['HTTPS'])?'https://':'http://').$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'])[0],
            [
                get_site_url().'/wp-json/ccu/login',
                get_site_url().'/wp-json/ccu/login/',
                get_site_url().'/wp-json/ccu/logout/',
                get_site_url().'/wp-json/ccu/logout/',
                get_site_url().'/login',
                get_site_url().'/login/',
                get_site_url().'/wp-admin',
                get_site_url().'/wp-admin/',
                get_site_url().'/wp-login.php',
                get_site_url().'/wp-login.php/',
            ]
        ) ||
        is_admin()
    ){
        return;
    }else{
        $uri_segment = '';
        if ($_SERVER['REQUEST_URI'] != null) {
            $uri_segment = urldecode($_SERVER['REQUEST_URI']);
            $patterns = [];
            $patterns[] = '/\<script\>(.*?)\<\/script\>/si';
            $patterns[] = '/src[\r\n]*=[\r\n]*\'(.*?)*\'/si';
            $patterns[] = '/src[\r\n]*=[\r\n]*\"(.*?)*\"/si';
            $patterns[] = '/\<\/script\>/si';
            $patterns[] = '/\<script(.*?)\>/si';
            $patterns[] = '/eval\((.*?)\)/si';
            $patterns[] = '/expression\((.*?)\)/si';
            $patterns[] = '/javascript:/si';
            $patterns[] = '/vbscript:/si';
            $patterns[] = '/onload(.*?)=/si';
            foreach ($patterns as $key => $value) {
                $uri_segment = preg_replace($value, '', $uri_segment);
            }
        }
        wp_redirect(network_site_url().'login/?redirect='.urlencode($uri_segment));
        exit;
    }
}
if (php_sapi_name() != 'cli') {
    add_action ('init', 'can_pass');
}

function redirect_country() {
    if(is_logged_in()){
        $current_url = (!empty($_SERVER['HTTPS'])?'https://':'http://').$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        if(
            strpos(
                $current_url,
                network_site_url().get_country_home()
            )=== FALSE &&
            strpos(
                $current_url,
                network_site_url().'wp-json'
            )=== FALSE &&
            strpos(
                $current_url,
                get_site_url().'/wp-json'
            )=== FALSE &&
            strpos(
                $current_url,
                network_site_url().'wp-admin'
            )=== FALSE &&
            strpos(
                $current_url,
                network_site_url().'wp-login'
            )=== FALSE
        ){
            wp_redirect(network_site_url().get_country_home());
            exit;
        }
    }
}
add_action('init', 'redirect_country');




function do_login(){
    if(!empty($_POST['idtoken'])){
        require_once 'vendor/autoload.php';
        $client = new Google_Client(['client_id' => GOOGLE_CLIENT_ID]);
        $payload = $client->verifyIdToken($_POST['idtoken']);
        if (!empty($payload) && !empty($payload['email'])) {
            global $wpdb;
            $users_db_name ='ccu_users_master';
            $user_db_query = $wpdb->prepare( "SELECT * FROM $users_db_name WHERE EMAIL = %s", [$payload['email']] );
            $user_db = $wpdb->get_results($user_db_query);
            if(count($user_db) == 1){
                $user_db = $user_db[0];
                if(in_array($user_db->COUNTRY, get_country_bd_array() )){
                    $_SESSION['intranet_ccu_session'] = $user_db;
                    return ['status' => 'ok'];
                }
            }
        }
        return ['status' => 'error', 'message'=>'Cuenta no válida.'];
    }
    return ['status' => 'error', 'message'=>'Error inesperado.'];
}

add_action('rest_api_init', function() {
    register_rest_route( 'ccu', '/login/',
        array(
            'methods' => 'POST',
            'callback' => 'do_login'
        )
    );
});


function do_logout(){
    session_destroy();
    return ['status' => 'ok'];
}

add_action( 'rest_api_init', function() {
    register_rest_route( 'ccu', '/logout/',
        array(
            'methods' => ['GET', 'POST'],
            'callback' => 'do_logout'
        )
    );
});
//FIN SESIONES NO TOCAR

//ORGANIGRAMA

function get_suggestions( $name_search ){
    global $wpdb;
    $users_db_name ='ccu_users_master';
    $organization_chart_info = array();
    
    $array_search = explode(' ', $name_search);

    $query  = "SELECT * FROM $users_db_name WHERE ";
    $values = [];
    foreach ($array_search as $key => $value){
        if($key == 0){
            $query .= "FULL_NAME LIKE %s";
        }else{
            $query .= " AND FULL_NAME LIKE %s";
        }
        array_push($values, '%'.$value.'%');
    }
    $user_db_query = $wpdb->prepare( $query , $values );
    $users_db = $wpdb->get_results($user_db_query);
    
    if(count($users_db) > 0){
        $organization_chart_info = $users_db;
    }else{
        return ['status' => 'empty', 'message'=>'No se encontraron coincidencias.'];
    }

    return ['status' => 'ok', 'data'=> $organization_chart_info];
}

function get_organization_chart( $idEmployee = NULL ){
    global $wpdb;
    $users_db_name ='ccu_users_master';
    $organization_chart_info = array();

    if($idEmployee){
        $user_db_query = $wpdb->prepare( "SELECT * FROM $users_db_name WHERE EMPLID = %s", $idEmployee );
        $user_db = $wpdb->get_results($user_db_query);

        if(count($user_db) == 1){
            $supervisor_id = $user_db[0]->EMPLID;
            $emplid_id     = $user_db[0]->SUPERVISOR_ID;
            $organization_chart_info['user'] = $user_db[0];
        }else{
            return ['status' => 'empty', 'message'=>'No se encontraron coincidencias.'];
        }
    }else{
        $supervisor_id = $_SESSION['intranet_ccu_session']->EMPLID;
        $emplid_id     = $_SESSION['intranet_ccu_session']->SUPERVISOR_ID;
    }

    $employee_db_query = $wpdb->prepare( "SELECT * FROM $users_db_name WHERE SUPERVISOR_ID = %s", $supervisor_id );
    $organization_chart_info['employee'] = $wpdb->get_results($employee_db_query);

    $supervisor_db_query = $wpdb->prepare( "SELECT * FROM $users_db_name WHERE EMPLID = %s", $emplid_id );
    $supervisor_db = $wpdb->get_results($supervisor_db_query);

    if(count($supervisor_db) == 1){
        $supervisor_db = $supervisor_db[0];
        $organization_chart_info['supervisor'] = $supervisor_db;
    }else{
        $organization_chart_info['supervisor'] = NULL;
    }

    return ['status' => 'ok', 'data'=> $organization_chart_info];
}

//FIN ORGANIGRAMA

//CUMPLEAÑOS
function get_birthday( $country ){
    if($country == 'CHL'){
        date_default_timezone_set('Chile/Continental');
    }

    //$date = date('d-m');
    $date = date('j/n/');
    setlocale(LC_TIME, "es_ES");
    $date_text = strftime('%e de %B');

    global $wpdb;
    $users_db_name ='ccu_users_master';

    $birthday_db_query = $wpdb->prepare( "SELECT * FROM $users_db_name WHERE COUNTRY = %s AND BIRTHDATE LIKE %s", array($country, $date.'%') );
    $birthday_db = $wpdb->get_results($birthday_db_query);

    if(count($birthday_db) > 0){
        return ['status' => 'ok', 'data'=> ['date' => $date_text, 'employees' => $birthday_db ]];
    }else{
        return ['status' => 'empty', 'message'=>'No se encontraron cumpleaños para hoy.'];
    }
}
//FIN CUMPLEAÑOS

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Opciones Generales',
		'menu_title'	=> 'Opciones',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
    ));
}

function my_acf_google_map_api( $api ){
    $api['key'] = 'AIzaSyCqowcldWZuq9N5PVpR3u38kBqLQitXZMQ';
    return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
/*-------------------------------------------------------------*/
/*--- No mostrar la version de Wordpress dentro del <head> ----*/
/*-------------------------------------------------------------*/
function eliminar_version_wordpress() {
return '';
}
add_filter('the_generator', 'eliminar_version_wordpress');


/*-------------------------------------------------------------*/
/*-------------- Eliminar barra de administración -------------*/
/*-------------------------------------------------------------*/
function quitar_barra_administracion()  {
    return false;
}
add_filter( 'show_admin_bar' , 'quitar_barra_administracion');



/*-------------------------------------------------------------*/
/*--------- Permito imagen destacada en los Posts -------------*/
/*-------------------------------------------------------------*/
if ( function_exists( 'add_theme_support' ) )
add_theme_support( 'post-thumbnails' );

/*-------------------------------------------------------------*/
/*------------------ Menús personalizados ---------------------*/
/*-------------------------------------------------------------*/
register_nav_menus( array(
    'Menu principal' => 'Menu principal',
    'Menu extra' => 'Menu extra',
    'Menu footer' => 'Menu footer',
));

/*-------------------------------------------------------------*/
/*--- Le añado la clase "active" al elemento actual del menu ---*/
/*-------------------------------------------------------------*/
add_filter('nav_menu_css_class', function ($classes, $item, $args, $depth) {
    //Todas las diferentes clases "active" añadidas por WordPress
    $active = [
        'current-menu-item',
        'current-menu-parent',
        'current-menu-ancestor',
        'current_page_item'
    ];
    //Si coincide, añade la clase "active"
    if ( array_intersect( $active, $classes ) ) {
        $classes[] = 'active';
    }
    return $classes;
}, 10, 4);

/*----------------------------------------------------------------------------*/
/*---------------------------- Eliminar Gutenberg ----------------------------*/
/*----------------------------------------------------------------------------*/
add_filter('use_block_editor_for_post', '__return_false', 10);
add_filter('use_block_editor_for_post_type', '__return_false', 10);

/*-------------------------------------------------------------*/
/*--------- Compatibilidad del tema con Woocommerce -----------*/
/*-------------------------------------------------------------*/
function my_theme_setup() {
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'my_theme_setup' );


/*-------------------------------------------------------------*/
/*------------- Añadimos los CSS y JS del Theme ---------------*/
/*-------------------------------------------------------------*/
function merlin_styles_and_scripts() {
    wp_deregister_script( 'jquery' );

    //jQuery
    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/components/merlin/js/jquery/jquery-3.5.1.min.js');

    //jQuery UI
    wp_enqueue_script( 'jquery-ui', get_template_directory_uri() . '/components/merlin/js/jquery/jquery-ui.js');

    //Masonry
    wp_enqueue_script( 'masonry', get_template_directory_uri() . '/components/custom/js/masonry.pkgd.min.js');

    //Lazy Loading
    wp_enqueue_script( 'lazysizes', get_template_directory_uri() . '/components/merlin/js/lazysizes.js');

    //Slick slider
    wp_enqueue_script( 'slick', get_template_directory_uri() . '/components/merlin/js/slick/slick.min.js');
    wp_enqueue_style( 'slick-style', get_template_directory_uri() . '/components/merlin/js/slick/slick.css');

    //Lightgallery
    wp_enqueue_script( 'lightgallery', get_template_directory_uri() . '/components/merlin/js/lightgallery/lightgallery.js');
    wp_enqueue_style( 'lightgallery-style', get_template_directory_uri() . '/components/merlin/js/lightgallery/lightgallery.css');

    //AOS
    wp_enqueue_script( 'aos', get_template_directory_uri() . '/components/merlin/js/aos/aos.js');
    wp_enqueue_style( 'aos-style', get_template_directory_uri() . '/components/merlin/js/aos/aos.css');

    //Scripts de Merlín
    wp_enqueue_script( 'scripts', get_template_directory_uri() . '/components/merlin/js/scripts.js');
    wp_enqueue_script( 'simple-parallax', get_template_directory_uri() . '/components/merlin/js/simpleParallax.min.js');
    wp_enqueue_script( 'modal', get_template_directory_uri() . '/components/merlin/js/modal.js');
    wp_enqueue_script( 'toggle', get_template_directory_uri() . '/components/merlin/js/toggle.js');
    wp_enqueue_script( 'header', get_template_directory_uri() . '/components/merlin/js/header.js');
    wp_enqueue_script( 'animacion-anclas', get_template_directory_uri() . '/components/merlin/js/animacion-anclas.js');

    //Scripts de Organismos: Sliders
    wp_enqueue_script( 'slider-1', get_template_directory_uri() . '/organisms/sliders/01/js/slider-1.js');
    wp_enqueue_script( 'slider-2', get_template_directory_uri() . '/organisms/sliders/02/js/slider-2.js');
    wp_enqueue_script( 'slider-4', get_template_directory_uri() . '/organisms/sliders/04/js/slider-4.js');
    wp_enqueue_script( 'slider-5', get_template_directory_uri() . '/organisms/sliders/05/js/slider-5.js');
    wp_enqueue_script( 'slider-6', get_template_directory_uri() . '/organisms/sliders/06/js/slider-6.js');
    wp_enqueue_script( 'slider-7', get_template_directory_uri() . '/organisms/sliders/07/js/slider-7.js');

    //Scripts de Organismos: Bloques
    //Bloque #3
    wp_enqueue_script( 'count-up', get_template_directory_uri() . '/organisms/blocks/03/js/count-up.js');
    wp_enqueue_script( 'in-view', get_template_directory_uri() . '/organisms/blocks/03/js/in-view.js');
    wp_enqueue_script( 'block-3', get_template_directory_uri() . '/organisms/blocks/03/js/block-3.js');
    //Bloque #5
    wp_enqueue_script( 'block-5', get_template_directory_uri() . '/organisms/blocks/05/js/block-5.js');
    wp_enqueue_script( 'site-scripts', get_template_directory_uri() . '/components/custom/js/scripts.js');
    
    $front_info =
    array(
        'themeurl'          => get_template_directory_uri(),
        'network_site_url'  => network_site_url(),
        'base_url'          => (!empty($_SERVER['HTTPS'])?'https://':'http://').$_SERVER['HTTP_HOST'],
        'GOOGLE_CLIENT_ID'  => GOOGLE_CLIENT_ID,
        'site_url'          => get_site_url(),
    );
    if(!empty( $_SESSION['intranet_ccu_session'])) {
        $front_info['sessioncountry'] = $_SESSION['intranet_ccu_session']->COUNTRY;
    }
    wp_localize_script( 'site-scripts', 'front_info', $front_info);

}

add_action( 'wp_enqueue_scripts', 'merlin_styles_and_scripts' );

// Register Custom Post Type marcas
function custom_post_type_marcas_cl() {

    $labels = array(
        'name'                  => _x( 'Marcas', 'Post Type General Name', 'ccu-intranet' ),
        'singular_name'         => _x( 'Marca', 'Post Type Singular Name', 'ccu-intranet' ),
        'menu_name'             => __( 'Marcas', 'ccu-intranet' ),
        'name_admin_bar'        => __( 'Marcas', 'ccu-intranet' ),
        'archives'              => __( 'Archivo', 'ccu-intranet' ),
        'attributes'            => __( 'Atributos', 'ccu-intranet' ),
        'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
        'all_items'             => __( 'Todos', 'ccu-intranet' ),
        'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
        'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
        'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
        'edit_item'             => __( 'Editar', 'ccu-intranet' ),
        'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
        'view_item'             => __( 'Ver', 'ccu-intranet' ),
        'view_items'            => __( 'Ver', 'ccu-intranet' ),
        'search_items'          => __( 'Buscar', 'ccu-intranet' ),
        'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
        'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
        'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
        'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
        'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
        'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
        'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
        'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
        'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
        'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
        'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
    );
    $args = array(
        'label'                 => __( 'Marcas', 'ccu-intranet' ),
        'description'           => __( 'Marcas de CCU', 'ccu-intranet' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 10,
        'menu_icon'             => 'dashicons-beer',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'marcas_cl', $args );

}
add_action( 'init', 'custom_post_type_marcas_cl', 0 );

// Asignar single-marca.php a todas las marcas por país
add_filter( 'template_include', function( $template ) {
    if ( is_singular( array( 'marcas_cl', 'marcas_ar', 'marcas_bo', 'marcas_co', 'marcas_py', 'marcas_uy' ) ) ) {
      $locate = locate_template( 'single-marcas.php', false, false );
      if ( ! empty( $locate ) ) {
        $template = $locate;
      }
    }
    return $template;
  });

function custom_post_type_videos_cl() {


    $labels = array(
        'name'                  => _x( 'Videos', 'Post Type General Name', 'ccu-intranet' ),
        'singular_name'         => _x( 'Video', 'Post Type Singular Name', 'ccu-intranet' ),
        'menu_name'             => __( 'Videos', 'ccu-intranet' ),
        'name_admin_bar'        => __( 'Videos', 'ccu-intranet' ),
        'archives'              => __( 'Archivo', 'ccu-intranet' ),
        'attributes'            => __( 'Atributos', 'ccu-intranet' ),
        'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
        'all_items'             => __( 'Todos', 'ccu-intranet' ),
        'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
        'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
        'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
        'edit_item'             => __( 'Editar', 'ccu-intranet' ),
        'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
        'view_item'             => __( 'Ver', 'ccu-intranet' ),
        'view_items'            => __( 'Ver', 'ccu-intranet' ),
        'search_items'          => __( 'Buscar', 'ccu-intranet' ),
        'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
        'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
        'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
        'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
        'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
        'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
        'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
        'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
        'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
        'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
        'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
    );
    $args = array(
        'label'                 => __( 'Videos', 'ccu-intranet' ),
        'description'           => __( 'Videos de CCU Chile', 'ccu-intranet' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 10,
        'menu_icon'             => 'dashicons-video-alt2',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'videos_cl', $args );
}
add_action( 'init', 'custom_post_type_videos_cl', 0 );

// Asignar single-marca.php a todas las marcas por país
add_filter( 'template_include', function( $template ) {
    if ( is_singular( array( 'videos_cl') ) ) {
      $locate = locate_template( 'single-videos.php', false, false );
      if ( ! empty( $locate ) ) {
        $template = $locate;
      }
    }
    return $template;
  });

function custom_post_type_cambios() {

    $labels = array(
        'name'                  => _x( 'Cambios', 'Post Type General Name', 'ccu-intranet' ),
        'singular_name'         => _x( 'Cambio', 'Post Type Singular Name', 'ccu-intranet' ),
        'menu_name'             => __( 'Cambios', 'ccu-intranet' ),
        'name_admin_bar'        => __( 'Cambios', 'ccu-intranet' ),
        'archives'              => __( 'Archivo', 'ccu-intranet' ),
        'attributes'            => __( 'Atributos', 'ccu-intranet' ),
        'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
        'all_items'             => __( 'Todos', 'ccu-intranet' ),
        'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
        'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
        'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
        'edit_item'             => __( 'Editar', 'ccu-intranet' ),
        'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
        'view_item'             => __( 'Ver', 'ccu-intranet' ),
        'view_items'            => __( 'Ver', 'ccu-intranet' ),
        'search_items'          => __( 'Buscar', 'ccu-intranet' ),
        'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
        'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
        'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
        'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
        'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
        'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
        'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
        'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
        'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
        'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
        'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
    );
    $args = array(
        'label'                 => __( 'Cambios organizacionales', 'ccu-intranet' ),
        'description'           => __( 'Cambios organizacionales', 'ccu-intranet' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 10,
        'menu_icon'             => 'dashicons-groups',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'cambios', $args );

}
add_action( 'init', 'custom_post_type_cambios', 0 );

function custom_post_type_pins() {

    $labels = array(
        'name'                  => _x( 'Direcciones', 'Post Type General Name', 'ccu-intranet' ),
        'singular_name'         => _x( 'Dirección', 'Post Type Singular Name', 'ccu-intranet' ),
        'menu_name'             => __( 'Direcciones', 'ccu-intranet' ),
        'name_admin_bar'        => __( 'Direcciones', 'ccu-intranet' ),
        'archives'              => __( 'Archivo', 'ccu-intranet' ),
        'attributes'            => __( 'Atributos', 'ccu-intranet' ),
        'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
        'all_items'             => __( 'Todos', 'ccu-intranet' ),
        'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
        'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
        'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
        'edit_item'             => __( 'Editar', 'ccu-intranet' ),
        'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
        'view_item'             => __( 'Ver', 'ccu-intranet' ),
        'view_items'            => __( 'Ver', 'ccu-intranet' ),
        'search_items'          => __( 'Buscar', 'ccu-intranet' ),
        'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
        'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
        'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
        'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
        'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
        'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
        'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
        'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
        'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
        'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
        'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
    );
    $args = array(
        'label'                 => __( 'Direcciones', 'ccu-intranet' ),
        'description'           => __( 'Direcciones', 'ccu-intranet' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 10,
        'menu_icon'             => 'dashicons-location',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'direcciones', $args );

}
add_action( 'init', 'custom_post_type_pins', 0 );

// Register Custom Taxonomy
function marca_taxonomy_pais() {

    $labels = array(
        'name'                       => _x( 'Países', 'Taxonomy General Name', 'ccu-intranet' ),
        'singular_name'              => _x( 'País', 'Taxonomy Singular Name', 'ccu-intranet' ),
        'menu_name'                  => __( 'Países', 'ccu-intranet' ),
        'all_items'                  => __( 'Todos', 'ccu-intranet' ),
        'parent_item'                => __( 'Item Relacionado', 'ccu-intranet' ),
        'parent_item_colon'          => __( 'Item Relacionado:', 'ccu-intranet' ),
        'new_item_name'              => __( 'Añadir nuevo', 'ccu-intranet' ),
        'add_new_item'               => __( 'Añadir nuevo', 'ccu-intranet' ),
        'edit_item'                  => __( 'Editar', 'ccu-intranet' ),
        'update_item'                => __( 'Actualizar', 'ccu-intranet' ),
        'view_item'                  => __( 'Ver', 'ccu-intranet' ),
        'separate_items_with_commas' => __( 'Separar con coma', 'ccu-intranet' ),
        'add_or_remove_items'        => __( 'Añadir o Eliminar', 'ccu-intranet' ),
        'choose_from_most_used'      => __( 'Elegir de los más usados', 'ccu-intranet' ),
        'popular_items'              => __( 'Populares', 'ccu-intranet' ),
        'search_items'               => __( 'Buscar', 'ccu-intranet' ),
        'not_found'                  => __( 'No encontrado', 'ccu-intranet' ),
        'no_terms'                   => __( 'No hay', 'ccu-intranet' ),
        'items_list'                 => __( 'Lista', 'ccu-intranet' ),
        'items_list_navigation'      => __( 'Lista de navegación', 'ccu-intranet' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'pais', array( 'marcas_cl', 'marcas_ar', 'marcas_bo', 'marcas_co', 'marcas_py', 'marcas_uy' ), $args );

}
add_action( 'init', 'marca_taxonomy_pais', 0 );

// Register Custom Taxonomy
function marca_taxonomy_tipo() {

    $labels = array(
        'name'                       => _x( 'Tipos', 'Taxonomy General Name', 'ccu-intranet' ),
        'singular_name'              => _x( 'tipo', 'Taxonomy Singular Name', 'ccu-intranet' ),
        'menu_name'                  => __( 'Tipos', 'ccu-intranet' ),
        'all_items'                  => __( 'Todos', 'ccu-intranet' ),
        'parent_item'                => __( 'Item Relacionado', 'ccu-intranet' ),
        'parent_item_colon'          => __( 'Item Relacionado:', 'ccu-intranet' ),
        'new_item_name'              => __( 'Añadir nuevo', 'ccu-intranet' ),
        'add_new_item'               => __( 'Añadir nuevo', 'ccu-intranet' ),
        'edit_item'                  => __( 'Editar', 'ccu-intranet' ),
        'update_item'                => __( 'Actualizar', 'ccu-intranet' ),
        'view_item'                  => __( 'Ver', 'ccu-intranet' ),
        'separate_items_with_commas' => __( 'Separar con coma', 'ccu-intranet' ),
        'add_or_remove_items'        => __( 'Añadir o Eliminar', 'ccu-intranet' ),
        'choose_from_most_used'      => __( 'Elegir de los más usados', 'ccu-intranet' ),
        'popular_items'              => __( 'Populares', 'ccu-intranet' ),
        'search_items'               => __( 'Buscar', 'ccu-intranet' ),
        'not_found'                  => __( 'No encontrado', 'ccu-intranet' ),
        'no_terms'                   => __( 'No hay', 'ccu-intranet' ),
        'items_list'                 => __( 'Lista', 'ccu-intranet' ),
        'items_list_navigation'      => __( 'Lista de navegación', 'ccu-intranet' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'tipo', array( 'marcas_cl', 'marcas_ar', 'marcas_bo', 'marcas_co', 'marcas_py', 'marcas_uy' ), $args );

}
add_action( 'init', 'marca_taxonomy_tipo', 0 );

// Register Custom Taxonomy
function video_taxonomy_cat() {

    $labels = array(
        'name'                       => _x( 'Categorías', 'Taxonomy General Name', 'ccu-intranet' ),
        'singular_name'              => _x( 'Categoría', 'Taxonomy Singular Name', 'ccu-intranet' ),
        'menu_name'                  => __( 'Categorías', 'ccu-intranet' ),
        'all_items'                  => __( 'Todos', 'ccu-intranet' ),
        'parent_item'                => __( 'Item Relacionado', 'ccu-intranet' ),
        'parent_item_colon'          => __( 'Item Relacionado:', 'ccu-intranet' ),
        'new_item_name'              => __( 'Añadir nuevo', 'ccu-intranet' ),
        'add_new_item'               => __( 'Añadir nuevo', 'ccu-intranet' ),
        'edit_item'                  => __( 'Editar', 'ccu-intranet' ),
        'update_item'                => __( 'Actualizar', 'ccu-intranet' ),
        'view_item'                  => __( 'Ver', 'ccu-intranet' ),
        'separate_items_with_commas' => __( 'Separar con coma', 'ccu-intranet' ),
        'add_or_remove_items'        => __( 'Añadir o Eliminar', 'ccu-intranet' ),
        'choose_from_most_used'      => __( 'Elegir de los más usados', 'ccu-intranet' ),
        'popular_items'              => __( 'Populares', 'ccu-intranet' ),
        'search_items'               => __( 'Buscar', 'ccu-intranet' ),
        'not_found'                  => __( 'No encontrado', 'ccu-intranet' ),
        'no_terms'                   => __( 'No hay', 'ccu-intranet' ),
        'items_list'                 => __( 'Lista', 'ccu-intranet' ),
        'items_list_navigation'      => __( 'Lista de navegación', 'ccu-intranet' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'cat_video', array( 'videos_cl', ' videos_ar', ' videos_bo', ' videos_co', ' videos_py', ' videos_uy' ), $args );

}
add_action( 'init', 'video_taxonomy_cat', 0 );

function merlin_pagination($pages = '', $range = 2)
{  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
         echo "<div class='pagination'>";
        //  if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>Anterior</a>";
        if($paged > 1 && $showitems < $pages) { echo "<a href='".get_pagenum_link($paged - 1)."' class='arrow-page'><i class='icon-chevron-left'></i></a>"; } else {
            echo "<span class='no-link-page at-first-page'><i class='icon-chevron-left'></i></span>";
        }
        echo "<div class='num-pages'>";
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='num current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='num inactive' >".$i."</a>";
             }
         }
         echo "</div>\n";
         if ($paged < $pages && $showitems < $pages) { echo "<a href='".get_pagenum_link($paged + 1)."' class='arrow-page'><i class='icon-chevron-right'></i></a>"; }
         else {
            echo "<span class='no-link-page at-last-page'><i class='icon-chevron-right'></i></span>";
         } 
        //  if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         echo "</div>\n";
     }
}
add_action('init', 'merlin_pagination');

add_filter( 'comment_form_fields', 'move_comment_field' );
function move_comment_field( $fields ) {
    $comment_field = $fields['comment'];
    $author_field = $fields['author'];
    $email_field = $fields['email'];
    $url_field = $fields['url'];
    $cookies_field = $fields['cookies'];
    unset( $fields['comment'] );
    unset( $fields['author'] );
    unset( $fields['email'] );
    unset( $fields['url'] );
    unset( $fields['cookies'] );
    $fields['author'] = $author_field;
    $fields['email'] = $email_field;
    $fields['url'] = $url_field;
    $fields['comment'] = $comment_field;
    $fields['cookies'] = $cookies_field;
    return $fields;
}

function psot_comment_form_avatar()
{
  ?>
<div class="comment-avatar">
    <?php 
     $current_user = wp_get_current_user();
     if ( ($current_user instanceof WP_User) ) {
        echo get_avatar( $current_user->user_email, 32 );
     }
     ?>
</div>
<?php
}

if ( is_user_logged_in() ) {
    add_action( 'comment_form_logged_in', 'psot_comment_form_avatar' );
    add_action( 'comment_form_before_fields', 'psot_comment_form_avatar' );
}

function my_comment_time_ago_function() {
    return sprintf( esc_html__( 'Hace %s', 'textdomain' ), human_time_diff(get_comment_time ( 'U' ), current_time( 'timestamp' ) ) );
    }

add_filter( 'get_comment_date', 'my_comment_time_ago_function' );

function ccu_type_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment; ?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
    <div id="comment-<?php comment_ID(); ?>" class="comment-block">
        <div class="profile-img-area">
            <div class="profile-img">
                <?php echo get_avatar($comment,$size='48',$default='<path_to_url>' ); ?>
            </div>
        </div>
        <div class="comment-area">
            <div class="comment-author vcard">
                <?php printf(__('<cite class="fn">%s</cite>'), get_comment_author_link()); ?>
            </div>
            <div class="comment-meta commentmetadata">
                <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
                    <?php printf(__('%1$s'), get_comment_date()); ?>
                </a><?php edit_comment_link(__('(Edit)'),'  ','') ?>
            </div>
        </div>
        <div class="comment-content-area">
            <?php if ($comment->comment_approved == '0') : ?>
            <em><?php _e('Tu comentario espera aprobación.') ?></em>
            <br />
            <?php endif; ?>
            <?php comment_text() ?>

            <div class="reply">
                <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
            </div>
        </div>
    </div>
</li>
<?php
}

add_action('rest_api_init', function() {
    register_rest_route( 'ccu', '/cambios_enviar_contenido/',
        array(
            'methods' => 'POST',
            'callback' => 'cambios_enviar_contenido'
        )
    );
});

add_action('wp_enqueue_scripts', 'cambios_insertar_js');

function cambios_insertar_js(){

    wp_register_script('cambios_script', get_template_directory_uri(). '/components/custom/js/cambios.js', array('jquery'), '1', true );
    wp_enqueue_script('cambios_script');

    // wp_localize_script('cambios_script','cambios_vars',['ajaxurl'=>admin_url('admin-ajax.php')]);
}

add_action('wp_ajax_nopriv_cambios_ajax_readmore','cambios_enviar_contenido');
add_action('wp_ajax_cambios_ajax_readmore','cambios_enviar_contenido');

function cambios_enviar_contenido()
{

    $pid        = intval($_POST['id_post']);
    $the_query  = new WP_Query(array('post_type' => 'cambios', 'p' => $pid));

    if ($the_query->have_posts()) {
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
            $data = get_template_part('components/template-parts/cambio');

        }
    } 
    else {
        echo '<div id="postdata">'.__('No hay contenido', THEME_NAME).'</div>';
    }
    wp_reset_postdata();

    echo '<div id="postdata">'.$data.'</div>';
    die();
}




/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getDriveClient()
{
    require_once 'vendor/autoload.php';
    //$client = new Google_Client(['client_id' => GOOGLE_CLIENT_ID]);
    $client = new Google_Client();
    $client->setApplicationName('Google Drive API PHP Quickstart');
    $client->setScopes(Google_Service_Drive::DRIVE_READONLY);
    $client->setAuthConfig('credentials.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    $tokenPath = 'token.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);

            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
        }
        // Save the token to a file.
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }
    return $client;
}


function update_photos_ccu($args){
    require_once 'vendor/autoload.php';
    $client         = getDriveClient();
    $service        = new Google_Service_Drive($client);
    $folder_name    = 'imagenestest';
    $drive_name     = 'IntranetCCU';
    $folder_id      = null;
    $drive_id       = null;
    $avatars_folder = 'wp-content/uploads/ccu_avatars';
    $log_file_name  = 'FotosNuevas.log';
    $log            = null;
    $new_log        = null;
    $new_log_array  = null;

    if(!is_dir($avatars_folder)){
        mkdir($avatars_folder);
    }

    if(is_file($avatars_folder)){
        $log = file_get_contents($avatars_folder.'/'.$log_file_name);
    }

    $optParamsDrives = array(
        'pageSize'  => 100,
        'fields' => 'nextPageToken, drives(id, name)',
    );
    $drives = $service->drives->listdrives($optParamsDrives);

    if(count($drives->getDrives()) != 0){
        foreach ($drives->getDrives() as $key => $drive) {
            if($drive->getName() == $drive_name){
                $drive_id = $drive->getId();
            }
        }
    }
    if($drive_id == null){
        //print "No drive found.\n";
        die;
    }
    $logParamsFiles = array(
        'fields' => 'nextPageToken, files(id, name)',
        'corpora' => 'drive',
        'driveId' => $drive_id,
        'includeItemsFromAllDrives' => true,
        'supportsAllDrives' => true,
        'q' => "mimeType != 'application/vnd.google-apps.folder' and name = 'FotosNuevas.log'",
    );
    $resultsLog = $service->files->listFiles($logParamsFiles);

    if(count($resultsLog->getFiles()) != 0){

        $temp_log = $service->files->get($resultsLog->getFiles()[0]->getId(),array('alt' => 'media'));
        $new_log = $temp_log->getBody()->getContents();

        if(!empty($new_log) && $log != $new_log){
            $new_log_array = preg_split("/\r\n|\n|\r/", $new_log);
        }
    }

    if($new_log_array == null){
        //print "No files to update found.\n";
        die;
    }

    $optParamsFiles = array(
        'fields' => 'nextPageToken, files(id, name)',
        'corpora' => 'drive',
        'driveId' => $drive_id,
        'includeItemsFromAllDrives' => true,
        'supportsAllDrives' => true,
        'q' => "mimeType != 'application/vnd.google-apps.folder'",
    );
    while(true){
        $resultsFiles = $service->files->listFiles($optParamsFiles);

        if(count($resultsFiles->getFiles()) != 0){
            foreach($resultsFiles->getFiles() as $file){
                if(in_array($file->getName(), $new_log_array)){
                    $filedata = $service->files->get($file->getId(),array('alt' => 'media'));
                    file_put_contents($avatars_folder.'/'.$file->getName(), $filedata->getBody()->getContents());
                }
            }
        }else{
            break;
        }
        if($resultsFiles['nextPageToken'] == null){
            break;
        }
        $optParamsFiles['pageToken'] = $resultsFiles['nextPageToken'];
    }

    file_put_contents($avatars_folder.'/'.$log_file_name, $new_log);


    //WP_CLI::success( $args[0] );
}
if(php_sapi_name() == 'cli'){
    WP_CLI::add_command( 'update_photos_ccu', 'update_photos_ccu' );
}



function update_users_ccu($args){
    require_once 'vendor/autoload.php';
    $client         = getDriveClient();
    $service        = new Google_Service_Drive($client);
    $drive_name     = 'IntranetCCU';
    $folder_id      = null;
    $drive_id       = null;
    $temp_csv       = null;
    $csv       = null;



    $optParamsDrives = array(
        'pageSize'  => 100,
        'fields' => 'nextPageToken, drives(id, name)',
    );
    $drives = $service->drives->listdrives($optParamsDrives);

    if(count($drives->getDrives()) != 0){
        foreach ($drives->getDrives() as $key => $drive) {
            if($drive->getName() == $drive_name){
                $drive_id = $drive->getId();
            }
        }
    }
    if($drive_id == null){
        die;
    }
    $ParamsCSV = array(
        'fields' => 'nextPageToken, files(id, name)',
        'corpora' => 'drive',
        'driveId' => $drive_id,
        'includeItemsFromAllDrives' => true,
        'supportsAllDrives' => true,
        'q' => "mimeType != 'application/vnd.google-apps.folder' and name contains 'EmpleadosVigentes' and name contains '.csv' ",
    );
    $resultsCSV = $service->files->listFiles($ParamsCSV);
    if(count($resultsCSV->getFiles()) != 0){
        $newest = 0;
        foreach ($resultsCSV->getFiles() as $k =>$file) {
            $temp_date  = (int)preg_replace("/[^0-9]/", "", $file->getName());
            if($temp_date >= $newest){
                try {
                    $temp_csv = $service->files->get($file->getId(), array('alt' => 'media'));
                    $newest         = $temp_date;
                } catch (Exception $e) {
                }
            }
        }

        if($temp_csv){
            $csv = $temp_csv->getBody()->getContents();
        }

    }


    if($csv == null){
        die;
    }
    $map = array(
        chr(0x8A) => chr(0xA9),
        chr(0x8C) => chr(0xA6),
        chr(0x8D) => chr(0xAB),
        chr(0x8E) => chr(0xAE),
        chr(0x8F) => chr(0xAC),
        chr(0x9C) => chr(0xB6),
        chr(0x9D) => chr(0xBB),
        chr(0xA1) => chr(0xB7),
        chr(0xA5) => chr(0xA1),
        chr(0xBC) => chr(0xA5),
        chr(0x9F) => chr(0xBC),
        chr(0xB9) => chr(0xB1),
        chr(0x9A) => chr(0xB9),
        chr(0xBE) => chr(0xB5),
        chr(0x9E) => chr(0xBE),
        chr(0x80) => '&euro;',
        chr(0x82) => '&sbquo;',
        chr(0x84) => '&bdquo;',
        chr(0x85) => '&hellip;',
        chr(0x86) => '&dagger;',
        chr(0x87) => '&Dagger;',
        chr(0x89) => '&permil;',
        chr(0x8B) => '&lsaquo;',
        chr(0x91) => '&lsquo;',
        chr(0x92) => '&rsquo;',
        chr(0x93) => '&ldquo;',
        chr(0x94) => '&rdquo;',
        chr(0x95) => '&bull;',
        chr(0x96) => '&ndash;',
        chr(0x97) => '&mdash;',
        chr(0x99) => '&trade;',
        chr(0x9B) => '&rsquo;',
        chr(0xA6) => '&brvbar;',
        chr(0xA9) => '&copy;',
        chr(0xAB) => '&laquo;',
        chr(0xAE) => '&reg;',
        chr(0xB1) => '&plusmn;',
        chr(0xB5) => '&micro;',
        chr(0xB6) => '&para;',
        chr(0xB7) => '&middot;',
        chr(0xBB) => '&raquo;',
    );


    $csv = html_entity_decode(mb_convert_encoding(strtr($csv, $map), 'UTF-8', 'ISO-8859-2'), ENT_QUOTES, 'UTF-8');


    $fp = fopen('php://temp','r+');
    fwrite($fp, $csv);
    rewind($fp); //rewind to process CSV
    $csv = [];
    $headers = fgetcsv($fp, 0, ';');
    while (($a = fgetcsv($fp, 0, ';')) !== FALSE) {
        if(count($a)==27){
            $csv[] = array_combine($headers, $a);
        }
    }

    global $wpdb;
    $users_db_name ='ccu_users_master';
    $fields = [ 'FIRST_NAME', 'MIDDLE_NAME', 'LAST_NAME', 'SECOND_LAST_NAME', 'BIRTHDATE', 'COUNTRY', 'COUNTRY_NAME', 'COMPANY', 'COMPANY_NAME', 'SETID_DEPT', 'DEPTID', 'DEPT_NAME', 'SETID_LOCATION', 'LOCATION', 'LOCATION_NAME', 'SETID_JOBCODE', 'JOBCODE', 'JOBCODE_NAME', 'SUPERVISOR_ID', 'UNION_CD', 'UNION_CD_NAME', 'UNION_CD_TYPE', 'JOB_FAMILY', 'OPRID', 'EMAIL', 'SEGMENTO' ];
    $empls = [];
    foreach ($csv as $csv_person) {
        if(!empty($csv_person["EMPLID"])){
            $empls[] = $csv_person["EMPLID"];
            $user_db_query = $wpdb->prepare( "SELECT * FROM $users_db_name WHERE EMPLID = %s", [$csv_person["EMPLID"]] );
            $user_db = $wpdb->get_results($user_db_query);
            foreach ($fields as $field) {
                if(!isset($csv_person[$field]) || trim(strval($csv_person[$field])) == ''){
                    $csv_person[$field] = '';
                }
            }
            $csv_person['FULL_NAME'] = implode(' ', array_filter([(!empty($csv_person['FIRST_NAME'])?$csv_person['FIRST_NAME']:NULL), (!empty($csv_person['MIDDLE_NAME'])?$csv_person['MIDDLE_NAME']:NULL), (!empty($csv_person['LAST_NAME'])?$csv_person['LAST_NAME']:NULL), (!empty($csv_person['SECOND_LAST_NAME'])?$csv_person['SECOND_LAST_NAME']:NULL)]));
            if(count($user_db) == 1){
                $wpdb->update($users_db_name, $csv_person, array('EMPLID'=>$csv_person["EMPLID"]));
            }else{
                $wpdb->insert($users_db_name, $csv_person);
            }
        }
    }
    if(count($empls)){
        $ids = implode( "', '", $empls );
        $wpdb->query( "DELETE FROM $users_db_name WHERE EMPLID NOT IN('$ids')" );
    }

    //WP_CLI::success( $args[0] );
}
if(php_sapi_name() == 'cli'){
    WP_CLI::add_command( 'update_users_ccu', 'update_users_ccu' );
}

add_filter('the_content', function($content) {
	return str_replace(array("<iframe", "</iframe>"), array('<div class="iframe-container"><iframe', "</iframe></div>"), $content);
});

add_filter('embed_oembed_html', function ($html, $url, $attr, $post_id) {
	if(strpos($html, 'youtube.com') !== false || strpos($html, 'youtu.be') !== false){
  		return '<div class="embed-responsive embed-responsive-16by9">' . $html . '</div>';
	} else {
	 return $html;
	}
}, 10, 4);


add_filter('embed_oembed_html', function($code) {
  return str_replace('<iframe', '<iframe class="embed-responsive-item" ', $code);
});

function filter_marcas(){
    $pais = $_POST["pais"];
    $tipo = $_POST['tipo'];
    $subtipo = $_POST['subtipo'];

    $paises = get_terms('pais');
    $tipos = get_terms('tipo');

    $paises_ids = wp_list_pluck( $paises, 'slug' );
    $tipos_ids = wp_list_pluck($tipos, 'slug');

    if($pais != '*') {
        $pais = $pais;
    } else {
        $pais = $paises_ids;
    }

    if($tipo != '*') {
        $tipo = $tipo;
    } else {
        $tipo = $tipos_ids;
    }

    $posts_marcas = array();

    $args = array(
                'post_type' => array('marcas_cl', 'marcas_ar', 'marcas_bo', 'marcas_co', 'marcas_py', 'marcas_uy'),
                'posts_per_page' => -1,
                'tax_query' => array(
                    'relation' => 'AND',
                    array(
                        'taxonomy' => 'pais',
                        'field'    => 'slug',
                        'terms'    => $pais,
                    ),
                    array(
                        'taxonomy' => 'tipo',
                        'field'    => 'slug',
                        'terms'    => $tipo,
                    )
                ),
            );
    $the_query = new WP_Query($args);

    if ( $the_query->have_posts() ) :
        $text = "";

        while ( $the_query->have_posts() ) : $the_query->the_post();
            $nameMarca = get_the_title();
            $introMarca = get_field( 'intro_marca' );
            $logo = get_field( 'logo' );
            $text.= '<a href="'.get_the_permalink().'" class="marca-box">
                        <div class="marca-info">
                            <h5 class="name">'.$nameMarca.'</h5>
                            <p class="description">'.$introMarca.'</p>
                            <span class="btn size-s is-verde is-rounded is-bordered is-transparent">Ver
                                Producto</span>
                        </div>
                        <div class="marca-data">
                            <div class="img">
                                <img src="'.$logo['url'].'" alt="'.$logo['alt'].'"
                                    class="logo">
                            </div>
                            <span class="name">'.$nameMarca.'</span>
                        </div>
                    </a>';
        endwhile;
        $posts_marcas[0] = $the_query->post_count;
        $posts_marcas[1] = $text;
        wp_reset_query();
    else:
        $posts_marcas[0] = 0;
        $posts_marcas[1] = '<p class="content-not-found">'.__('No hay eventos', 'ccu-intranet').'</p>';
    endif;


    return $posts_marcas;
}

add_action('rest_api_init', function() {
    register_rest_route( 'ccu', '/filter_marcas/',
        array(
            'methods' => 'POST',
            'callback' => 'filter_marcas'
        )
    );
});

function filter_organigrama(){
    $id_empleado = $_POST["idEmployee"];
    $organization_chart_info = get_organization_chart($id_empleado);
    $result = array();
    $result['data'] = '';

    if($organization_chart_info['status'] == 'ok'){
        $user = $organization_chart_info['data']['user'];
        $supervisor = $organization_chart_info['data']['supervisor'];
        $employees = $organization_chart_info['data']['employee'];

        $result['data'] .=
            '<div class="person-data">'.
                '<div class="person-avatar-box">'.
                    '<div class="profile-img cover" style="background-image: url('.get_photo_url($user->EMPLID).')" title=""></div>'.
                '</div>'.
                '<div class="person-info-box">'.
                    '<h3 class="nombre">'.implode(' ', (array_filter([$user->FIRST_NAME, $user->MIDDLE_NAME, $user->LAST_NAME, $user->SECOND_LAST_NAME]))).'</h3>'.
                    '<h4 class="cargo">'.$user->JOBCODE_NAME.'</h4>'.
                    '<h4>'.$user->DEPT_NAME.', '.$user->COUNTRY_NAME.'</h4>';

        if($user->EMAIL){
            $result['data'] .=
                        '<div class="contact-info">'.
                            '<a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&to='.$user->EMAIL.'&tf=1" class="contact-link" target="_blank">'.
                                '<span class="icono"><i class="icon-chat"></i></span><span class="texto">'.$user->EMAIL.'</span>'.
                            '</a>'.
                        '</div>';
        }
        $result['data'] .=
                '</div>'.
            '</div>';



        $result['data'] .=
            '<div class="organigrama-data">'.
                '<div class="organigrama-container">';

        if($supervisor){
            $result['data'] .=
                    '<div class="person-box level-one">'.
                        '<div class="person-avatar-box">'.
                            '<div class="profile-img cover" style="background-image: url('.get_photo_url($supervisor->EMPLID).');" title=""></div>'.
                        '</div>'.
                        '<div class="person-info-box">'.
                            '<h4 class="cargo">'.$supervisor->JOBCODE_NAME.'</h4>'.
                            '<h3 class="nombre">'.implode(' ', (array_filter([$supervisor->FIRST_NAME, $supervisor->MIDDLE_NAME, $supervisor->LAST_NAME, $supervisor->SECOND_LAST_NAME]))).'</h3>'.
                        '</div>'.
                    '</div>';
        }

        $result['data'] .=
                    '<div class="person-box level-two current-person">'.
                        '<div class="person-avatar-box">'.
                            '<div class="profile-img cover" style="background-image: url('.get_photo_url($user->EMPLID).');" title=""></div>'.
                        '</div>'.
                        '<div class="person-info-box">'.
                            '<h4 class="cargo">'. $user->EMAIL.'</h4>'.
                            '<h3 class="nombre">'.implode(' ', (array_filter([$user->FIRST_NAME, $user->MIDDLE_NAME, $user->LAST_NAME, $user->SECOND_LAST_NAME]))).'</h3>'.
                        '</div>'.
                    '</div>';

        if($employees){
            foreach ($employees as $key => $employee) {
                $result['data'] .=
                    '<div class="person-box level-three">'.
                        '<div class="person-avatar-box">'.
                            '<div class="profile-img cover" style="background-image: url('.get_photo_url($employee->EMPLID).');" title=""></div>'.
                        '</div>'.
                        '<div class="person-info-box">'.
                            '<h4 class="cargo">'.$employee->JOBCODE_NAME.'</h4>'.
                            '<h3 class="nombre">'.implode(' ', (array_filter([$employee->FIRST_NAME, $employee->MIDDLE_NAME, $employee->LAST_NAME, $employee->SECOND_LAST_NAME]))).'</h3>'.
                        '</div>'.
                    '</div>';
            }
        }
        $result['data'] .=
                '</div>'.
            '</div>';
        $result['status'] = 'ok';
    }else{
        $result['status'] = 'error';
        $result['data']   = 'No se encontraron coindincias con la búsqueda.';
    }

    return $result;
}

add_action('rest_api_init', function() {
    register_rest_route( 'ccu', '/filter_organigrama/',
        array(
            'methods' => 'POST',
            'callback' => 'filter_organigrama'
        )
    );
});


function suggestions_organigrama(){
    $nombre_empleado = $_POST["searchName"];
    $organization_chart_info = get_suggestions($nombre_empleado);
    $result = array();
    $result['data'] = '';

    if($organization_chart_info['status'] == 'ok'){
        $result['status'] = 'ok';
        $result['data']   = $organization_chart_info['data'];
    }else{
        $result['status'] = 'error';
        $result['data']   = 'No se encontraron coindincias con la búsqueda.';
    }

    return $result;
}

add_action('rest_api_init', function() {
    register_rest_route( 'ccu', '/suggestions_organigrama/',
        array(
            'methods' => 'POST',
            'callback' => 'suggestions_organigrama'
        )
    );
});

function get_dolar(){
    $countryCurr = $_POST['country'];
    $result = [
        'status' => "ERROR",
    ];
    switch($countryCurr) {
        case 'CHL':
            $currency = 'CLP';
            break;
        default:
            $currency = 'CLP';
    }
    try {
        $json = @file_get_contents("https://api.cambio.today/v1/quotes/USD/".$currency."/json?quantity=1&key=6404|3BW0o1LNBm_yhdbD7jqCu30GOiWLh1So");
        $result = $json !== FALSE?$json:$result;
    } catch (Exception $e) {
    }

    return $result;
}

add_action('rest_api_init', function() {
    register_rest_route( 'ccu', '/get_dolar/',
        array(
            'methods' => 'POST',
            'callback' => 'get_dolar'
        )
    );
});

?>