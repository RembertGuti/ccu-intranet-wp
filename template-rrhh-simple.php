<?php
/*
* Template Name: Recursos Humanos - Simple
*/
get_header();
?>

<section class="section">
    <div class="wrap-xl">
        <div class="row-area">
            <?php if ( have_rows( 'politicas_rrhh' ) ) : ?>
            <div class="organigrama-container box-section box-1-wide">
                <div class="content">
                    <?php while ( have_rows( 'politicas_rrhh' ) ) : the_row(); ?>
                    <div class="heading-box-area">
                        <h3 class="head-title"><?php the_sub_field( 'titulo_caja_prh' ); ?></h3>
                    </div>
                    <div class="organigrama-area">
                        <div class="box-content">
                            <?php $icono_prh = get_sub_field( 'icono_prh' ); ?>
                            <?php if ( $icono_prh ) { ?>
                            <div class="icono-imagen">
                                <img src="<?php echo $icono_prh['url']; ?>" alt="<?php echo $icono_prh['alt']; ?>" />
                            </div>
                            <?php } ?>
                            <h4 class="titulo"><?php the_sub_field( 'titulo_interno_prh' ); ?></h4>
                            <div class="bajada">
                                <p><?php the_sub_field( 'bajada_prh' ); ?></p>
                            </div>
                            <?php $link_prh = get_sub_field( 'link_prh' ); ?>
                            <?php if ( $link_prh ) { ?>
                            <div class="boton-mas">
                                <a href="<?php echo $link_prh['url']; ?>" target="<?php echo $link_prh['target']; ?>"
                                    class="btn is-verde is-rounded size-s"><?php echo $link_prh['title']; ?></a>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php endif; ?>
            <?php if ( have_rows( 'cambios_organizacionales' ) ) : ?>
            <div class="organigrama-container box-section box-1-wide">
                <div class="content">
                    <?php while ( have_rows( 'cambios_organizacionales' ) ) : the_row(); ?>
                    <div class="heading-box-area">
                        <h3 class="head-title"><?php the_sub_field( 'titulo_caja_co' ); ?></h3>
                    </div>
                    <div class="organigrama-area">
                        <div class="box-content">
                            <?php $icono_co = get_sub_field( 'icono_co' ); ?>
                            <?php if ( $icono_co ) { ?>
                            <div class="icono-imagen">
                                <img src="<?php echo $icono_co['url']; ?>" alt="<?php echo $icono_co['alt']; ?>" />
                            </div>
                            <?php } ?>
                            <h4 class="titulo"><?php the_sub_field( 'titulo_interno_co' ); ?></h4>
                            <div class="bajada">
                                <p><?php the_sub_field( 'bajada_co' ); ?></p>
                            </div>
                            <div class="boton-mas">
                                <a href="#<?php the_sub_field( 'link_popup_co' ); ?>" target="_blank"
                                    class="btn is-verde is-rounded size-s modal-trigger" data-id="modal-cambios">Ver
                                    Más</a>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>

<div data-id="modal-cambios" class="modal modal-cambios-area">
    <i class="close icon-equis"></i>
    <div class="content-modal modal-cambios">
        <div class="modal-heading">
            <div class="title-area">
                <h4>Cambios Organizacionales</h4>
            </div>
            <div class="date-selector-area">
                <div class="date-selector-box">
                    <div class="select-box">
                        <select name="post-date-selector" id="post-date-selector">
                            <?php 
                            $posts = get_posts( array(
                                'post_type' => 'cambios',
                                'meta_key'  => 'fecha_cambios',
                                'orderby'   => 'meta_value_num',
                                'order'     => 'DESC',
                            ));
                            
                            if( $posts ) {
                                foreach( $posts as $post ) {
                                    echo '<option value="'.get_the_id($post).'">'.get_the_title($post).'</option>';
                                }
                            }
                            ?>
                        </select>
                        <i class="icon-chevron-down"></i>
                    </div>
                </div>
                <div class="icon-box">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/organizacion-icon.svg" alt="">
                </div>
            </div>
        </div>
        <div id="post-cambios" class="cambios-container">
            <?php
            $the_query  = new WP_Query(array('post_type' => 'cambios', 'posts_per_page' => 1, 'orderby' => 'meta_value_num', 'meta_key' => 'fecha_cambios', 'order' => 'DESC'));

            if ($the_query->have_posts()) {
                while ( $the_query->have_posts() ) {
                    $the_query->the_post();
                    echo get_template_part('components/template-parts/cambio');
        
                }
            } 
            else {
                echo '<div id="postdata">'.__('Didnt find anything', THEME_NAME).'</div>';
            }
            wp_reset_postdata();
            ?>
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<script>
$(document).ready(function() {


    $("#cambios-tabs").tabs({
        show: 'fade',
        hide: 'fade',
        activate: function(event, ui) {
            newPanel = $('#slider-gerentes').slick('setPosition');
        }
    });
    $('#slider-gerentes').slick({
        arrows: false,
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        centerMode: true,
        centerPadding: '20px'
    });
    $('#gerentes-arrows .arrow').each(function(index, element) {
        if ($(this).hasClass('prev')) {
            $(this).click(function(e) {
                e.preventDefault();
                $('#slider-gerentes').slick('slickPrev');
            });

        } else if ($(this).hasClass('next')) {
            $(this).click(function(e) {
                e.preventDefault();
                $('#slider-gerentes').slick('slickNext');
            });
        }
    });
});
</script>
<script>
$(document).ajaxComplete(function() {
    $("#cambios-tabs").tabs({
        show: 'fade',
        hide: 'fade',
        activate: function(event, ui) {
            newTab = $('#slider-gerentes').slick('setPosition');
        }
    });
    $('#slider-gerentes').slick({
        arrows: false,
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        centerMode: true,
        centerPadding: '20px'
    });
});
</script>


<?php get_footer(); ?>